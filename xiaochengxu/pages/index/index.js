
var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
const app = getApp()
let wcache = require("../../utils/wcache.js");
var urlPath = getApp().globalData.url;
Page({
  data: {
    inputVal: "",
    inputVal2: "",
    inputVal3: "",
    searchPageNum: 1,   // 设置加载的第几次，默认是第一次
    tabs: ['全部', '在线', '离线'],// 导航菜单栏
    curIdx: 0,// 当前导航索引
    curIdx2: 0,// 当前导航索引
    scrollHeight: 0, //滚动高度 = 设备可视区高度 -  导航栏高度
    list: [],// 内容区列表
    list2: [],// 内容区列表
    list3: [],// 内容区列表
    list4: [],// 内容区列表
    arr: [1],
  },
  //跳转到详情页
  detail: function (e) {
    var carrierName = e.currentTarget.id;
    var all = e.currentTarget.dataset.all;
    var online = e.currentTarget.dataset.online;
    var offline = e.currentTarget.dataset.offline;
    wx.setStorageSync('carrierName', carrierName);
    wx.navigateTo({
      url: "/pages/turn/turn?carrierName=" + carrierName + '&all=' + all + '&online=' + online + '&offline=' + offline,
    })
  },
  //点击切换
  clickTab: function (e) {
    this.setData({
      curIdx: e.currentTarget.dataset.current,
      curIdx2: e.currentTarget.dataset.current
    })
  },
  //滑动切换
  swiperTab: function (e) {
    this.setData({
      curIdx: e.detail.current,
      curIdx2: e.detail.current
    });
  },
  /**
* 页面上拉触底事件的处理函数
*/
  // onReachBottom: function () {
  //   // 更新列表
  //   let list = this.data.list;
  //   console.log(list)
  //   let lens = list.length
  //   for (let i = lens; i < lens + 30; i++) {
  //     list.push(i)
  //   }
  //   this.setData({
  //     list: list
  //   });

  // },
  onLoad: function (options) {
    var that = this;
    this.setData({
      carrierName: options.carrierName
    });
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      // url: urlPath + 'ztjk/miniapp/deviceList',
      header: {
        'token': token
      },
      data: {
        // name: title1,
        // page: that.data.searchPageNum,
        // limit: 20
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        wx.hideLoading();
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          
          that.setData({
            list: arr,
            tabs: ['全部' + '(' + res.data.data.length + ')'],
            // tabs: ['全部' + '(' + res.data.data.length + ')', '在线' + '(' + '' + ')', '离线' + '(' + '' + ')'],
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      // url: urlPath + 'ztjk/miniapp/deviceList',
      header: {
        'token': token
      },
      data: {
        // name: title1,
        // page: that.data.searchPageNum,
        // limit: 20
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list2: arr,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      // url: urlPath + 'ztjk/miniapp/deviceList',
      header: {
        'token': token
      },
      data: {
        // name: title1,
        // page: that.data.searchPageNum,
        // limit: 20
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list3: arr,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request1: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      // url: urlPath + 'ztjk/miniapp/deviceList',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        page: that.data.searchPageNum,
        limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list: arr,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request2: function (title1) {
    var that = this;
    var token = wcache.get('token');
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      // url: urlPath + 'ztjk/miniapp/deviceList',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        page: that.data.searchPageNum,
        limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list2: arr,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request3: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      // url: urlPath + 'ztjk/miniapp/deviceList',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        page: that.data.searchPageNum,
        limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list3: arr,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request4: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        page: that.data.searchPageNum,
        limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list: []
          });
          that.setData({
            list: arr,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request5: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.request({
      url: urlPath + 'ztjk/miniapp/carrier',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        page: that.data.searchPageNum,
        limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        if (res.data.code == 0) {
          var that = this;
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            var data = {
              'carrier': res.data.data[i][0], 'all': res.data.data[i][1], 'online': res.data.data[i][2], 'offline': res.data.data[i][3]
            }
            arr.push(data)
          }
          that.setData({
            list: []
          });
          that.setData({
            list: arr,
          });
          this.setData({
            arr: [],
            list4: [],
          });
          var value = e.detail.value;
          var ary = that.data.list;
          var len = ary.length;
          var reg = new RegExp(value, 'i');
          ary.forEach(function (i) {
            that.data.list4.push(i);
          })
          var ce = that.data.list4;
          for (var i = 0; i < len; i++) {
            if (ce[i].carrier.match(reg)) {
              that.data.arr.push(ce[i]);
            }
          }
          this.setData({
            inputVal: e.detail.value,
            arr: that.data.arr,
          });
          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  //筛点击搜索
  // searchValueInput: function (e) {
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal: value,
  //   });
  // },
  // searchValueInput: function (e) {
  //   var that = this;
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal: e.detail.value,
  //     searchPageNum: 1,
  //     list: []
  //   });
  //   that.request1(this.data.inputVal);
  // },
  searchValueInput: function (e) {
    var that = this;
    this.setData({
      arr: [],
      list4: [],
    });
    var value = e.detail.value;
    var ary = that.data.list;
    var len = ary.length;
    var reg = new RegExp(value, 'i');
    ary.forEach(function (i) {
      that.data.list4.push(i);
    })
    var ce = that.data.list4;
    for (var i = 0; i < len; i++) {
      if (ce[i].carrier.match(reg)) {
        that.data.arr.push(ce[i]);
      }
    }
    this.setData({
      inputVal: e.detail.value,
      arr: that.data.arr,
    });
  },
  // searchValueInput2: function (e) {
  //   var that = this;
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal2: e.detail.value,
  //     searchPageNum: 1,
  //     list2: []
  //   });
  //   that.request2(this.data.inputVal2);
  // },
  // searchValueInput3: function (e) {
  //   var that = this;
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal3: e.detail.value,
  //     searchPageNum: 1,
  //     list3: []
  //   });
  //   that.request3(this.data.inputVal3);
  // },
  searchBtn: function (e) {
    var that = this;
    this.setData({
      searchPageNum: 1,
      list: []
    });
    that.request(that.data.inputVal);
  },
  //滚动到底部触发事件
  searchScrollLower: function () {
    let that = this;
    that.setData({
      searchPageNum: that.data.searchPageNum + 1,  //每次触发上拉事件，把searchPageNum+1
    });
    that.request(that.data.inputVal);
  },

  //下拉刷新
  onPullDownRefresh: function () {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //加载
    this.setData({
      searchPageNum: 1,
      Loading: false,
      searchLoading: true,
      searchLoadingComplete: true,
      norecord: true,
      // list: [],
      // arr: [1],
    });
    setTimeout(function () {
      console.log('ceshi');
      if (that.data.inputVal.length == 0) {
        that.request4(that.data.inputVal);
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
      }
      else if (that.data.inputVal.length != 0) {
        that.request5(that.data.inputVal);
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
      }
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新  
    }, 1000)
  },
  clickMe: function (e) {
    var that = this;
    // 获取事件绑定的当前组件
    var index = e.currentTarget.dataset.index;
    that.data.list[index].hiddenName = !that.data.list[index].hiddenName
    this.setData({
      list: that.data.list
    })
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      //inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  off_canvas: function () {
    this.data.open ? this.setData({ open: false }) : this.setData({ open: true });
  },
  openConfirm: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    wx.showModal({
      title: '提示',
      content: '确认删除吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: urlPath + 'ship/delete',
            header: {
              'token': token
            },
            data: {
              id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              // console.log(data);
              if (data.data.code == 0) {
                wx.showToast({
                  title: "删除成功",
                  duration: 2000
                });
                setTimeout(function () {
                  that.data.list.splice(index, 1);
                  that.setData({
                    list: that.data.list
                  });
                }, 1000)
              } else if (res.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else {
                wx.showToast({
                  title: data.data.msg,
                  duration: 2000
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  onShow: function () {
    // 100为导航栏swiper-tab 的高度
    this.setData({
      scrollHeight: wx.getSystemInfoSync().windowHeight - (wx.getSystemInfoSync().windowWidth / 750 * 100),
    })
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  tabChange: function (e) {
    var index = e.target.id.replace('tabTag-', '');
    this.setData({ swiperCurrentIndex: index, tabCurrentIndex: index });
  },
  swiperChange: function (e) {
    var index = e.detail.current;
    this.setData({ tabCurrentIndex: index });
  },
  //每个选项滚动到底部
  scrollend: function (e) {
    //获取是哪个选项滚动到底？
    var index = e.currentTarget.dataset.scindex;
    console.log(index);
    //可以利用 tabs 携带的分类id 与服务器交互请求对应分类的数据
    console.log(this.data.tabs[index].id);
    //加载更多的演示
    //判断当前是否正在加载
    if (this.data.tabs[index].loadingType === 1) {
      return false;
    }
    //判断是否是最后一页
    if (this.data.tabs[index].page > 3) {
      this.data.tabs[index].loadingType = 2;//全部
      this.setData({ tabs: this.data.tabs });
      return;
    }
    this.data.tabs[index].loadingType = 1;//加载中
    this.setData({ tabs: this.data.tabs });
    //模拟延迟
    setTimeout(function () {
      _self.data.newsAll[index] = _self.data.newsAll[index].concat(news);
      _self.setData({ newsAll: _self.data.newsAll });
      //分页
      _self.data.tabs[index].page++;
      _self.data.tabs[index].loadingType = 0;//恢复加载状态
      _self.setData({ tabs: _self.data.tabs });
      //
    }, 1000);
  }
});

