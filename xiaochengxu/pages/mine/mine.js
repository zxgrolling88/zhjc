// pages/mine/mine.js
let wcache = require("../../utils/wcache.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var name = wcache.get('name');
    let that = this;
    that.setData({
      name: name,
    });
  },
  //跳转到详情页
  quit: function (e) {
    wx.clearStorageSync()
    wx.clearStorageSync()
    wx.reLaunch({
      url: '/pages/login/login'
    })
  },
  updatePwd: function (e) {
    wx.navigateTo({
      url: '/pages/updatePwd/updatePwd',
    })
  },
  history: function (e) {
    wx.navigateTo({
      // url: '/pages/history/history',
      url: '/pages/history2/history2', 
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})