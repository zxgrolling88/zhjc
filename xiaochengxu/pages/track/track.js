const app = getApp()
var urlPath = getApp().globalData.url;
let wcache = require("../../utils/wcache.js");
var util = require('../../utils/util.js'); 
var wxGps = require('../../utils/wxGps.js'); 
Page({
  data: {
    latitude: '',
    longitude: '',
    lockCode: '',
    markers: [],
    lockStatus: '',
    gpsTime: '',
    time: '',
    lng: '',
    lat: '',
    address: '',
  },
  onLoad: function(options) {
    var result = wxGps.wgs84_to_gcj02(options.longitude, options.latitude);
    var that = this;
    var time = util.formatTime(new Date());
    that.setData({
      // longitude: options.longitude,
      // latitude: options.latitude,
      longitude: result[0],
      latitude: result[1],
      lockCode: options.lockCode,
      gpsTime: options.gpsTime,
      lockStatus: options.lockStatus,
      markers: [{
        'latitude': result[1],
        'longitude': result[0],
      }]
    });

    var locationString = result[1] + "," + result[0];
        wx.request({
          url: 'https://apis.map.qq.com/ws/geocoder/v1/',
          data: {
            "key": "AH3BZ-3QQCF-B3KJU-JBBXR-BJKIF-SKFRF",
            "location": locationString,
          },
          method: 'GET',
          success: function (r) {
            //输出一下位置信息
            console.log('用户位置信息', r.data.result.address);
            //r.data.result.address获得的就是用户的位置信息，将它保存到一个全局变量上
            that.setData({
              address: r.data.result.address
            });
          }
        });
  },
  closeConfirm: function(e) {
    var that = this;
    var token = wcache.get('token');
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(that.data.lockCode);
    wx.showModal({
      title: '提示',
      content: '确认施封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function(res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '施封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + that.data.lockCode + '&lng=' + that.data.longitude + '&lat=' + that.data.latitude + '&address=' + that.data.address,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function(data) {
              wx.hideLoading();
              // console.log(data);
              if (data.data.code == 0) {
                // wx.showToast({
                //   title: "施封成功",
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: "施封成功",
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.navigateBack({
                        delta: 1,
                      })
                    }
                  },
                });
                // setTimeout(function() {
                //   wx.navigateBack({
                //     delta: 1,
                //   })
                // }, 2000)
                that.setData({
                  lockStatus: '施封态',
                })
              } else if (data.data.code == 401) {
                wx.showToast({
                    title: '请重新登录',
                    icon: 'loading',
                    duration: 2000
                  }),
                  setTimeout(function() {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500) {
                // wx.showToast({
                //   image: '../../images/error.png',
                //   title: data.data.msg,
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.navigateBack({
                        delta: 1,
                      })
                    }
                  },
                });
                // setTimeout(function () {
                //   wx.navigateBack({
                //     delta: 1,
                //   })
                // }, 2000)
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function() {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  openConfirm: function (e) {
    var that = this;
    var token = wcache.get('token');
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(that.data.lockCode);
    wx.showModal({
      title: '提示',
      content: '确认解封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '解封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + that.data.lockCode + '&lng=' + that.data.longitude + '&lat=' + that.data.latitude + '&address=' + that.data.address,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              // console.log(data);
              if (data.data.code == 0) {
                // wx.showToast({
                //   title: "解封成功",
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: "解封成功",
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.navigateBack({
                        delta: 1,
                      })
                    }
                  },
                });
                // setTimeout(function () {
                //   wx.navigateBack({
                //     delta: 1,
                //   })
                // }, 2000)
                that.setData({
                  lockStatus: '解封态',
                })
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500) {
                // wx.showToast({
                //   image: '../../images/error.png',
                //   title: data.data.msg,
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      wx.navigateBack({
                        delta: 1,
                      })
                    }
                  },
                });
                // setTimeout(function () {
                //   wx.navigateBack({
                //     delta: 1,
                //   })
                // }, 2000)
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
})