var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
const app = getApp()
let wcache = require("../../utils/wcache.js");
var urlPath = getApp().globalData.url;
var util = require('../../utils/util.js');  
var util2 = require('../../utils/util2.js')
const formatLocation = util2.formatLocation
Page({
  data: {
    inputVal: "",
    inputVal2: "",
    inputVal3: "",
    carrierName: "",
    allAccount: "",//全部设备数量
    allAccount2: "",//全部设备数量
    allAccount3: "",//全部设备数量
    onlineAccount: "",//在线设备数量
    offlineAccount: "",//离线设备数量
    allAccount2: "",//全部设备数量
    onlineAccount2: "",//在线设备数量
    offlineAccount2: "",//离线设备数量
    searchPageNum: 1,   // 设置加载的第几次，默认是第一次
    tabs: ['全部', '在线', '离线'],// 导航菜单栏
    curIdx: 0,// 当前导航索引
    curIdx2: 0,// 当前导航索引
    scrollHeight: 0, //滚动高度 = 设备可视区高度 -  导航栏高度
    list: [],// 内容区列表
    list2: [],// 内容区列表
    list3: [],// 内容区列表
    arr: [1],
    arr2: [1],
    arr3: [1],
    list4: [],// 内容区列表
    list5: [],// 内容区列表
    list6: [],// 内容区列表
    time:'',
    lng: '',// 经度
    lat: '',// 纬度
  },
  //点击切换
  clickTab: function (e) {
    this.setData({
      curIdx: e.currentTarget.dataset.current,
      curIdx2: e.currentTarget.dataset.current
    })
  }, 
  //滑动切换
  swiperTab: function (e) {
    this.setData({
      curIdx: e.detail.current,
      curIdx2: e.detail.current
    });
  },
  onLoad: function (e) {
    var token = wcache.get('token');
    var username = wcache.get('username');
    if (username == "admin"){
      var carrierName = wx.getStorageSync('carrierName')
      wx.setStorageSync('carrierName', carrierName)
    }else {
      var carrierName = wcache.get('name')
    }
    
    var that = this;
    var time = util.formatTime(new Date());
    // var formatLocation = util2.formatLocation
    wx.getLocation({
      success(res) {
        console.log(res)
        that.setData({
          lng: res.longitude,
          lat: res.latitude,
          // location: formatLocation(res.longitude, res.latitude)
        })
      }
    })
    this.setData({
      time: time, 
      carrierName: carrierName,
      // carrierName: options.carrierName,
      // // carrierName: '镇石',
      allAccount: e.all,
      onlineAccount: e.online,
      offlineAccount: e.offline
    });
    var token = wcache.get('token');
    
    // var carrierName = that.data.carrirName
    var allAccount = that.data.allAccount
    var onlineAccount = that.data.onlineAccount
    var offlineAccount = that.data.offlineAccount
  },
  request: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var allAccount = this.data.allAccount;
    var onlineAccount = this.data.onlineAccount;
    var offlineAccount = this.data.offlineAccount;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName, 
      // url: urlPath + 'ztjk/vec/page' , 
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: 1,
        // limit: 10
      },
      method: 'GET',
        success: function (res) {
          console.log(that.data.arr.length);
        if (res.data.code == 0) {
          var allAccount = res.data.data.length
          that.setData({
            list: [],
            allAccount: allAccount,
            allAccount2: allAccount
          });
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list.push(res.data.data[i]);
          }
          that.setData({
            list: that.data.list,
            tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
      // url: urlPath + 'ztjk/vec/page' , 
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        page: 1,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        
        // console.log(res);
        if (res.data.code == 0) {
          var onlineAccount = res.data.data.length
          that.setData({
            list2: [],
            onlineAccount: onlineAccount,
          });
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list2.push(res.data.data[i]);
          }
          that.setData({
            list2: that.data.list2,
            tabs: ['全部' + '(' + that.data.allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
          
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
      // url: urlPath + 'ztjk/vec/page' , 
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        
        // console.log(res);
        if (res.data.code == 0) {
          var offlineAccount = res.data.data.length
          that.setData({
            list3: [],
            offlineAccount: offlineAccount
          });
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list3.push(res.data.data[i]);
          }
          that.setData({
            list3: that.data.list3,
            tabs: ['全部' + '(' + that.data.allAccount + ')', '在线' + '(' + that.data.onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
    wx.hideLoading();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新  
  },
  request1: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName ,
      // url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
      // url: urlPath + 'ztjk/vec/page' , 
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
        // console.log(res);
        if (res.data.code == 0) {
          that.setData({
            list: []
          });
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list.push(res.data.data[i]);
          }
          that.setData({
            list: that.data.list,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request2: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
      // url: urlPath + 'ztjk/vec/page' , 
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        wx.hideLoading();
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
        if (res.data.code == 0) {
          that.setData({
            list2: []
          });
          var arr = []
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list2.push(res.data.data[i]);
          }
          that.setData({
            list2: that.data.list2,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request3: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
      // url: urlPath + 'ztjk/vec/page' , 
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        // console.log(res);
        wx.hideLoading();
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
        if (res.data.code == 0) {
          var arr = []
          that.setData({
            list3: []
          });
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list3.push(res.data.data[i]);
          }
          that.setData({
            list3: that.data.list3,
          });

          wx.hideNavigationBarLoading() //完成停止加载
          wx.stopPullDownRefresh() //停止下拉刷新
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request4: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var inputVal = this.data.inputVal;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
        // console.log(res);
        if (res.data.code == 0) {
          var arr = []
          that.setData({
            list: []
          });
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list.push(res.data.data[i]);
          }
          that.setData({
            list: that.data.list,
            list4: [],
            arr: [],
            // arr: that.data.list,
          });
          var ary = that.data.list;
          var len = ary.length;
          var value = that.data.inputVal;
          var reg = new RegExp(value, 'i');
          ary.forEach(function (i) {
            that.data.list4.push(i);
          })
          var ce = that.data.list4;
          for (var i = 0; i < len; i++) {
            if (ce[i].vecNo.match(reg)) {
              that.data.arr.push(ce[i]);
            }
          }
          that.setData({
            arr: that.data.arr,
          });
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request5: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var inputVal = this.data.inputVal2;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
        // console.log(res);
        if (res.data.code == 0) {
          that.setData({
            list2: []
          });
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list2.push(res.data.data[i]);
          }
          that.setData({
            list2: that.data.list2,
            list5: [],
            arr2: [],
            // arr: that.data.list,
          });
          var ary = that.data.list2;
          var len = ary.length;
          var value = that.data.inputVal2;
          var reg = new RegExp(value, 'i');
          ary.forEach(function (i) {
            that.data.list5.push(i);
          })
          var ce = that.data.list5;
          for (var i = 0; i < len; i++) {
            if (ce[i].vecNo.match(reg)) {
              that.data.arr2.push(ce[i]);
            }
          }
          that.setData({
            arr2: that.data.arr2,
          });
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request6: function (title1) {
    var that = this;
    var carrierName = this.data.carrierName;
    var inputVal3 = this.data.inputVal3;
    var token = wcache.get('token');
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    wx.request({
      url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
      header: {
        'token': token
      },
      data: {
        filters: { "groupOp": "AND", "rules": [{ "field": "vec_no", "op": "cn", "data": title1 }] },
        // page: that.data.searchPageNum,
        // limit: 10
      },
      method: 'GET',
      success: function (res) {
        wx.hideLoading();
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新  
        // console.log(res);
        if (res.data.code == 0) {
          that.setData({
            list3: []
          });
          for (var i = 0; i < res.data.data.length; i++) {
            that.data.list3.push(res.data.data[i]);
          }
          that.setData({
            list3: that.data.list3,
            list6: [],
            arr3: [],
            // arr: that.data.list,
          });
          var ary = that.data.list3;
          var len = ary.length;
          var value = that.data.inputVal3;
          var reg = new RegExp(value, 'i');
          ary.forEach(function (i) {
            that.data.list6.push(i);
          })
          var ce = that.data.list6;
          for (var i = 0; i < len; i++) {
            if (ce[i].vecNo.match(reg)) {
              that.data.arr3.push(ce[i]);
            }
          }
          that.setData({
            arr3: that.data.arr3,
          });
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '请重新登录',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else if (res.data.code == 500){
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          });
        }
      },
      fail: function () {
        that.setData({
          searchLoading: true,
          searchLoadingComplete: true
        });
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    })
  },
  request7: function (e) {
    this.setData({
      // allAccount = res.data.data.length + 11
      tabs: ['全部' + '(' + e[0].all + ')', '在线' + '(' + e[0].online + ')', '离线' + '(' + e[0].offline + ')'],
    });
    
  },
  //筛点击搜索
  // searchValueInput: function (e) {
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal: e.detail.value,
  //   });
  // },
  // searchValueInput2: function (e) {
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal2: value,
  //   });
  // },
  // searchValueInput3: function (e) {
  //   var value = e.detail.value;
  //   console.log(value);
  //   this.setData({
  //     inputVal3: value,
  //   });
  // },
  searchValueInput: function (e) {
    var that = this;
    this.setData({
      arr: [],
      list4: [],
    });
    var value = e.detail.value;
    var ary = that.data.list;
    var len = ary.length;
    var reg = new RegExp(value, 'i');
    ary.forEach(function (i) {
      that.data.list4.push(i);
    })
    var ce = that.data.list4;
    for (var i = 0; i < len; i++) {
      if (ce[i].vecNo.match(reg)) {
        that.data.arr.push(ce[i]);
      }
    }
    this.setData({
      inputVal: e.detail.value,
      arr: that.data.arr,
    });
  },
  searchValueInput2: function (e) {
    var that = this;
    this.setData({
      arr2: [],
      list5: [],
    });
    var value = e.detail.value;
    var ary = that.data.list2;
    var len = ary.length;
    var reg = new RegExp(value, 'i');
    ary.forEach(function (i) {
      that.data.list5.push(i);
    })
    var ce = that.data.list5;
    for (var i = 0; i < len; i++) {
      if (ce[i].vecNo.match(reg)) {
        that.data.arr2.push(ce[i]);
      }
    }
    this.setData({
      inputVal2: e.detail.value,
      arr2: that.data.arr2,
    });
    // var that = this;
    // var value = e.detail.value;
    // console.log(value);
    // this.setData({
    //   inputVal2: value,
    //   searchPageNum: 1,
    //   list2: []
    // });
  },
  searchValueInput3: function (e) {
    var that = this;
    this.setData({
      arr3: [],
      list6: [],
    });
    var value = e.detail.value;
    var ary = that.data.list3;
    var len = ary.length;
    var reg = new RegExp(value, 'i');
    ary.forEach(function (i) {
      that.data.list6.push(i);
    })
    var ce = that.data.list6;
    for (var i = 0; i < len; i++) {
      if (ce[i].vecNo.match(reg)) {
        that.data.arr3.push(ce[i]);
      }
    }
    this.setData({
      inputVal3: e.detail.value,
      arr3: that.data.arr3,
    });
    // var that = this;
    // var value = e.detail.value;
    // console.log(value);
    // this.setData({
    //   inputVal3: value,
    //   searchPageNum: 1,
    //   list3: []
    // });
    // that.request3(this.data.inputVal3);
  },
  searchBtn: function (e) {
    var that = this;
    this.setData({
      searchPageNum: 1,
      list: []
    });
    that.request1(this.data.inputVal);
  },
  searchBtn2: function (e) {
    var that = this;
    this.setData({
      searchPageNum: 1,
      list2: []
    });
    that.request2(this.data.inputVal2);
  },
  searchBtn3: function (e) {
    var that = this;
    this.setData({
      searchPageNum: 1,
      list3: []
    });
    that.request3(this.data.inputVal3);
  },
  //滚动到底部触发事件
  searchScrollLower: function () {
    let that = this;
    that.setData({
      searchPageNum: that.data.searchPageNum + 1,  //每次触发上拉事件，把searchPageNum+1
    });
    that.request(that.data.inputVal);
  },

  //下拉刷新
  onPullDownRefresh: function (e) {
    var that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //加载
    this.setData({
      searchPageNum: 1,
      Loading: false,
      searchLoading: true,
      searchLoadingComplete: true,
      norecord: true,
      // list: [],// 内容区列表
      // list2: [],// 内容区列表
      // list3: [],// 内容区列表
      // arr: [1],
      // arr2: [1],
      // arr3: [1],
      // list4: [],// 内容区列表
      // list5: [],// 内容区列表
      // list6: [],// 内容区列表
    });
    setTimeout(function () {
      // var that = this;
      console.log('ceshi');
      if (that.data.curIdx2 == 0&&that.data.inputVal.length == 0 ){
        that.request1(that.data.inputVal); 
        that.request2(that.data.inputVal2);
        that.request3(that.data.inputVal3);
      }
      else if (that.data.curIdx2 == 0 &&that.data.inputVal.length != 0){
        that.request4(that.data.inputVal); 
        that.request5(that.data.inputVal2);
        that.request6(that.data.inputVal3);
      }
      else if (that.data.curIdx2 == 1 && that.data.inputVal2.length == 0) {
        // that.request2(that.data.inputVal2);
        that.request1(that.data.inputVal);
        that.request2(that.data.inputVal2);
        that.request3(that.data.inputVal3);
      }
      else if (that.data.curIdx2 == 1 && that.data.inputVal2.length != 0) {
        // that.request5(that.data.inputVal2);
        that.request4(that.data.inputVal);
        that.request5(that.data.inputVal2);
        that.request6(that.data.inputVal3);
      }
      else if (that.data.curIdx2 == 2 && that.data.inputVal2.length == 0) {
        // that.request3(that.data.inputVal3);
        that.request1(that.data.inputVal);
        that.request2(that.data.inputVal2);
        that.request3(that.data.inputVal3);
      }
      else if (that.data.curIdx2 == 2 && that.data.inputVal2.length != 0) {
        // that.request6(that.data.inputVal3);
        that.request4(that.data.inputVal);
        that.request5(that.data.inputVal2);
        that.request6(that.data.inputVal3);
      }
    }, 1000)
  },
  clickMe: function (e) {
    var that = this;
    // 获取事件绑定的当前组件
    var index = e.currentTarget.dataset.index;
    that.data.list[index].hiddenName = !that.data.list[index].hiddenName
    this.setData({
      list: that.data.list
    })
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      //inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  off_canvas: function () {
    this.data.open ? this.setData({ open: false }) : this.setData({ open: true });
  },
  openConfirm: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    var carrierName = this.data.carrierName;
    var allAccount = this.data.allAccount;
    var onlineAccount = this.data.onlineAccount;
    var offlineAccount = this.data.offlineAccount;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认解封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '解封中',
            mask: true,
          })
          wx.request({
            url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat ,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              wx.hideNavigationBarLoading() //完成停止加载
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2 : allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.showToast({
                  title: data.data.msg,
                  duration: 2000
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request1(that.data.inputVal);
                  that.request7(arr);
                }, 1000)
                
                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                // wx.showToast({
                //   title: data.data.msg,
                //   image: '../../images/error.png', 
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  openConfirm2: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认解封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '解封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              wx.hideNavigationBarLoading() //完成停止加载
              wx.stopPullDownRefresh() //停止下拉刷新  
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.showToast({
                  title: data.data.msg,
                  duration: 2000
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request4(that.data.inputVal);
                  that.request7(arr);
                }, 1000)
                // setTimeout(function () {
                //   that.setData({
                //     searchPageNum: 1,
                //     list: [],
                //     arr: [],
                //   });
                //   that.request4(that.data.inputVal);

                // }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                // wx.showToast({
                //   title: data.data.msg,
                //   image: '../../images/error.png', 
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  openConfirm3: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认解封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '解封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              wx.hideNavigationBarLoading() //完成停止加载
              wx.stopPullDownRefresh() //停止下拉刷新  
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.showToast({
                  title: data.data.msg,
                  duration: 2000
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request5(that.data.inputVal2);
                  that.request7(arr);
                }, 1000)
                // setTimeout(function () {
                //   that.setData({
                //     searchPageNum: 1,
                //     list2: [],
                //     arr2: [],
                //   });
                //   that.request5(that.data.inputVal2);

                // }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                // wx.showToast({
                //   title: data.data.msg,
                //   image: '../../images/error.png', 
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  openConfirm4: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认解封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '解封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/unlock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              wx.hideNavigationBarLoading() //完成停止加载
              wx.stopPullDownRefresh() //停止下拉刷新  
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.showToast({
                  title: data.data.msg,
                  duration: 2000
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request6(that.data.inputVal3);
                  that.request7(arr);
                }, 1000)
                // setTimeout(function () {
                //   that.setData({
                //     searchPageNum: 1,
                //     list3: [],
                //     arr3: [],
                //   });
                //   that.request6(that.data.inputVal3);

                // }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                // wx.showToast({
                //   title: data.data.msg,
                //   image: '../../images/error.png', 
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  closeConfirm: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    var carrierName = this.data.carrierName;
    var allAccount = this.data.allAccount;
    var onlineAccount = this.data.onlineAccount;
    var offlineAccount = this.data.offlineAccount;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认施封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '施封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if(res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                // wx.showToast({
                //   title: "施封成功",
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: "施封成功",
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request1(that.data.inputVal);
                  that.request7(arr);
                }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                wx.hideLoading();
                // wx.showToast({
                //   image: '../../images/error.png',
                //   title: data.data.msg,
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  closeConfirm2: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认施封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '施封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                // wx.showToast({
                //   title: "施封成功",
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: "施封成功",
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request4(that.data.inputVal);
                  that.request7(arr);
                }, 1000)
                // setTimeout(function () {
                //   that.setData({
                //     searchPageNum: 1,
                //     list: [],
                //     arr: [],
                //   });
                //   that.request4(that.data.inputVal);

                // }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                // wx.showToast({
                //   image: '../../images/error.png',
                //   title: data.data.msg,
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  closeConfirm3: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认施封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '施封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                // wx.showToast({
                //   title: "施封成功",
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: "施封成功",
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request5(that.data.inputVal2);
                  that.request7(arr);
                }, 1000)
                // setTimeout(function () {
                //   that.setData({
                //     searchPageNum: 1,
                //     list2: [],
                //     arr2: [],
                //   });
                //   that.request5(that.data.inputVal2);

                // }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                // wx.showToast({
                //   image: '../../images/error.png',
                //   title: data.data.msg,
                //   duration: 2000
                // });
                wx.hideLoading();
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  closeConfirm4: function (e) {
    var that = this;
    var token = wcache.get('token');
    var index = e.currentTarget.dataset.index;
    var lockCode = e.currentTarget.id;
    // var str = JSON.stringify(e._relatedInfo.anchorRelatedText);
    // var str2 = str.match(/SIM卡(\S*)时间/)[1];
    // var tr = str2.substring(0, str2.length-2);
    console.log(lockCode);
    wx.showModal({
      title: '提示',
      content: '确认施封吗?',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        wx.getLocation({
          success(res) {
            console.log(res)
            that.setData({
              lng: res.longitude,
              lat: res.latitude,
              // location: formatLocation(res.longitude, res.latitude)
            })
          }
        })
        if (res.confirm) {
          wx.showLoading({
            title: '施封中',
            mask: true,
          })
          wx.request({
            // url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode,
            url: urlPath + 'ztjk/lockstatus/lock?lockNum=' + lockCode + '&lng=' + that.data.lng + '&lat=' + that.data.lat,
            header: {
              'token': token
            },
            data: {
              // id: that.data.list[index].id
            },
            method: 'GET',
            success: function (data) {
              wx.hideLoading();
              // console.log(data);
              if (data.data.code == 0) {
                that.onPullDownRefresh()
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName,
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var allAccount2 = res.data.data.length
                      that.setData({
                        allAccount2: allAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });
                      // that.request7(allAccount2);
                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=1',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var onlineAccount2 = res.data.data.length
                      that.setData({
                        onlineAccount2: onlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                wx.request({
                  url: urlPath + 'ztjk/miniapp/deviceList?carrierName=' + carrierName + '&status=0',
                  // url: urlPath + 'ztjk/vec/page' , 
                  header: {
                    'token': token
                  },
                  data: {
                  },
                  method: 'GET',
                  success: function (res) {
                    if (res.data.code == 0) {
                      var offlineAccount2 = res.data.data.length
                      that.setData({
                        offlineAccount2: offlineAccount2
                        // tabs: ['全部' + '(' + allAccount + ')', '在线' + '(' + onlineAccount + ')', '离线' + '(' + offlineAccount + ')'],
                      });

                      wx.hideNavigationBarLoading() //完成停止加载
                      wx.stopPullDownRefresh() //停止下拉刷新
                    } else if (res.data.code == 401) {
                      wx.showToast({
                        title: '请重新登录',
                        icon: 'loading',
                        duration: 2000
                      }),
                        setTimeout(function () {
                          wx.redirectTo({
                            url: '/pages/login/login',
                          })
                        }, 2000)
                    } else if (res.data.code == 500){
                      wx.showToast({
                        title: res.data.msg,
                        icon: 'none',
                        duration: 2000
                      });
                    }
                  },
                  fail: function () {
                    that.setData({
                      searchLoading: true,
                      searchLoadingComplete: true
                    });
                    wx.showToast({
                      title: "服务器内部错误",
                      icon: 'none',
                      duration: 2000
                    });
                  }
                })
                // wx.showToast({
                //   title: "施封成功",
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: "施封成功",
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
                setTimeout(function () {
                  var arr = []
                  var data = {
                    'all': that.data.allAccount2, 'online': that.data.onlineAccount2, 'offline': that.data.offlineAccount2
                  }
                  that.setData({
                    searchPageNum: 1,
                    list: []
                  });
                  arr.push(data)
                  that.request6(that.data.inputVal3);
                  that.request7(arr);
                }, 1000)
                // setTimeout(function () {
                //   that.setData({
                //     searchPageNum: 1,
                //     list3: [],
                //     arr3: [],
                //   });
                //   that.request6(that.data.inputVal3);

                // }, 1000)

                // setTimeout(function () {
                //   that.data.list.splice(index, 1);
                //   that.setData({
                //     list: that.data.list
                //   });
                // }, 1000)
              } else if (data.data.code == 401) {
                wx.showToast({
                  title: '请重新登录',
                  icon: 'loading',
                  duration: 2000
                }),
                  setTimeout(function () {
                    wx.redirectTo({
                      url: '/pages/login/login',
                    })
                  }, 2000)
              } else if (data.data.code == 500){
                wx.hideLoading();
                // wx.showToast({
                //   image: '../../images/error.png',
                //   title: data.data.msg,
                //   duration: 2000
                // });
                wx.showModal({
                  title: '温馨提醒',
                  content: data.data.msg,
                  confirmText: "确定",
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                    }
                  },
                });
              }
            }
          })
        } else {
          console.log('用户点击辅助操作')
        }
      },
      fail: function () {
        wx.showToast({
          title: "服务器内部错误",
          icon: 'none',
          duration: 2000
        });
      }
    });
  },
  onShow: function () {
    // 100为导航栏swiper-tab 的高度
    this.setData({
      scrollHeight: wx.getSystemInfoSync().windowHeight - (wx.getSystemInfoSync().windowWidth / 750 * 100),
    });
    this.request(this.data.inputVal);
    // this.request1(this.data.inputVal1);
    // this.request2(this.data.inputVal2);
    // this.request3(this.data.inputVal3);
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  tabChange: function (e) {
    var index = e.target.id.replace('tabTag-', '');
    this.setData({ swiperCurrentIndex: index, tabCurrentIndex: index });
  },
  swiperChange: function (e) {
    var index = e.detail.current;
    this.setData({ tabCurrentIndex: index });
  },
  //每个选项滚动到底部
  scrollend: function (e) {
    //获取是哪个选项滚动到底？
    var index = e.currentTarget.dataset.scindex;
    console.log(index);
    //可以利用 tabs 携带的分类id 与服务器交互请求对应分类的数据
    console.log(this.data.tabs[index].id);
    //加载更多的演示
    //判断当前是否正在加载
    if (this.data.tabs[index].loadingType === 1) {
      return false;
    }
    //判断是否是最后一页
    if (this.data.tabs[index].page > 3) {
      this.data.tabs[index].loadingType = 2;//全部
      this.setData({ tabs: this.data.tabs });
      return;
    }
    this.data.tabs[index].loadingType = 1;//加载中
    this.setData({ tabs: this.data.tabs });
    //模拟延迟
    setTimeout(function () {
      _self.data.newsAll[index] = _self.data.newsAll[index].concat(news);
      _self.setData({ newsAll: _self.data.newsAll });
      //分页
      _self.data.tabs[index].page++;
      _self.data.tabs[index].loadingType = 0;//恢复加载状态
      _self.setData({ tabs: _self.data.tabs });
      //
    }, 1000);
  }
});

