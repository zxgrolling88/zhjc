const app = getApp()
let wcache = require("../../utils/wcache.js");
let request = require("../../utils/request2.js");
Page({
  data: {
    inputShowed: false,
    inputVal: "",
    list: [],
    searchPageNum: 1,   // 设置加载的第几次，默认是第一次
    reload: false,// 下拉刷新判断
    scrollHeight: 0
  },
  onLoad: function () {
    let that = this;
    this.setData({
      list: [],
      searchPageNum: 1,
      reload: true
    });
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          scrollHeight: res.windowHeight - 45
        });
      }
    });
    that.request();
  },
  request: function () {
    let that = this;
    let requestUrl = "/ztjk/deviceoprlog/page";
    let req_obj = {
      filters: { "groupOp": "AND", "rules": [{ "field": "device_code", "op": "cn", "data": that.data.inputVal }] },
      page: that.data.searchPageNum,
      limit: 10,
    }
    request.requestLoading(requestUrl, 'GET', req_obj, '正在加载中...', function (res) {
      //res就是我们请求接口返回的数据
      if (res.code == 0) {
        let _list = res.page.list;
        that.setData({
          list: that.data.reload ? _list : that.data.list.concat(_list),
          reload: false,
          searchPageNum: that.data.searchPageNum + 1
        });
      }
    }, function () {
      wx.showToast({
        title: '加载数据失败',
      })
    })

  },
  //筛点击搜索
  searchValueInput: function (e) {
    var value = e.detail.value;
    this.setData({
      inputVal: value,
    });
  },
  searchBtn: function (e) {
    var that = this;
    this.setData({
      searchPageNum: 1,
      list: []
    });
    that.request();
  },
  //滚动到底部触发事件
  searchScrollLower: function () {
    console.log("滚动到底部触发事件")
    this.request();
  },
  //下拉刷新
  onPullDownRefresh: function () {
    let that = this;
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //加载
    this.setData({
      searchPageNum: 1,
      reload: true
    });
    setTimeout(function () {
      that.setData({
        list: [],
      });
      that.request();
    }, 1000)
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  hideInput: function () {
    this.setData({
      //inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  //跳转到详情页
  infoJump: function (e) {
    let data = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '/pages/register-info/register-info?data=' + JSON.stringify(data)
    })
  },
  //跳转新增修改页
  addOrUpdateJump: function (e) {
    wx.navigateTo({
      url: '/pages/register-addOrUpdate/register-addOrUpdate'
    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log("滚动到底部触发事件2")
    this.request();
  },
  /**
  * 生命周期函数--监听页面显示
  */
  onShow: function () { }
});