// pages/updatePwd/updatePwd.js
const app = getApp()
let wcache = require("../../utils/wcache.js");
let base64encode = require("../../utils/base64.js");
let base64encode2 = require("../../utils/base64forchangePwd.js");
var url = getApp().globalData.url;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username:'',
    old: '',
    new: '',
    new2: '',
    info:{},
    newPassword: '',
    password: '',
    newPassword2: '',
    password2: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var username = wcache.get('username');
    let that = this;
    that.setData({
      username: username,
    });
  },
  // 整体保存
  formSubmit: function (e) {
    var that = this;
    that.savaRequest(e)
  },
  savaRequest: function (e) {
    var that = this;
    var token = wcache.get('token');
    var detail = e.detail.value;
    if (detail.old != "" && detail.new != '' && detail.new2 != '' && detail.new.length >=6 && detail.new == detail.new2) {
      that.data.old = detail.old
      that.data.new = detail.new
      that.data.new2 = detail.new2
      that.setData({
        requestLoading: false,
        password: detail.old,
        newPassword: detail.new,
      });
      // for (let i = 0; i < 3; i++) {
      //   that.setData({
      //     password2: base64encode.base64encode(that.data.password),
      //     newPassword2: base64encode.base64encode(that.data.newPassword)
      //   })
      // }
      var password2 = that.data.password;
      var newPassword2 = that.data.newPassword;
      for (let i = 0; i < 3; i++) {
          password2 = base64encode2.Base64.encode(password2),
          newPassword2 = base64encode2.Base64.encode(newPassword2)
 
      }

      that.setData({
        password2: password2,
        newPassword2: newPassword2
      })
      wx.request({
        // url: urlPath + '/zhatu/check/update' + '?lng=' + that.data.lng + '&lat=' + that.data.lat,
        url: url + 'ztjk/sys/user/password',
        header: {
          'token': token
        },
        data: {
          newPassword:that.data.newPassword2, 
          password: that.data.password2},
        method: 'POST',
        success: function (res) {
          if (res.data.code == 0) {
            that.setData({
              requestLoading: true
            });
            wx.showToast({
              title: "操作成功",
              icon: 'success',
              duration: 2000
            })
            setTimeout(function () {
              wx.clearStorageSync()
              wx.clearStorageSync()
              wx.reLaunch({
                url: '/pages/login/login'
              })
            }, 2000)

          } else if (res.data.code == 401) {
            that.setData({
              requestLoading: true
            });
            wx.showToast({
              title: '请重新登录',
              icon: 'loading',
              duration: 2000
            }),
              setTimeout(function () {
                wx.redirectTo({
                  url: '/pages/login/login',
                })
              }, 2000)
          } else {
            that.setData({
              requestLoading: true
            });
            wx.showToast({
              title: res.data.msg,
              icon: 'none',
              duration: 2000
            })
          }
        },
        fail: function () {
          that.setData({
            requestLoading: true
          });
          wx.showToast({
            title: "稍后再试",
            icon: 'none',
            duration: 2000
          });
        }
      })
    } else if (detail.new != detail.new2){
      wx.showToast({

        title: '两次密码不一样！',

        icon: 'none',

        duration: 1500

      })
    } else if (detail.new.length <6) {
      wx.showToast({

        title: '密码长度必须大于等于6位！',

        icon: 'none',

        duration: 1500

      })
    }else {
      wx.showToast({

        title: '请填写完整数据！',

        icon: 'none',

        duration: 1500

      })
    }

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})