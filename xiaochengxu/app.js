//app.js

App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []

    var _this = this
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        console.log(res)
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          _this.globalData.code = res.code

        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
    const updateManager = wx.getUpdateManager()

    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log(res.hasUpdate)
    })

    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function (res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })

    updateManager.onUpdateFailed(function () {
      // 新的版本下载失败
      wx.showModal({
        title: '更新提示',
        content: '新版本下载失败',
        showCancel: false
      })
    })


    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
              // 获取openid
              wx.request({
                url: _this.globalData._url + 'user/miniapp/userinfo',
                header: { "Content-Type": "application/x-www-form-urlencoded" },
                data: {
                  code: _this.globalData.code,
                  rawData: res.rawData,
                  signature: res.signature,
                  encryptedData: res.encryptedData,
                  iv: res.iv
                },
                method: 'POST',
                success: data => {
                  _this.globalData.openid = data.data.openid
                  if (this.openidCallback) {
                    this.openidCallback(data.data.openid)
                  }
                  _this.userBind(data.data.openid)
                }
              })
            }
          })
        }
      }
    })
  },
  userBind: function (wechatId) {
    let wcache = require("utils/wcache.js");
    let _this = this
    wx.request({
      url: _this.globalData._url + 'user/isbind',
      data: {
        'wechatId': wechatId,
        'bindType': 1
      },
      method: 'GET',
      success: function (res) {
        if (res.data.code != 0) {
          wx.showToast({
            title: '您还未绑定账号，请先绑定',
            icon: 'loading',
            duration: 2000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/login/login',
              })
            }, 2000)
        } else {
          wx.showToast({
            title: '您已绑定，自动登陆……',
            icon: 'none',
            duration: 1000
          }),
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/index/index',
              })
            }, 1000)

        }
      }
    })
  },
  globalData: {
    userInfo: null,
    code: "",
    openid: "",
    unionid: "",
    // url: 'https://zhgh.dacyun.com/zhgh-portal/',
    // url: 'https://stag-zhgh.dacyun.com/zhgh-portal/',
    url: 'https://www.jhyj56.com/',
    // _url: 'https://dcys.dacyun.com/zjdc-wechat/'
  },
  aData: {
    show: false
  }
})