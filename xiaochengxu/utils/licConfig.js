// 企业证照信息配置
const entp = [
  // "企业营业执照": "8010.200"
  {
    "catCd": "8010.200",
    "name": "企业营业执照",
    "vldTo": "请选择年-月-日",
    "vldToNM": "结束日期",
    "vldFrom": "",
    "vldFromNM": "开始日期",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见，如果三证合一企业，此处上传统一社会信用代码证",
    "licSave": true,
    "subItems": [{
      "rsrcCd": "8010.200.150",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "企业营业执照",
      "files": [],
      "licLoading": true
    }]
  },

  // "水路经营许可证":"8010.202"
  {
    "catCd": "8010.202",
    "name": "水路经营许可证",
    "licCd": "请选择年-月-日",
    "vldToNM": "结束日期",
    "vldFrom": "",
    "vldFromNM": "开始日期",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见，如果三证合一企业，此处上传统一社会信用代码证",
    "licSave": true,
    "subItems": [{
      "rsrcCd": "8010.201.150",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "水路经营许可证",
      "files": [],
      "licLoading": true
    }]
  }
]

// 人员证照信息配置
const pers = [
  // "身份证"："8010.400"
  {
    "licCatCd": "8010.400",
    "licCatNmCn": "身份证",
    "vldFrom": "请选择年-月-日",
    "vldTo": "请选择年-月-日",
    "licVldToNM": "身份证有效期",
    "licCd": null,
    "licCdNM": "",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见",
    "licSave": true,
    "subItems": [{
      "rsrcCd": "8010.400.150",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "身份证正面",
      "files": [],
      "licLoading": true
    }, {
      "rsrcCd": "8010.400.151",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "身份证反面",
      "files": [],
      "licLoading": true
    }]
  },

  // "劳动合同":"8010.401"
  {
    "licCatCd": "8010.401",
    "licCatNmCn": "劳动合同",
    "licVldTo": "请选择年-月-日",
    "licVldToNM": "劳动合同有效期",
    "licCd": null,
    "licCdNM": "",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见",
    "licSave": true,
    "subItems": [{
      "rsrcCd": "8010.401.150",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "劳动合同封面，含甲方乙方名称",
      "files": [],
      "licLoading": true
    }, {
      "rsrcCd": "8010.401.151",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "劳动合同内页，含劳动合约时间页",
      "files": [],
      "licLoading": true
    }, {
      "rsrcCd": "8010.401.152",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "劳动合同内页，含甲方乙方签字/敲章，含日期页",
      "files": [],
      "licLoading": true
    }]
  },

  // "驾驶证（驾驶员岗位必须）":"8010.402"
  {
    "licCatCd": "8010.402",
    "licCatNmCn": "驾驶证",
    "licVldTo": "请选择年-月-日",
    "licVldToNM": "审验有效期",
    "licCd": "",
    "licCdNM": "",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见",
    "licSave": true,
    "subItems": [{
        "rsrcCd": "8010.402.150",
        "url": '',
        "thumbnailUrl": '',
        "isModify": 0,
        "licNm": "驾驶证正本正面",
        "files": [],
        "licLoading": true
      },
      {
        "rsrcCd": "8010.402.151",
        "url": '',
        "thumbnailUrl": '',
        "isModify": 0,
        "licNm": "驾驶证正本反面",
        "files": [],
        "licLoading": true
      },
      {
        "rsrcCd": "8010.402.152",
        "url": '',
        "thumbnailUrl": '',
        "isModify": 0,
        "licNm": "驾驶证副本正面",
        "files": [],
        "licLoading": true
      },
      {
        "rsrcCd": "8010.402.153",
        "url": '',
        "thumbnailUrl": '',
        "isModify": 0,
        "licNm": "驾驶证副本反面",
        "files": [],
        "licLoading": true
      }
    ]
  },

  // "驾驶员从业资格证（驾驶员必须）":"8010.403"
  {
    "licCatCd": "8010.403",
    "licCatNmCn": "驾驶员从业资格证",
    "licVldTo": "请选择年-月-日",
    "licVldToNM": "有效期截止日期",
    "licCd": "",
    "licCdNM": "从业资格证号",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见，含从业资格证基本信息页",
    "licSave": true,
    "subItems": [{
      "rsrcCd": "8010.403.150",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "驾驶员从业资格证",
      "files": [],
      "licLoading": true
    }]
  },

  // "押运员从业资格证（押运员必须）":"8010.404"
  {
    "licCatCd": "8010.404",
    "licCatNmCn": "押运员从业资格证",
    "licVldTo": "请选择年-月-日",
    "licVldToNM": "有效期截止日期",
    "licCd": "",
    "licCdNM": "从业资格证号",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见，含从业资格证基本信息页",
    "licSave": true,
    "subItems": [{
      "rsrcCd": "8010.404.150",
      "url": '',
      "thumbnailUrl": '',
      "isModify": 0,
      "licNm": "押运员从业资格证",
      "files": [],
      "licLoading": true
    }]
  },

  // "安全责任状":"8010.405"
  {
    "licCatCd": "8010.405",
    "licCatNmCn": "安全责任状",
    "licVldTo": "请选择年-月-日",
    "licVldToNM": "有效期截止日期",
    "licCd": null,
    "licCdNM": "",
    "isModify": 0,
    "title": "要求：彩色扫描件或彩色照片，内容清晰可见",
    "licSave": true,
    "subItems": [{
        "rsrcCd": "8010.405.150",
        "url": '',
        "thumbnailUrl": '',
        "isModify": 0,
        "licNm": "含劳甲方乙方名称页",
        "files": [],
        "licLoading": true
      },
      {
        "rsrcCd": "8010.405.151",
        "url": '',
        "thumbnailUrl": '',
        "isModify": 0,
        "licNm": "含甲方乙方签字/敲章，含日期页",
        "files": [],
        "licLoading": true
      }
    ]
  }
]

export default {
  entp,
  pers
}