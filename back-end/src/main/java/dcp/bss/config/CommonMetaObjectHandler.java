package dcp.bss.config;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import dcp.bss.common.util.DateUtils;
import dcp.bss.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.shiro.SecurityUtils;

/**
 * MP自动填充类
 *
 * @author Administrator
 */
public class CommonMetaObjectHandler extends MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
//        if (metaObject.hasSetter("crtBy")) {
//            SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
//            if (metaObject.hasSetter("crtBy")) {
//                metaObject.setValue("crtBy", user.getUsername());
//            }
//
//            if (metaObject.hasSetter("updBy")) {
//                metaObject.setValue("updBy", user.getUsername());
//            }
//
//            if (metaObject.hasSetter("createBy")) {
//                metaObject.setValue("createBy", user.getUsername());
//            }
//
//            if (metaObject.hasSetter("updateBy")) {
//                metaObject.setValue("updateBy", user.getUsername());
//            }
//
//
//        }

        if (metaObject.hasSetter("crtTm")) {
            metaObject.setValue("crtTm", DateUtils.currentTimestamp());
        }

        if (metaObject.hasSetter("updTm")) {
            metaObject.setValue("updTm", DateUtils.currentTimestamp());
        }

        if (metaObject.hasSetter("createDate")) {
            metaObject.setValue("createDate", DateUtils.currentTimestamp());
        }

        if (metaObject.hasSetter("updateDate")) {
            metaObject.setValue("updateDate", DateUtils.currentTimestamp());
        }

        if (metaObject.hasSetter("editFlag")) {
            metaObject.setValue("editFlag", "1100.10");
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {

//        if (metaObject.hasSetter("crtBy")) {
//			SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
//
//			if (null != user) {
//				if (metaObject.hasSetter("updBy")) {
//					metaObject.setValue("updBy", user.getUsername());
//				}
//
//				if (metaObject.hasSetter("updateBy")) {
//					metaObject.setValue("updateBy", user.getUsername());
//				}
//			}

            if (metaObject.hasSetter("updTm")) {
                metaObject.setValue("updTm", DateUtils.currentTimestamp());
            }

            if (metaObject.hasSetter("updateDate")) {
                metaObject.setValue("updateDate", DateUtils.currentTimestamp());
            }

        }

  //  }

}
