/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package dcp.bss.config;

import com.baomidou.mybatisplus.entity.GlobalConfiguration;
import com.baomidou.mybatisplus.mapper.LogicSqlInjector;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.plugins.SqlExplainInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * mybatis-plus配置
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2.0.0 2018-02-05
 */
@Configuration
public class MybatisPlusConfig {

//    /**
//     * mybatis-plus SQL执行效率插件【生产环境建议关闭】
//     */
//    @Bean
//    //设置dev test环境开启
//    @Profile({"dev","stag"})
//    public PerformanceInterceptor performanceInterceptor() {
//        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
//        //maxTime SQL 执行最大时长，超过自动停止运行，有助于发现问题
//        performanceInterceptor.setMaxTime(5000);
//        //SQL语句是否格式化
//        performanceInterceptor.setFormat(true);
//        return performanceInterceptor;
//    }

//    /**
//     * 执行分析插件
//     */
//    @Bean
//    public SqlExplainInterceptor sqlExplainInterceptor() {
//        SqlExplainInterceptor sqlExplainInterceptor = new SqlExplainInterceptor();
//        // SQL 执行分析拦截器 stopProceed 发现全表执行 delete update 是否停止运行
//        sqlExplainInterceptor.setStopProceed(Boolean.TRUE);
//        return sqlExplainInterceptor;
//    }
    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
