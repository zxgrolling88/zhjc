package dcp.bss.config;

import dcp.bss.modules.bussiness.service.impl.MessageReceiver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 11:13
 */
@Configuration
public class RedisConfig {


    /**
     * redis消息监听器容器
     * 可以添加多个监听不同话题的redis监听器，只需要把消息监听器和相应的消息订阅处理器绑定，该消息监听器
     * 通过反射技术调用消息订阅处理器的相关方法进行一些业务处理
     * @param connectionFactory
     * @param listenerAdapter
     * @return
     */
    @Bean //相当于xml中的bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter lockListenerAdapter,MessageListenerAdapter gpsListenerAdapter) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(lockListenerAdapter, new PatternTopic("lock"));
        container.addMessageListener(gpsListenerAdapter, new PatternTopic("gps"));
        return container;
    }

    /**
     * 消息监听器适配器，绑定消息处理器，利用反射技术调用消息处理器的业务方法
     * @param receiver
     * @return
     */
    @Bean
    MessageListenerAdapter gpsListenerAdapter(MessageReceiver receiver) {
        return new MessageListenerAdapter(receiver, "gpsMessage");
    }


    @Bean
    MessageListenerAdapter lockListenerAdapter(MessageReceiver receiver) {
        return new MessageListenerAdapter(receiver, "lockMessage");
    }
    /**redis 读取内容的template */
    @Bean
    StringRedisTemplate template(RedisConnectionFactory connectionFactory) {
        return new StringRedisTemplate(connectionFactory);
    }


}
