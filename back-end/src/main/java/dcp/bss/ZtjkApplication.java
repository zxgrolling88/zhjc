package dcp.bss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 系统启动入口
 * @author Administrator
 */
@SpringBootApplication
@EnableAsync
@EnableScheduling
public class ZtjkApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ZtjkApplication.class, args);
    }

}