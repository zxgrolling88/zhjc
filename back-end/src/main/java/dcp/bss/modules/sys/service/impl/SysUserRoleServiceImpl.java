package dcp.bss.modules.sys.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.modules.sys.dao.SysUserRoleDao;
import dcp.bss.modules.sys.entity.SysUserRoleEntity;
import dcp.bss.modules.sys.service.SysUserRoleService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



/**
 * 用户与角色对应关系
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:45:48
 */
@Service("sysUserRoleService")
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleDao, SysUserRoleEntity> implements SysUserRoleService {

	@Override
	public void saveOrUpdate(Long userId, List<Long> roleIdList) {
		//先删除用户与角色关系
		HashMap<String ,Object> map = new HashMap();
		map.put("user_id", userId);
		this.deleteByMap(map);

		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}

		//保存用户与角色关系
		List<SysUserRoleEntity> list = new ArrayList<>(roleIdList.size());
		for(Long roleId : roleIdList){
			SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
			sysUserRoleEntity.setUserId(userId);
			sysUserRoleEntity.setRoleId(roleId);

			list.add(sysUserRoleEntity);
		}
		this.insertBatch(list);
	}

	@Override
	public List<Long> queryRoleIdList(Long userId) {
		return baseMapper.queryRoleIdList(userId);
	}

	@Override
	public List<String> queryEntpRoleNameList(Long userId) {
		return baseMapper.queryEntpRoleNameList(userId);
	}

	@Override
	public List<String> queryRoleNameList(Long userId) {
		return baseMapper.queryRoleNameList(userId);
	}
	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}
}
