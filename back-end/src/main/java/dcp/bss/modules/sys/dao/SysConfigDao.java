package dcp.bss.modules.sys.dao;

import dcp.bss.modules.sys.entity.SysConfigEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 系统配置信息表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-26 13:02:42
 */
@Mapper
@Repository
public interface SysConfigDao extends BaseMapper<SysConfigEntity> {
	
}
