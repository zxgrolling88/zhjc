package dcp.bss.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.sys.entity.SysDictEntity;
import dcp.bss.modules.sys.service.SysDictService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-18 14:44:09
 */
@RestController
@RequestMapping("sys/dict")
@Slf4j
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

    /**
         * 列表
         */
    @RequestMapping("/list")
   // @RequiresPermissions("sys:sysdict:list")
    public R list(QueryViewVo<SysDictEntity> queryViewVo) {
        PageUtils page = sysDictService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("sys:sysdict:info")
    public R info(@PathVariable("id") Long id){
			SysDictEntity sysDict = sysDictService.selectById(id);

        return R.ok().put("sysDict", sysDict);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("sys:sysdict:save")
    public R save(@RequestBody SysDictEntity sysDict){
			sysDictService.insert(sysDict);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("sys:sysdict:update")
    public R update(@RequestBody SysDictEntity sysDict){
			sysDictService.updateById(sysDict);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("sys:sysdict:delete")
    public R delete(@RequestBody Long[] ids){
			sysDictService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
	 * 导出数据到excel
	 *
	 * @param response
	 * @param queryViewVo
	 * @return
	 */
    @RequestMapping("/download")
  //  @RequiresPermissions("sys:sysdict:download")
    public void download(HttpServletResponse response, QueryViewVo<SysDictEntity> queryViewVo) {
        try {
            List<SysDictEntity> download = sysDictService.download(queryViewVo);
            XSSFWorkbook sfworkbook = new XSSFWorkbook();
            ExcelUtil excelUtil = new ExcelUtil();
            SXSSFWorkbook createWorkbook = excelUtil.createWorkbook(sfworkbook);
            ExcelUtil.WriteExcel<SysDictEntity> writeexcel = new  ExcelUtil.WriteExcel<SysDictEntity>() {

                @Override
                public void writeHead(XSSFRow contentxfrow) {
                                            XSSFCell createCell1 = contentxfrow.createCell(0);
                        createCell1.setCellValue("");
                                            XSSFCell createCell2 = contentxfrow.createCell(1);
                        createCell2.setCellValue("");
                                            XSSFCell createCell3 = contentxfrow.createCell(2);
                        createCell3.setCellValue("");
                                            XSSFCell createCell4 = contentxfrow.createCell(3);
                        createCell4.setCellValue("");
                                    }

                @Override
                public void write(XSSFRow contentxfrow, SysDictEntity t) {
                                            XSSFCell createCell1 = contentxfrow.createCell(0);
                        createCell1.setCellValue(t.getId());
                                            XSSFCell createCell2 = contentxfrow.createCell(1);
                        createCell2.setCellValue(t.getCd());
                                            XSSFCell createCell3 = contentxfrow.createCell(2);
                        createCell3.setCellValue(t.getNmEn());
                                            XSSFCell createCell4 = contentxfrow.createCell(3);
                        createCell4.setCellValue(t.getNmCn());
                                    }
            };
            excelUtil.createHead(sfworkbook, createWorkbook, writeexcel);
            excelUtil.writeXlsFile(sfworkbook, createWorkbook, download, writeexcel);
            /**
             * 保存文件
             */
            String saveXlsFile = excelUtil.saveXlsFile(sfworkbook);
            excelUtil.sendExcel(response, saveXlsFile);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }


}
