package dcp.bss.modules.sys.dao;

import dcp.bss.modules.sys.entity.SysDictEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-18 14:44:09
 */
@Mapper
@Repository
public interface SysDictDao extends BaseMapper<SysDictEntity> {
	
}
