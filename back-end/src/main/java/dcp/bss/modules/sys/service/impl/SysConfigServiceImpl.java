package dcp.bss.modules.sys.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.sys.dao.SysConfigDao;
import dcp.bss.modules.sys.entity.SysConfigEntity;
import dcp.bss.modules.sys.service.SysConfigService;


@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfigEntity> implements SysConfigService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysConfigEntity> page = this.selectPage(
                new Query<SysConfigEntity>(params).getPage(),
                new EntityWrapper<SysConfigEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<SysConfigEntity> queryViewVo) {
        Page<SysConfigEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<SysConfigEntity> download(QueryViewVo<SysConfigEntity> queryViewVo) {
        List<SysConfigEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
