package dcp.bss.modules.sys.controller;

import dcp.bss.common.util.Base64Util;
import dcp.bss.modules.sys.service.SysUserService;

import dcp.bss.common.annotations.Log;
import dcp.bss.common.util.PwdUtil;
import dcp.bss.common.util.Constant;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;
import dcp.bss.web.validator.Assert;
import dcp.bss.web.validator.ValidatorUtils;
import dcp.bss.web.validator.group.AddGroup;
import dcp.bss.web.validator.group.UpdateGroup;
import dcp.bss.modules.sys.entity.SysUserEntity;
import dcp.bss.modules.sys.form.PasswordForm;
import dcp.bss.modules.sys.service.SysUserRoleService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年10月31日 上午10:40:10
 */
@RestController
@RequestMapping("sys/user")
public class SysUserController extends AbstractController {


	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;

	/**
	 * 修改登录用户密码
	 */
	@RequestMapping("password")
	public R updPassword(@RequestBody PasswordForm form){

		String pwd = null;
		String newPwd = null;
		try {
			pwd=Base64Util.decode(form.getPassword());
			pwd=Base64Util.decode(pwd);
			pwd=Base64Util.decode(pwd);
			newPwd=Base64Util.decode(form.getNewPassword());
			newPwd=Base64Util.decode(newPwd);
			newPwd=Base64Util.decode(newPwd);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.isBlank(newPwd, "新密码不为能空");
		
		if (!PwdUtil.match(newPwd)) {
			return R.error("密码长度大于8位, 且至少包含数字、大写字母、小写字母及特殊字符的三种或三种以上");
		}


		//sha256加密
		String password = new Sha256Hash(pwd, getUser().getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(newPwd, getUser().getSalt()).toHex();

		//更新密码
		boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}

		return R.ok();
	}
	/**
	 * 所有用户列表
	 */
	@RequestMapping("list")
	@RequiresPermissions("sys:user:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysUserService.queryPage(params);
		return R.ok().put("page", page);
	}
	
	/**
	 * 所有用户列表
	 */
	@RequestMapping("list2")
	@RequiresPermissions("sys:user:list2")
	public R list2(@RequestParam Map<String, Object> params){
		PageUtils page = sysUserService.queryPage(params);
		return R.ok().put("page", page);
	}

	/**
	 * 获取登录的用户信息
	 */
	@RequestMapping("info")
	public R info(){
		return R.ok().put("user", getUser());
	}
	/**
	 * 用户信息
	 */
	@RequestMapping("info/{userId}")
	@RequiresPermissions("sys:user:info")
	public R info(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.selectById(userId);

		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);

		return R.ok().put("user", user);
	}

	/**
	 * 保存用户
	 */
	@RequestMapping("save")
	@RequiresPermissions("sys:user:save")
	public R save(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, AddGroup.class);
		user.setCreateUserId(getUserId());

		//sha256加密
		String newPassword = new Sha256Hash(user.getPassword(), getUser().getSalt()).toHex();
		user.setPassword(newPassword);
		sysUserService.insert(user);
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
		return R.ok();
	}

	/**
	 * 修改用户
	 */
	@RequestMapping("update")
	@RequiresPermissions("sys:user:update")
	public R update(@RequestBody SysUserEntity user){
		ValidatorUtils.validateEntity(user, UpdateGroup.class);
		user.setCreateUserId(getUserId());
		String newPassword = new Sha256Hash(user.getPassword(), getUser().getSalt()).toHex();
		user.setPassword(newPassword);
		sysUserService.updateById(user);
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
		return R.ok();
	}

	/**
	 * 删除用户
	 */
	@Log("删除用户")
	@RequestMapping("delete")
	@RequiresPermissions("sys:user:delete")
	public R delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return R.error("系统管理员不能删除");
		}

		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}

		sysUserService.deleteBatch(userIds);

		return R.ok();
	}

}
