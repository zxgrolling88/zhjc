package dcp.bss.modules.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-18 14:44:09
 */
@Data
@TableName("sys_dict")
public class SysDictEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private String cd;
	/**
	 * 
	 */
	private String nmEn;
	/**
	 * 
	 */
	private String nmCn;


}
