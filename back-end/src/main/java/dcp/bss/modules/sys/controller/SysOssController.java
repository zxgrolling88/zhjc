//package dcp.bss.modules.sys.controller;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import dcp.bss.web.exception.RRException;
//import dcp.bss.common.util.HttpClientUtils;
//import dcp.bss.common.util.R;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.File;
//import java.io.IOException;
//import java.util.*;
//
//
///**
// * 文件上传
// *
// * @author zjdc
// * @email 631079326@qq.com
// * @date 2017-03-25 12:13:26
// */
//@RestController
//@RequestMapping("sys/oss")
//public class SysOssController {
//    @Value("${BUCKET_NAME}")
//    private String bucketName;
//    @Value("${APP_OSS_URL}")
//    private String appOssUrl;
//
//    @Value("${ossUrl}")
//    private String ossUrl;
//
//    private Logger logger = LoggerFactory.getLogger(SysOssController.class);
//    public static final long FILE_SIZE = 10 * 1024 * 1024L;
//
//    /**
//     * 上传文件
//     */
//    @RequestMapping("/upload")
//    public R upload(@RequestParam("file") MultipartFile file) {
//        try {
//            if (file.isEmpty()) {
//                throw new RRException("上传文件不能为空");
//            }
//            // 如果文件大小大于限制
//            if (file.getSize() > FILE_SIZE) {
//                throw new RRException("图片过大,请选择小于5M的图片");
//            }
//            Map<String, String> param = new HashMap<>();
//            param.put("fileServerHost", appOssUrl);
//            param.put("bucketName", bucketName);
//            param.put("isWaterMark", "true");
//            // 获取文件名
//            String fileName = file.getOriginalFilename();
//            // 获取文件后缀
//            String prefix = fileName.substring(fileName.lastIndexOf("."));
//            final File f = File.createTempFile(UUID.randomUUID().toString(), prefix);
//            file.transferTo(f);
//            List<File> files = new ArrayList<File>();
//            files.add(f);
//            String result = HttpClientUtils.uploadFileImpl(ossUrl, files,  param);
//            JSONArray jsonArray = JSON.parseArray(result);
//            return R.ok().put("data", jsonArray);
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//            throw new RRException("上传文件不能为空，请选择文件上传！");
//        }
//
//    }
//
//    /**
//     * 上传多文件
//     */
//
//    @RequestMapping("/upload/multi")
//    public R uploadMulti(MultipartFile[] file) throws Exception {
//        Map<String, String> param = new HashMap<>();
//        param.put("fileServerHost", appOssUrl);
//        param.put("bucketName", bucketName);
//        param.put("isWaterMark", "true");
//
//        if (file != null && file.length > 0) {
//            List<File> files = new ArrayList<File>();
//            for (int i = 0; i < file.length; i++) {
//                MultipartFile item = file[i];
//                String fileName = item.getOriginalFilename();
//                // 获取文件后缀
//                String prefix = fileName.substring(fileName.lastIndexOf("."));
//                final File f = File.createTempFile(UUID.randomUUID().toString(), prefix);
//                item.transferTo(f);
//                files.add(f);
//            }
//            String result = HttpClientUtils.uploadFileImpl(ossUrl, files,  param);
//            JSONArray jsonArray = JSON.parseArray(result);
//            return R.ok().put("data", jsonArray);
//        } else {
//            throw new RRException("上传文件不能为空，请选择文件上传！");
//        }
//
//
//    }
//
//
//}
