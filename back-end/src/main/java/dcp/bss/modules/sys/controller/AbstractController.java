package dcp.bss.modules.sys.controller;

import dcp.bss.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;

/**
 * Controller公共组件
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月9日 下午9:42:26
 */
public abstract class AbstractController {

	protected SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}


	/**
	 * 获取当前登录用户所属企业
	 * @return {String}
	 */
	public String getIpName() {
		return this.getUser().getIpName();
	}
}
