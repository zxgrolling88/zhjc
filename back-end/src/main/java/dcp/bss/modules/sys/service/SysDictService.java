package dcp.bss.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.sys.entity.SysDictEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-18 14:44:09
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<SysDictEntity> queryViewVo);

    List<SysDictEntity> download(QueryViewVo<SysDictEntity> queryViewVo);
}

