package dcp.bss.modules.sys.service.impl;

import dcp.bss.query.QueryViewVo;
import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.modules.sys.dao.SysDictDao;
import dcp.bss.modules.sys.entity.SysDictEntity;
import dcp.bss.modules.sys.service.SysDictService;


/**
 * @author Administrator
 */
@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictDao, SysDictEntity> implements SysDictService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysDictEntity> page = this.selectPage(
                new Query<SysDictEntity>(params).getPage(),
                new EntityWrapper<SysDictEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<SysDictEntity> queryViewVo) {
        Page<SysDictEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<SysDictEntity> download(QueryViewVo<SysDictEntity> queryViewVo) {
        List<SysDictEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
