package dcp.bss.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.sys.entity.SysConfigEntity;
import dcp.bss.modules.sys.service.SysConfigService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 系统配置信息表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-26 13:02:42
 */
@RestController
@RequestMapping("sysconfig")
@Slf4j
public class SysConfigController {
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("sys:sysconfig:list")
    public R list(QueryViewVo<SysConfigEntity> queryViewVo) {
        PageUtils page = sysConfigService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("sys:sysconfig:info")
    public R info(@PathVariable("id") Long id) {
        SysConfigEntity sysConfig = sysConfigService.selectById(id);

        return R.ok().put("sysConfig", sysConfig);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("sys:sysconfig:save")
    public R save(@RequestBody SysConfigEntity sysConfig) {
        sysConfigService.insert(sysConfig);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("sys:sysconfig:update")
    public R update(@RequestBody SysConfigEntity sysConfig) {
        sysConfigService.updateById(sysConfig);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("sys:sysconfig:delete")
    public R delete(@RequestBody Long[] ids) {
        sysConfigService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
     * 导出数据到excel
     *
     * @param response
     * @param queryViewVo
     * @return
     */
    @RequestMapping("/download")
    //  @RequiresPermissions("sys:sysconfig:download")
    public void download(HttpServletResponse response, QueryViewVo<SysConfigEntity> queryViewVo) {
        try {
            List<SysConfigEntity> download = sysConfigService.download(queryViewVo);
            XSSFWorkbook sfworkbook = new XSSFWorkbook();
            ExcelUtil excelUtil = new ExcelUtil();
            SXSSFWorkbook createWorkbook = excelUtil.createWorkbook(sfworkbook);
            ExcelUtil.WriteExcel<SysConfigEntity> writeexcel = new ExcelUtil.WriteExcel<SysConfigEntity>() {

                @Override
                public void writeHead(XSSFRow contentxfrow) {
                    XSSFCell createCell1 = contentxfrow.createCell(0);
                    createCell1.setCellValue("");
                    XSSFCell createCell2 = contentxfrow.createCell(1);
                    createCell2.setCellValue("key");
                    XSSFCell createCell3 = contentxfrow.createCell(2);
                    createCell3.setCellValue("value");
                    XSSFCell createCell4 = contentxfrow.createCell(3);
                    createCell4.setCellValue("状态   0：隐藏   1：显示");
                    XSSFCell createCell5 = contentxfrow.createCell(4);
                    createCell5.setCellValue("备注");
                }

                @Override
                public void write(XSSFRow contentxfrow, SysConfigEntity t) {
                    XSSFCell createCell1 = contentxfrow.createCell(0);
                    createCell1.setCellValue(t.getId());
                    XSSFCell createCell2 = contentxfrow.createCell(1);
                    createCell2.setCellValue(t.getParamKey());
                    XSSFCell createCell3 = contentxfrow.createCell(2);
                    createCell3.setCellValue(t.getParamValue());
                    XSSFCell createCell4 = contentxfrow.createCell(3);
                    createCell4.setCellValue(t.getStatus());
                    XSSFCell createCell5 = contentxfrow.createCell(4);
                    createCell5.setCellValue(t.getRemark());
                }
            };
            excelUtil.createHead(sfworkbook, createWorkbook, writeexcel);
            excelUtil.writeXlsFile(sfworkbook, createWorkbook, download, writeexcel);
            /**
             * 保存文件
             */
            String saveXlsFile = excelUtil.saveXlsFile(sfworkbook);
            excelUtil.sendExcel(response, saveXlsFile);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }


}
