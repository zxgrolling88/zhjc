/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package dcp.bss.modules.sys.service.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import dcp.bss.common.util.DateUtils;
import dcp.bss.common.util.IPUtils;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.modules.sys.entity.SysUserEntity;
import dcp.bss.modules.sys.dao.SysLogDao;
import dcp.bss.modules.sys.entity.SysLogEntity;
import dcp.bss.modules.sys.service.SysLogService;


/**
 * @author Administrator
 */
@Service("sysLogService")
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
		String operation = (String)params.get("operation");
        Page<SysLogEntity> page = this.selectPage(
            new Query<SysLogEntity>(params).getPage(),
            new EntityWrapper<SysLogEntity>().like(StringUtils.isNotBlank(key),"username", key)
					.like(StringUtils.isNotBlank(operation),"operation", operation)
					.orderBy("create_date",false)
        );

        return new PageUtils(page);
    }

    @Async
	@Override
	public void addLoginLog(HttpServletRequest request, String username) {
    	//String username = ((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).getUsername();
		SysLogEntity sysLog = new SysLogEntity();
		sysLog.setCreateDate(DateUtils.getCurrentTime());
		sysLog.setIp(IPUtils.getIpAddr(request));
		sysLog.setMethod("login");
		sysLog.setOperation("系统登陆");
		sysLog.setUsername(username);
		this.insert(sysLog);
	}

    @Async
	@Override
	public void addLogoutLog(HttpServletRequest request) {
    	String username = ((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).getUsername();
		SysLogEntity sysLog = new SysLogEntity();
		sysLog.setCreateDate(DateUtils.getCurrentTime());
		sysLog.setIp(IPUtils.getIpAddr(request));
		sysLog.setMethod("logout");
		sysLog.setOperation("系统登出");
		sysLog.setUsername(username);
		this.insert(sysLog);
		
	}
}
