package dcp.bss.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.sys.entity.SysConfigEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * 系统配置信息表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-07-26 13:02:42
 */
public interface SysConfigService extends IService<SysConfigEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<SysConfigEntity> queryViewVo);

    List<SysConfigEntity> download(QueryViewVo<SysConfigEntity> queryViewVo);
}

