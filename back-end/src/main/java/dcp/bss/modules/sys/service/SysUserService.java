package dcp.bss.modules.sys.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.sys.entity.SysUserEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


/**
 * 系统用户
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年9月18日 上午9:43:39
 */
public interface SysUserService extends IService<SysUserEntity> {


	PageUtils queryPage(Map<String, Object> params);

	/**
	 * 根据id查询用户
	 * @param id
	 */
	List<SysUserEntity> queryByid(Long id);


	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);


	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

    void saveLoginTime(HttpServletRequest request,Long userId, String username);

	void deleteBatch(Long[] userId);

	boolean updatePassword(Long userId, String password, String newPassword);
}
