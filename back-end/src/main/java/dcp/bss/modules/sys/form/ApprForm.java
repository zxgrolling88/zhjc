package dcp.bss.modules.sys.form;

import lombok.Data;

/**
 * @author Administrator
 */
@Data
public class ApprForm {

    private Long entityPk;        //审核对象主键
    private String entityDesc;    //审核对象描述
    private Long licPk;           //证照主键
    private String catCd;         //审核类型
    private String statCd;        //审核状态
    private String desc;          //审核描述
    
    private String catNmCn;       //后台获取，前台不需要传
    private String statNmCn;      //后台获取，前台不需要传
    
	
}
