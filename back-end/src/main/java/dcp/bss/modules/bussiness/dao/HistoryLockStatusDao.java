package dcp.bss.modules.bussiness.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import dcp.bss.modules.bussiness.entity.HistoryLockStatusEntity;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Mapper
@Repository
public interface HistoryLockStatusDao extends BaseMapper<HistoryLockStatusEntity> {
	
}
