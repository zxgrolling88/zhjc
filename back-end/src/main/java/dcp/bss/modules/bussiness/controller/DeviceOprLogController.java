package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.DeviceOprLogEntity;
import dcp.bss.modules.bussiness.service.DeviceOprLogService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("deviceoprlog")
@Slf4j
public class DeviceOprLogController {
    @Autowired
    private DeviceOprLogService deviceOprLogService;

    /**
         * 列表
         */
    @RequestMapping("/page")
   // @RequiresPermissions("bussiness:deviceoprlog:list")
    public R list(QueryViewVo<DeviceOprLogEntity> queryViewVo) {
        PageUtils page = deviceOprLogService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("bussiness:deviceoprlog:info")
    public R info(@PathVariable("id") Long id){
			DeviceOprLogEntity deviceOprLog = deviceOprLogService.selectById(id);

        return R.ok().put("deviceOprLog", deviceOprLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("bussiness:deviceoprlog:save")
    public R save(@RequestBody DeviceOprLogEntity deviceOprLog){
			deviceOprLogService.insert(deviceOprLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("bussiness:deviceoprlog:update")
    public R update(@RequestBody DeviceOprLogEntity deviceOprLog){
			deviceOprLogService.updateById(deviceOprLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("bussiness:deviceoprlog:delete")
    public R delete(@RequestBody Long[] ids){
			deviceOprLogService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
	 * 导出数据到excel
	 *
	 * @param response
	 * @param queryViewVo
	 * @return
	 */
    @RequestMapping("/download")
  //  @RequiresPermissions("bussiness:deviceoprlog:download")
    public void download(HttpServletResponse response, QueryViewVo<DeviceOprLogEntity> queryViewVo) {
        try {
            List<DeviceOprLogEntity> download = deviceOprLogService.download(queryViewVo);
            XSSFWorkbook sfworkbook = new XSSFWorkbook();
            ExcelUtil excelUtil = new ExcelUtil();
            SXSSFWorkbook createWorkbook = excelUtil.createWorkbook(sfworkbook);
            ExcelUtil.WriteExcel<DeviceOprLogEntity> writeexcel = new  ExcelUtil.WriteExcel<DeviceOprLogEntity>() {

                @Override
                public void writeHead(XSSFRow contentxfrow) {
                                            XSSFCell createCell3 = contentxfrow.createCell(1);
                        createCell3.setCellValue("设备编号");
                                            XSSFCell createCell4 = contentxfrow.createCell(2);
                        createCell4.setCellValue("操作类型");
                                            XSSFCell createCell5 = contentxfrow.createCell(3);
                        createCell5.setCellValue("操作人");
                                            XSSFCell createCell6 = contentxfrow.createCell(4);
                        createCell6.setCellValue("操作时间");
                                    }

                @Override
                public void write(XSSFRow contentxfrow, DeviceOprLogEntity t) {
                                            XSSFCell createCell3 = contentxfrow.createCell(1);
                        createCell3.setCellValue(t.getDeviceCode());
                                            XSSFCell createCell4 = contentxfrow.createCell(2);
                        createCell4.setCellValue(t.getOprType());
                                            XSSFCell createCell5 = contentxfrow.createCell(3);
                        createCell5.setCellValue(t.getOprator());
                                            XSSFCell createCell6 = contentxfrow.createCell(4);
                        createCell6.setCellValue(t.getOprTm());
                                    }
            };
            excelUtil.createHead(sfworkbook, createWorkbook, writeexcel);
            excelUtil.writeXlsFile(sfworkbook, createWorkbook, download, writeexcel);
            /**
             * 保存文件
             */
            String saveXlsFile = excelUtil.saveXlsFile(sfworkbook);
            excelUtil.sendExcel(response, saveXlsFile);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }


}
