package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.PersEntity;
import dcp.bss.modules.bussiness.service.PersService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 人员信息表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("pers")
@Slf4j
public class PersController {
    @Autowired
    private PersService persService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("bussiness:pers:list")
    public R list(QueryViewVo<PersEntity> queryViewVo) {
        PageUtils page = persService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:pers:info")
    public R info(@PathVariable("id") Long id) {
        PersEntity pers = persService.selectById(id);

        return R.ok().put("pers", pers);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:pers:save")
    public R save(@RequestBody PersEntity pers) {
        persService.insert(pers);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:pers:update")
    public R update(@RequestBody PersEntity pers) {
        persService.updateById(pers);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:pers:delete")
    public R delete(@RequestBody Long[] ids) {
        persService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }




}
