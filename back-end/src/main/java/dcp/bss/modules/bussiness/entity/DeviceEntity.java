package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_device")
public class DeviceEntity extends  BaseEntity{

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 设备编码
	 */
	private String code;
	/**
	 * 设备名称
	 */
	private String name;

	/**
	 * 设备名称
	 */
	private String password;
	/**
	 * 车辆主键
	 */
	private Long vecPk;
	/**
	 * 车牌号
	 */
	private String vecNo;
	/**
	 * 设备类型
	 */
	private String catNmCn;
	/**
	 * 分类编码
	 */
	private String catCd;



}
