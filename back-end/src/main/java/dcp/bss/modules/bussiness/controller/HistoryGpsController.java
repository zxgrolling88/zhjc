package dcp.bss.modules.bussiness.controller;

import java.util.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import dcp.bss.common.model.Gps;
import dcp.bss.common.model.GpsQuery;
import dcp.bss.common.util.CalculateUtil;
import dcp.bss.common.util.PositionUtil;
import dcp.bss.modules.bussiness.dao.HistoryLockStatusDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import dcp.bss.modules.bussiness.entity.HistoryLockStatusEntity;
import dcp.bss.common.util.R;

/**
 * GPS历史表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("track")
@Slf4j
public class HistoryGpsController {
    @Autowired
    private HistoryLockStatusDao historyLockStatusDao;
    private int maxTimeDif = 300 * 1000;



    /**
     * 信息
     */
    @RequestMapping("/list")
    // @RequiresPermissions("bussiness:historygps:info")
    public R list(@RequestBody GpsQuery gpsQuery) {
        List<HistoryLockStatusEntity> gpsInfoList = historyLockStatusDao.selectList(
                new EntityWrapper<HistoryLockStatusEntity>().eq("vec_no", gpsQuery.getVecNo()).
                        ge("gps_time", gpsQuery.getBeginTime()).le("gps_time", gpsQuery.getEndTime()));

        List<HistoryLockStatusEntity> runPointList = new ArrayList<>(gpsInfoList.size());
        List<HistoryLockStatusEntity> pausePointList = new ArrayList<>(gpsInfoList.size());
        /**
         * 初始化各变量初值 当前时间firstTime 当前经度nowfx 当前纬度nowfy 暂停开始时间startPause
         * 暂停结束时间endPause 暂停持续时间lasttime 暂停持续时间标志last 总路程sum 距离dis
         */
        Date firstTime = null;
        double nowfx = -1;
        double nowfy = -1;
        Date startPause = null;
        Date endPause = null;
        long lasttime = 0;
        boolean last = false;
        double sum = 0;
        double dis = 0;
        if (gpsInfoList.size() > 0) {
            for (HistoryLockStatusEntity gpsInfo : gpsInfoList) {
                if (gpsInfo.getLatitude() > 20
                        && gpsInfo.getLongitude() > 80) {
                    Gps gps = PositionUtil.gps84_To_Bd09(gpsInfo.getLatitude(), gpsInfo.getLongitude());
                    gpsInfo.setLongitude(gps.getWgLon());
                    gpsInfo.setLatitude(gps.getWgLat());
                    /**
                     * 如果nowfx与nowfy为初值 将nowfx与nowfy赋当前gpsInfo的值
                     * 暂停开始时间startPause,暂停截止时间endPause赋值为当前gpsInfo的时间
                     */
                    if (nowfx == -1 && nowfy == -1) {
                        nowfx = gpsInfo.getLongitude();
                        nowfy = gpsInfo.getLatitude();
                        startPause = gpsInfo.getGpsTime();
                        endPause = gpsInfo.getGpsTime();
                    } else {
/**
 * 计算当前位置与gpsInfo的距离
 */
                        dis = (CalculateUtil.getDistance(nowfx, nowfy,
                                gpsInfo.getLongitude(),
                                gpsInfo.getLatitude())) / 1000;

                        /**
                         * 如果当前位置与gpsInfo的距离没有超过规定的视为停止的距离 将gpsInfo的
                         * LONGITUDEFIX,LATITUDEFIX赋值为nowfx,nowfy 将距离赋值为0
                         * 将暂停标记last赋值为true,
                         * 将暂停截止时间endPause赋值为gpsInfo的gpsTime
                         */

                        if (gpsInfo.getSpeed() == 0) {
                            gpsInfo.setLongitude(nowfx);
                            gpsInfo.setLatitude(nowfy);
                            dis = 0;
                            last = true;
                            endPause = gpsInfo.getGpsTime();
                        } else {

                            /**
                             * 如果当前的点处于暂停状态且gpsInfo对应的点处于运动状态
                             */
                            if (last) {
                                /**
                                 * 通过暂停结束时间约暂停开始时间相减计算暂停持续时间
                                 */
                                lasttime = (endPause.getTime() - startPause.getTime()) / 60 / 1000;
                                /**
                                 * 如果暂停时间超过设置的停驶过久的时间，
                                 * 将暂停点的信息存放到pausePointList数组
                                 */
                                if (lasttime >= maxTimeDif) {
                                    pausePointList.add(gpsInfo);
                                }
                                /**
                                 * 存放完成之后停止标志置为flase
                                 */
                                last = false;
                            }
                            /**
                             * gpsInfo对应的点处于运动状态
                             * nowfx赋值为gpsInfo的LONGITUDEFIX
                             * nowfy赋值为gpsInfo的LATITUDEFIX
                             * 暂停开始时间startPause赋值为gpsInfo的GPSTIME
                             * 展厅截止时间endPause赋值为gpsInfo的GPSTIME
                             */
                            nowfx = gpsInfo.getLongitude();
                            nowfy = gpsInfo.getLatitude();
                            startPause = gpsInfo.getGpsTime();
                            endPause = gpsInfo.getGpsTime();
                        }

                    }

                    /**
                     * 总路程加上当前的距离dis
                     */
                    sum += dis;


                    /**
                     * 如果不是第一次
                     */
                    if (firstTime!=null) {
                        /**
                         * 如果当前点的时间与gpsInfo的GPSTIME时间差超过最大时间间隔
                         */
                        if (exceedMaxTime(firstTime,
                                gpsInfo.getGpsTime(), maxTimeDif)) {
                            sum -= dis;
                        }
                    }
                    /**
                     * 把当前时间设置为gpsInfo的GPSTIME
                     */
                    firstTime = gpsInfo.getGpsTime();
                    runPointList.add(gpsInfo);
                }
            }
        }
        return R.ok().put("runPoints", runPointList).put("stayPoints",pausePointList).put("sum",sum);
    }
    /**
     * 判断是否超出所设定的最长时间间隔
     *
     * @param firstTime
     * @param secoundTime
     * @param validMinutes
     * @return
     */
    private boolean exceedMaxTime(Date firstTime, Date secoundTime,
                                  int validMinutes) {
        try {

            if (secoundTime.getTime() - firstTime.getTime() > validMinutes * 60 * 1000) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return true;
        }
    }


}
