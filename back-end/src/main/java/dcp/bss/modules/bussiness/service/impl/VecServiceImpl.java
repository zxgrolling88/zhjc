package dcp.bss.modules.bussiness.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.VecDao;
import dcp.bss.modules.bussiness.entity.VecEntity;
import dcp.bss.modules.bussiness.service.VecService;


@Service("vecService")
public class VecServiceImpl extends ServiceImpl<VecDao, VecEntity> implements VecService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<VecEntity> page = this.selectPage(
                new Query<VecEntity>(params).getPage(),
                new EntityWrapper<VecEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<VecEntity> queryViewVo) {
        Page<VecEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<VecEntity> download(QueryViewVo<VecEntity> queryViewVo) {
        List<VecEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
