package dcp.bss.modules.bussiness.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.EntpDao;
import dcp.bss.modules.bussiness.entity.EntpEntity;
import dcp.bss.modules.bussiness.service.EntpService;


@Service("entpService")
public class EntpServiceImpl extends ServiceImpl<EntpDao, EntpEntity> implements EntpService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<EntpEntity> page = this.selectPage(
                new Query<EntpEntity>(params).getPage(),
                new EntityWrapper<EntpEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<EntpEntity> queryViewVo) {
        Page<EntpEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<EntpEntity> download(QueryViewVo<EntpEntity> queryViewVo) {
        List<EntpEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
