package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_lock_status")
public class LockStatusEntity {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 锁编号
	 */
	private String lockCode;
	/**
	 * 锁状态
	 */
	private String lockStatus;

	private String bill;

	private String cmdSrc;

	private Date crtTm;
	/**
	 * 车牌号
	 */
	private String vecNo;

	private String carrierName;

	private Double longitude;
	private Double latitude;
	private int  speed;
	private int  direction;
	private float  totalMile;
	private double  voltage;//锁电量（百分比）
	/**
	 * gps更新时间
	 */
	private Date gpsTime;
	private int  alarmStatus;//锁电量（百分比）
	private int  lockCharge;//锁电量（百分比）
	private int  gpsAntennaStatus;//0正常,1开路，2短路，3非适合天线
	private int  gpsLocationStaus;//0定位无效（非精确定位）,2D 定位（非精确定位）, 3D 定位（精确定位）

	private int  lac;
	private int  cell;


}
