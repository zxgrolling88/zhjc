package dcp.bss.modules.bussiness.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.PersDao;
import dcp.bss.modules.bussiness.entity.PersEntity;
import dcp.bss.modules.bussiness.service.PersService;


@Service("persService")
public class PersServiceImpl extends ServiceImpl<PersDao, PersEntity> implements PersService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<PersEntity> page = this.selectPage(
                new Query<PersEntity>(params).getPage(),
                new EntityWrapper<PersEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<PersEntity> queryViewVo) {
        Page<PersEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<PersEntity> download(QueryViewVo<PersEntity> queryViewVo) {
        List<PersEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
