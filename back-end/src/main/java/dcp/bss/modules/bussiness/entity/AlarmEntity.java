package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 报警表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_alarm")
public class AlarmEntity extends  BaseEntity{

	/**
	 * 报警id
	 */
	@TableId
	private Long id;
	/**
	 * 报警编码
	 */
	private String cd;

	/**
	 * 运输企业名称
	 */
	private String entpName;
	/**
	 * 牵引车号
	 */
	private String vecNo;

	/**
	 * 报警时间
	 */
	private Date alarmTime;
	/**
	 * 报警点地址
	 */
	private String alarmLocation;
	/**
	 * 报警点经度
	 */
	private Double alarmLongitude;
	/**
	 * 报警点纬度
	 */
	private Double alarmLatitude;

	/**
	 * 报警类型
	 */
	private String catNmCn;
	/**
	 * 报警详情
	 */
	private String descr;




}
