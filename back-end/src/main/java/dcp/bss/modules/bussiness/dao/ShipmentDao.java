package dcp.bss.modules.bussiness.dao;

import dcp.bss.modules.bussiness.entity.ShipmentEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-25 22:41:18
 */
@Mapper
@Repository
public interface ShipmentDao extends BaseMapper<ShipmentEntity> {
	
}
