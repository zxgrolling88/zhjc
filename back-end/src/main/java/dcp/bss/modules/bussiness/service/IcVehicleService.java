package dcp.bss.modules.bussiness.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.bussiness.entity.IcVehicleEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * ic卡和车辆绑定表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2019-05-13 20:12:55
 */
public interface IcVehicleService extends IService<IcVehicleEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<IcVehicleEntity> queryViewVo);

    List<IcVehicleEntity> download(QueryViewVo<IcVehicleEntity> queryViewVo);
}

