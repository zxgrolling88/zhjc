package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.ShipmentEntity;
import dcp.bss.modules.bussiness.service.ShipmentService;


/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-25 22:41:18
 */
@RestController
@RequestMapping("shipment")
@Slf4j
public class ShipmentController {
    @Autowired
    private ShipmentService shipmentService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("bussiness:shipment:list")
    public R list(QueryViewVo<ShipmentEntity> queryViewVo) {
        PageUtils page = shipmentService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:shipment:info")
    public R info(@PathVariable("id") Long id) {
        ShipmentEntity shipment = shipmentService.selectById(id);

        return R.ok().put("shipment", shipment);
    }



    /**
     * 查询某辆车最近一张电子运单
     */
    @RequestMapping("/lastest")
    // @RequiresPermissions("bussiness:lockstatus:info")
    public R lastest(String vecNo) {
        ShipmentEntity shipmentEntity = shipmentService.selectOne(
                new EntityWrapper<ShipmentEntity>().eq("vec_no", vecNo));
        return R.ok().put("data", shipmentEntity);
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:shipment:save")
    public R save(@RequestBody ShipmentEntity shipment) {
        shipmentService.insert(shipment);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:shipment:update")
    public R update(@RequestBody ShipmentEntity shipment) {
        shipmentService.updateById(shipment);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:shipment:delete")
    public R delete(Long id) {
        shipmentService.deleteById(id);
        return R.ok();
    }


}
