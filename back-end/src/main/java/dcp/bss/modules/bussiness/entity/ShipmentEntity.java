package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.util.Date;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-25 22:41:18
 */
@Data
@TableName("biz_shipment")
public class ShipmentEntity extends  BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 运单号
	 */
	private String code;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 车辆主键
	 */
	private Long vecPk;
	/**
	 * 车牌号
	 */
	private String vecNo;
	/**
	 * 客户主键
	 */
	private Long customerId;
	/**
	 * 客户名称
	 */
	private String customerName;
	/**
	 * 目的地
	 */
	private String destination;
	/**
	 * 司机主键
	 */
	private Long driverId;
	/**
	 * 司机名字
	 */
	private String driverName;
	/**
	 * 承运商主键
	 */
	private Long carrierId;
	/**
	 * 承运商名字
	 */
	private String carrierName;
	/**
	 * 装载容量
	 */
	private Double loadCapacity;
	/**
	 * 装货时间
	 */
	private Date loadTime;
	/**
	 * 发货地仓库id
	 */
	private Long originId;
	/**
	 * 发货地
	 */
	private String origin;
	/**
	 * 容量
	 */
	private Double volumn;
	/**
	 * 重量
	 */
	private Double weight;


}
