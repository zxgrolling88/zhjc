package dcp.bss.modules.bussiness.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import dcp.bss.query.QueryViewVo;

import java.util.List;
import java.util.Map;

/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
public interface LockStatusService extends IService<LockStatusEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<LockStatusEntity> queryViewVo);

    List<LockStatusEntity> download(QueryViewVo<LockStatusEntity> queryViewVo);

    LockStatusEntity selectByCode(String lockNum);

    /**
     * 开锁
     * @param lockNum
     */
    R unlock(String lockNum,Double lng,Double lat);

    /**
     * 锁定
     * @param lockNum
     */
    R lock(String lockNum,Double lng,Double lat);
}

