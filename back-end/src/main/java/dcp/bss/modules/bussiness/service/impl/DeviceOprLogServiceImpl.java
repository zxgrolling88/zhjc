package dcp.bss.modules.bussiness.service.impl;

import dcp.bss.common.model.Gps;
import dcp.bss.common.util.PositionUtil;
import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.DeviceOprLogDao;
import dcp.bss.modules.bussiness.entity.DeviceOprLogEntity;
import dcp.bss.modules.bussiness.service.DeviceOprLogService;


@Service("deviceOprLogService")
public class DeviceOprLogServiceImpl extends ServiceImpl<DeviceOprLogDao, DeviceOprLogEntity> implements DeviceOprLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DeviceOprLogEntity> page = this.selectPage(
                new Query<DeviceOprLogEntity>(params).getPage(),
                new EntityWrapper<DeviceOprLogEntity>().orderBy("opr_tm",false)
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<DeviceOprLogEntity> queryViewVo) {
        Page<DeviceOprLogEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper().orderBy("opr_tm",false));
        List<DeviceOprLogEntity> data =   page.getRecords();
        for(DeviceOprLogEntity deviceOprLogEntity:data){
            if(deviceOprLogEntity.getLat()!=null&&deviceOprLogEntity.getLng()!=null){
                Gps gps = PositionUtil.gps84_To_Bd09(deviceOprLogEntity.getLat(), deviceOprLogEntity.getLng());
                deviceOprLogEntity.setLng(gps.getWgLon());
                deviceOprLogEntity.setLat(gps.getWgLat());
            }
        }
        return new PageUtils(page);
    }

    @Override
    public List<DeviceOprLogEntity> download(QueryViewVo<DeviceOprLogEntity> queryViewVo) {
        List<DeviceOprLogEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
