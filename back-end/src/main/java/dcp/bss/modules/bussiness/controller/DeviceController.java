package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import dcp.bss.modules.bussiness.entity.CmdChangeIpBean;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import dcp.bss.modules.bussiness.service.impl.MessageReceiver;
import dcp.bss.modules.sys.entity.SysUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.DeviceEntity;
import dcp.bss.modules.bussiness.service.DeviceService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("device")
@Slf4j
public class DeviceController {
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private StringRedisTemplate template;
    /**
     * 列表
     */
    @RequestMapping("/page")
    // @RequiresPermissions("bussiness:device:list")
    public R list(QueryViewVo<DeviceEntity> queryViewVo) {
        PageUtils page = deviceService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/queryByVecNo")
    // @RequiresPermissions("bussiness:lockstatus:info")
    public R queryByVecNo(String vecNo) {
        List<DeviceEntity> deviceEntityList = deviceService.selectList(
                new EntityWrapper<DeviceEntity>().eq("vec_no", vecNo));
        return R.ok().put("data", deviceEntityList);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:device:info")
    public R info(@PathVariable("id") Long id) {
        DeviceEntity device = deviceService.selectById(id);

        return R.ok().put("device", device);
    }


    /**
     * 保存
     */
    @RequestMapping("/changeIp")
    // @RequiresPermissions("bussiness:device:save")
    public R changeIp(@RequestBody CmdChangeIpBean changeIpBean){
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        log.info("changeIpOpr:"+user.getUsername());
        template.convertAndSend("changeIpOpr", JSONObject.toJSONString(changeIpBean));
        return R.ok();
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:device:save")
    public R save(@RequestBody DeviceEntity device) {
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        device.setCrtBy(user.getUsername());
        device.setUpdBy(user.getUsername());
        deviceService.insert(device);
        initVecDevMap();
        return R.ok();
    }

    private void initVecDevMap() {
        List<DeviceEntity> deviceEntityList = deviceService.selectList(new EntityWrapper<DeviceEntity>());
        for (DeviceEntity deviceEntity : deviceEntityList) {
            MessageReceiver.map.put(deviceEntity.getCode(), deviceEntity.getVecNo());
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:device:update")
    public R update(@RequestBody DeviceEntity device) {
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        device.setUpdBy(user.getUsername());
        deviceService.updateById(device);
        initVecDevMap();
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:device:delete")
    public R delete(@RequestBody Long[] ids) {
        deviceService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


}
