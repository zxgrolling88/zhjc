package dcp.bss.modules.bussiness.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.DeviceDao;
import dcp.bss.modules.bussiness.entity.DeviceEntity;
import dcp.bss.modules.bussiness.service.DeviceService;


@Service("deviceService")
public class DeviceServiceImpl extends ServiceImpl<DeviceDao, DeviceEntity> implements DeviceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<DeviceEntity> page = this.selectPage(
                new Query<DeviceEntity>(params).getPage(),
                new EntityWrapper<DeviceEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<DeviceEntity> queryViewVo) {
        Page<DeviceEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<DeviceEntity> download(QueryViewVo<DeviceEntity> queryViewVo) {
        List<DeviceEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

    @Override
    public DeviceEntity selectByCode(String code) {
        DeviceEntity deviceEntity = this.selectOne(new EntityWrapper<DeviceEntity>().eq("code",code));
        return deviceEntity;
    }

}
