package dcp.bss.modules.bussiness.service.impl;

import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.ShipmentDao;
import dcp.bss.modules.bussiness.entity.ShipmentEntity;
import dcp.bss.modules.bussiness.service.ShipmentService;


@Service("shipmentService")
public class ShipmentServiceImpl extends ServiceImpl<ShipmentDao, ShipmentEntity> implements ShipmentService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<ShipmentEntity> page = this.selectPage(
                new Query<ShipmentEntity>(params).getPage(),
                new EntityWrapper<ShipmentEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<ShipmentEntity> queryViewVo) {
        Page<ShipmentEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<ShipmentEntity> download(QueryViewVo<ShipmentEntity> queryViewVo) {
        List<ShipmentEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
