package dcp.bss.modules.bussiness.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import dcp.bss.common.model.GpsBean;
import dcp.bss.common.model.LockInfoBean;
import dcp.bss.common.util.BaseStationAPI;
import dcp.bss.common.util.DateUtils;
import dcp.bss.modules.bussiness.dao.DeviceDao;
import dcp.bss.modules.bussiness.dao.HistoryLockStatusDao;
import dcp.bss.modules.bussiness.dao.VecDao;
import dcp.bss.modules.bussiness.entity.*;
import dcp.bss.modules.bussiness.service.DeviceOprLogService;
import dcp.bss.modules.bussiness.service.LockStatusService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 11:14
 */
@Component
public class MessageReceiver {

    @Autowired
    private LockStatusService lockStatusService;

    @Autowired
    private HistoryLockStatusDao historyLockStatusDao;


    @Autowired
    private DeviceDao deviceDao;


    @Autowired
    private VecDao vecDao;


    @Autowired
    private DeviceOprLogService deviceOprLogService;

    public static HashMap<String, String> map = new HashMap<>();

    public static HashMap<String, String> entpmap = new HashMap<>();

    @PostConstruct
    private void init() {
        initVecDevMap();
    }

    @Scheduled(fixedDelay = 2 * 60 * 1000)
    private void initVecDevMap() {
        List<DeviceEntity> deviceEntityList = deviceDao.selectList(new EntityWrapper<DeviceEntity>());
        for (DeviceEntity deviceEntity : deviceEntityList) {
            map.put(deviceEntity.getCode(), deviceEntity.getVecNo());
        }
        List<VecEntity> vecEntityList = vecDao.selectList(new EntityWrapper<VecEntity>());
        for (VecEntity vecEntity : vecEntityList) {
            entpmap.put(vecEntity.getVecNo(), vecEntity.getOwnedCompany());
        }
    }

    /**
     * 接收消息的方法
     */
    public void gpsMessage(String message) throws Exception {
//        System.out.println("收到GPS消息：" + message);
        GpsBean gpsBean = JSONObject.parseObject(message, GpsBean.class);
        LockStatusEntity lockStatusEntity = new LockStatusEntity();

        BeanUtils.copyProperties(gpsBean, lockStatusEntity);
//
//        if ((gpsBean.getLongitude() - 113.8112) < 0.05 && (gpsBean.getLatitude() - 22.666032) < 0.05) {
////            String result = BaseStationAPI.getLocation("460", "00", gpsBean.getLac(), gpsBean.getCell());
////            if (result != null) {
////                JSONObject jsonObject = JSONObject.parseObject(result);
////                lockStatusEntity.setLongitude(84.899500);
////                lockStatusEntity.setLatitude(45.573648);
////            }
//            lockStatusEntity.setLongitude(84.899500);
//            lockStatusEntity.setLatitude(45.573648);
//        }
        lockStatusEntity.setGpsTime(DateUtils.covertStrToDate(gpsBean.getDate(), DateUtils.FULL_ST_FORMAT));
        lockStatusEntity.setCrtTm(new Date());
        lockStatusEntity.setLockCode(String.valueOf(gpsBean.getLockDevID()));
        if (map.containsKey(lockStatusEntity.getLockCode())) {
            lockStatusEntity.setVecNo(map.get(lockStatusEntity.getLockCode()));
            if (entpmap.containsKey(lockStatusEntity.getVecNo())) {
                lockStatusEntity.setCarrierName(entpmap.get(lockStatusEntity.getVecNo()));
            }
        }
        Wrapper ew = new EntityWrapper<LockStatusEntity>().eq("lock_code", lockStatusEntity.getLockCode());
        int count = lockStatusService.selectCount(ew);
        if (count > 0) {
            lockStatusService.update(lockStatusEntity, new EntityWrapper<LockStatusEntity>().eq("lock_code", lockStatusEntity.getLockCode()));
        } else {
            lockStatusService.insert(lockStatusEntity);
        }
        HistoryLockStatusEntity historyLockStatusEntity = new HistoryLockStatusEntity();
        historyLockStatusEntity.setGpsTime(DateUtils.covertStrToDate(gpsBean.getDate(), DateUtils.FULL_ST_FORMAT));
        BeanUtils.copyProperties(lockStatusEntity, historyLockStatusEntity);
        if (map.containsKey(lockStatusEntity.getLockCode())) {
            historyLockStatusEntity.setVecNo(map.get(lockStatusEntity.getLockCode()));
        }
        historyLockStatusEntity.setCrtTm(new Date());
        historyLockStatusDao.insert(historyLockStatusEntity);
    }

    public void lockMessage(String message) throws ParseException {
        System.out.println("收到电子锁消息：" + message);
        LockInfoBean lockInfoBean = JSONObject.parseObject(message, LockInfoBean.class);
        LockStatusEntity lockStatusEntity = lockStatusService.selectByCode(String.valueOf(lockInfoBean.getLockDevID()));
        lockStatusEntity.setLockStatus(lockInfoBean.getLockStatus());
        lockStatusEntity.setVoltage(lockInfoBean.getVoltage());
        lockStatusEntity.setCmdSrc(lockInfoBean.getCmdSRC());
        lockStatusEntity.setBill(lockInfoBean.getBill());
        lockStatusService.updateById(lockStatusEntity);
        if ("键盘".equals(lockInfoBean.getCmdSRC())) {
            DeviceOprLogEntity deviceOprLogEntity = new DeviceOprLogEntity();
            deviceOprLogEntity.setOprTm(DateUtils.covertStrToDate(lockInfoBean.getSendtime(), DateUtils.FULL_ST_FORMAT));
            deviceOprLogEntity.setDeviceCode(String.valueOf(lockInfoBean.getLockDevID()));
            deviceOprLogEntity.setLockStatus(lockInfoBean.getLockStatus());
            deviceOprLogEntity.setCmdSrc(lockInfoBean.getCmdSRC());
            deviceOprLogEntity.setResult(lockInfoBean.getCmd());
            deviceOprLogEntity.setOprator(lockInfoBean.getBill().substring(4, 12));
            deviceOprLogEntity.setCrtTm(new Date());
            if ("4960".equals(lockInfoBean.getBill().substring(0, 4))) {
                deviceOprLogEntity.setOprType("IC卡施封");
            }
            if ("4961".equals(lockInfoBean.getBill().substring(0, 4))) {
                deviceOprLogEntity.setOprType("IC卡解封");
            }
            if ("4962".equals(lockInfoBean.getBill().substring(0, 4))) {
                deviceOprLogEntity.setOprType("临时解封");
            }
            int count = deviceOprLogService.selectCount(new EntityWrapper<DeviceOprLogEntity>().
                    eq("device_code", lockInfoBean.getLockDevID()).eq("opr_tm", lockInfoBean.getSendtime()));
            if (count == 0) {
                Wrapper ew = new EntityWrapper<LockStatusEntity>().eq("lock_code", lockInfoBean.getLockDevID());
                LockStatusEntity lockStatus = lockStatusService.selectOne(ew);
                deviceOprLogEntity.setLng(lockStatus.getLongitude());
                deviceOprLogEntity.setLat(lockStatus.getLatitude());
                deviceOprLogService.insert(deviceOprLogEntity);
            }

        }

    }
}
