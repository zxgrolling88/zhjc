package dcp.bss.modules.bussiness.dao;

import dcp.bss.modules.bussiness.entity.DeviceEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Mapper
@Repository
public interface DeviceDao extends BaseMapper<DeviceEntity> {
	
}
