package dcp.bss.modules.bussiness.controller;

import dcp.bss.common.model.KeyValueBean;
import dcp.bss.common.util.DBUtil;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;
import dcp.bss.modules.bussiness.entity.VecEntity;
import dcp.bss.modules.bussiness.service.VecService;
import dcp.bss.query.QueryViewVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 车辆表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("home")
@Slf4j
public class HomeController {

    /**
     * 列表
     */
    @RequestMapping("/basic")
    public R list() throws Exception {
        R r = new R();
        String  sql ="SELECT status,count(*) FROM `biz_shipment` GROUP BY `status`";
        List<KeyValueBean> shipStat =  DBUtil.executeQuery(sql);
        for(KeyValueBean keyValueBean:shipStat){
            if("ARRIVED".equals(keyValueBean.getKey())) {
                r.put("arrived", keyValueBean.getValue());
            }
            if("COMPLETED".equals(keyValueBean.getKey())) {
                r.put("complete", keyValueBean.getValue());
            }

            if("PENDING_PICK".equals(keyValueBean.getKey())) {
                r.put("pendingPick", keyValueBean.getValue());
            }

            if("PICKED".equals(keyValueBean.getKey())) {
                r.put("picked", keyValueBean.getValue());
            }

        }

        Long totalVec = DBUtil.countAmount("biz_vec");
        r.put("totalVec", totalVec);
        String runSql = "SELECT count(*) FROM `biz_gps` where speed>0";
        r.put("runVec", DBUtil.countByType(runSql));
        Long totalAlarm = DBUtil.countAmount("biz_alarm");
        r.put("totalAlarm", totalAlarm);
        String cargoAmountSql = "SELECT sum(weight) FROM `biz_shipment`";
        r.put("cargoAmount", DBUtil.countByType(cargoAmountSql));
        return r;
    }



    //统计不同类型企业数量
    @RequestMapping("/alarmTypeAmount")
    public List<KeyValueBean> alarmTypeAmount() {
        List<KeyValueBean> list = new ArrayList<>();
        try {
            String sql = "select cat_nm_cn,count(*) from biz_alarm group by cat_nm_cn";
            list = DBUtil.executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    //统计月运输数量(吨)趋势
    @RequestMapping("/getCargoAmountTrend")
    public List<KeyValueBean> getCargoAmountTrend() {
        List<KeyValueBean> list = new ArrayList<>();
        try {
            String tranTypeSql = "SELECT DATE_FORMAT(`load_time`,'%Y-%m-%d'),count(*) FROM `biz_shipment` GROUP BY DATE_FORMAT(`load_time`,'%Y-%m-%d')";
            list = DBUtil.executeQuery(tranTypeSql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
