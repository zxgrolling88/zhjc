package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_area_in_out")
public class AreaInOutEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private String id;
	/**
	 * 围栏id
	 */
	private Long geoId;
	/**
	 * 围栏名称
	 */
	private String geoName;
	/**
	 * 车辆主键
	 */
	private Long vehicleId;
	/**
	 * 车牌号
	 */
	private String vehicleNo;
	/**
	 * 1表示进,2表示出
	 */
	private Integer type;
	/**
	 * 进入围栏时间
	 */
	private Date enterDate;
	/**
	 * 离开围栏时间
	 */
	private Date leaveDate;
	/**
	 * 进入经纬度
	 */
	private Double lng;
	/**
	 * 进入纬度
	 */
	private Double lat;
	/**
	 * 离开经度
	 */
	private Double leaveLng;
	/**
	 * 离开纬度
	 */
	private Double leaveLat;


}
