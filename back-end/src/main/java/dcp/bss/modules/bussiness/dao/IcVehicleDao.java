package dcp.bss.modules.bussiness.dao;

import dcp.bss.modules.bussiness.entity.IcVehicleEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * ic卡和车辆绑定表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2019-05-13 20:12:55
 */
@Mapper
@Repository
public interface IcVehicleDao extends BaseMapper<IcVehicleEntity> {
	
}
