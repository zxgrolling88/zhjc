package dcp.bss.modules.bussiness.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.GpsDao;
import dcp.bss.modules.bussiness.entity.GpsEntity;
import dcp.bss.modules.bussiness.service.GpsService;


@Service("gpsService")
public class GpsServiceImpl extends ServiceImpl<GpsDao, GpsEntity> implements GpsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<GpsEntity> page = this.selectPage(
                new Query<GpsEntity>(params).getPage(),
                new EntityWrapper<GpsEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<GpsEntity> queryViewVo) {
        Page<GpsEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<GpsEntity> download(QueryViewVo<GpsEntity> queryViewVo) {
        List<GpsEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
