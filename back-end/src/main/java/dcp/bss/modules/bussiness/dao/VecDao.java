package dcp.bss.modules.bussiness.dao;

import dcp.bss.modules.bussiness.entity.VecEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 车辆表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Mapper
@Repository
public interface VecDao extends BaseMapper<VecEntity> {
	
}
