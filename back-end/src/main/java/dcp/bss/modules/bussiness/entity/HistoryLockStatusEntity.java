package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_history_lock_status")
public class HistoryLockStatusEntity {

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 锁编号
	 */
	private String lockCode;
	/**
	 * 锁状态
	 */
	private String lockStatus;

	/**
	 * 车牌号
	 */
	private String vecNo;

	private Date crtTm;

	private double longitude;
	private double latitude;
	private int  speed;
	private int  direction;
	private float  totalMile;

	/**
	 * gps更新时间
	 */
	private Date gpsTime;

}
