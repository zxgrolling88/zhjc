package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 车辆表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_vec")
public class VecEntity extends  BaseEntity{

	/**
	 * 车辆表的主键
	 */
	@TableId
	private Long vecPk;
	/**
	 * 车号
	 */
	private String vecNo;
	/**
	 * 企业主键
	 */
	private Long entpPk;
	/**
	 * 所属公司
	 */
	private String ownedCompany;



}
