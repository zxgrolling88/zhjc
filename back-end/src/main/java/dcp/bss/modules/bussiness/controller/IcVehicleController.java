package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;
import dcp.bss.modules.sys.entity.SysUserEntity;
import dcp.bss.web.exception.RRException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.IcVehicleEntity;
import dcp.bss.modules.bussiness.service.IcVehicleService;


/**
 * ic卡和车辆绑定表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2019-05-13 20:12:55
 */
@RestController
@RequestMapping("icvehicle")
@Slf4j
public class IcVehicleController {
    @Autowired
    private IcVehicleService icVehicleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("bussiness:icvehicle:list")
    public R list(QueryViewVo<IcVehicleEntity> queryViewVo) {
        PageUtils page = icVehicleService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:icvehicle:info")
    public R info(@PathVariable("id") Long id) {
        IcVehicleEntity icVehicle = icVehicleService.selectById(id);

        return R.ok().put("icVehicle", icVehicle);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:icvehicle:save")
    public R save(@RequestBody IcVehicleEntity icVehicle) {
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        icVehicle.setCrtBy(user.getUsername());
        icVehicle.setUpdBy(user.getUsername());
        int count = icVehicleService.selectCount(new EntityWrapper<IcVehicleEntity>().eq("code", icVehicle.getCode()));
        if (count > 0) {
            throw new RRException( "IC卡["+icVehicle.getCode()+"]已经绑定到其他车辆，请先解绑之后再绑定");
        }

        int count2 = icVehicleService.selectCount(new EntityWrapper<IcVehicleEntity>().eq("vec_no", icVehicle.getVecNo()));
        if (count2 > 0) {
            throw new RRException(icVehicle.getVecNo()+"已绑定到其他IC卡中，请先解绑之后再绑定");
        }

        int count3 = icVehicleService.selectCount(new EntityWrapper<IcVehicleEntity>().eq("code", icVehicle.getCode()).eq("vec_no", icVehicle.getVecNo()));
        if (count3 > 0) {
            throw new RRException( "IC卡["+icVehicle.getCode()+"]已经绑定到车辆"+ icVehicle.getVecNo()+"，请先解绑之后再绑定");
        }
        icVehicleService.insert(icVehicle);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:icvehicle:update")
    public R update(@RequestBody IcVehicleEntity icVehicle) {
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        icVehicle.setUpdBy(user.getUsername());
        int count = icVehicleService.selectCount(new EntityWrapper<IcVehicleEntity>().eq("code", icVehicle.getCode()));
        if (count > 0) {
            throw new RRException( "IC卡["+icVehicle.getCode()+"]已经绑定到其他车辆，请先解绑之后再绑定");
        }

        int count2 = icVehicleService.selectCount(new EntityWrapper<IcVehicleEntity>().eq("vec_no", icVehicle.getVecNo()));
        if (count2 > 0) {
            throw new RRException(icVehicle.getVecNo()+"已绑定到其他IC卡中，请先解绑之后再绑定");
        }
        int count3 = icVehicleService.selectCount(new EntityWrapper<IcVehicleEntity>().eq("code", icVehicle.getCode()).eq("vec_no", icVehicle.getVecNo()));
        if (count3 > 0) {
            throw new RRException( "IC卡["+icVehicle.getCode()+"]已经绑定到车辆"+ icVehicle.getVecNo()+"，请先解绑之后再绑定");
        }
        icVehicleService.updateById(icVehicle);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:icvehicle:delete")
    public R delete(@RequestBody Long[] ids) {
        icVehicleService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }


}
