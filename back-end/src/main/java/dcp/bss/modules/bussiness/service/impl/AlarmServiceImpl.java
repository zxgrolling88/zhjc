package dcp.bss.modules.bussiness.service.impl;

import com.baomidou.mybatisplus.mapper.Wrapper;
import dcp.bss.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.AlarmDao;
import dcp.bss.modules.bussiness.entity.AlarmEntity;
import dcp.bss.modules.bussiness.service.AlarmService;


@Service("alarmService")
public class AlarmServiceImpl extends ServiceImpl<AlarmDao, AlarmEntity> implements AlarmService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<AlarmEntity> page = this.selectPage(
                new Query<AlarmEntity>(params).getPage(),
                new EntityWrapper<AlarmEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<AlarmEntity> queryViewVo) {
        Wrapper<AlarmEntity> ew = queryViewVo.getWrapper();
        SysUserEntity sysUserEntity = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        if (sysUserEntity.getIpPk() == 0) {
        } else {
            ew.eq("entp_name",sysUserEntity.getIpName());
        }
        Page<AlarmEntity> page = this.selectPage(queryViewVo.getPageUtil(),ew);
        return new PageUtils(page);
    }

    @Override
    public List<AlarmEntity> download(QueryViewVo<AlarmEntity> queryViewVo) {
        List<AlarmEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
