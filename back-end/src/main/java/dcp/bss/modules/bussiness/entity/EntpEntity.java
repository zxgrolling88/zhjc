package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 * 企业信息表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_entp")
public class EntpEntity extends  BaseEntity{

	/**
	 * 企业信息表的主键-与t_ip表共用主键
	 */
	@TableId
	private Long id;
	/**
	 * 企业名称
	 */
	private String name;
	/**
	 * 统一社会信用编码union social credit
	 */
	private String uscCd;

	private String mobile;
	/**
	 * 企业子类分类编码
	 */
	private String catCd;

	/**
	 * 企业子类分类中文名
	 */
	private String catNmCn;

	/**
	 * 法人代表姓名
	 */
	private String legalRepNm;

	/**
	 * 地址
	 */
	private String location;
	/**
	 * 经营范围
	 */
	private String businessScope;


}
