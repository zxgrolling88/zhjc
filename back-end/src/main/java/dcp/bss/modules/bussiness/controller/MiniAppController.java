package dcp.bss.modules.bussiness.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import dcp.bss.common.model.KeyValueBean;
import dcp.bss.common.util.DBUtil;
import dcp.bss.common.util.DateUtils;
import dcp.bss.common.util.R;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import dcp.bss.modules.bussiness.service.LockStatusService;
import dcp.bss.modules.sys.controller.AbstractController;
import dcp.bss.modules.sys.entity.SysUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 车辆表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("miniapp")
@Slf4j
public class MiniAppController extends AbstractController {
    @Autowired
    private LockStatusService lockStatusService;

    /**
     * 列表  https://www.jhyj56.com/ztjk/miniapp/carrier
     */
    @RequestMapping("/carrier")
    public R list() throws Exception {
        SysUserEntity sysUserEntity = getUser();

        R r = new R();
        String sql = "SELECT\n" +
                "\tcarrier_name,\n" +
                "\tcount( 1 ) AS '全部',\n" +
                "\tcount( IF ( gps_time > date_sub( CURRENT_TIME ( ), INTERVAL 1 HOUR ), 1, NULL ) ) AS '在线',\n" +
                "\tcount( IF ( gps_time < date_sub( CURRENT_TIME ( ), INTERVAL 1 HOUR ), 1, NULL ) ) AS '离线' \n" +
                "FROM\n" +
                "\t`biz_lock_status` \n";

        if (sysUserEntity.getIpPk() == 0) {
        } else {

            sql += "\twhere carrier_name='"+sysUserEntity.getIpName()+"'\n";
        }
        sql += "GROUP BY carrier_name";
        List<List<Object>> dataList = DBUtil.executeQueryFH(sql);
        r.put("data", dataList);
        return r;
    }

    /**
     * 列表 https://www.jhyj56.com/ztjk/miniapp/deviceList
     */
    @RequestMapping("/deviceList")
    public R deviceList(@RequestParam(value = "carrierName", required = true) String carrierName, Integer status) throws Exception {
        R r = new R();
        Wrapper<LockStatusEntity> ew = new EntityWrapper<LockStatusEntity>().eq("carrier_name", carrierName);
        if (status != null) {
            //离线
            if (status == 0) {
                ew.le("gps_time", DateUtils.formatDateTime(DateUtils.getMoveDate(new Date(), 0, 0, 0, -1, 0, 0)));
            } else {
                ew.gt("gps_time", DateUtils.getMoveDate(new Date(), 0, 0, 0, -1, 0, 0));
            }
        }
        List<LockStatusEntity> dataList = lockStatusService.selectList(ew);
        r.put("data", dataList);
        return r;
    }


}
