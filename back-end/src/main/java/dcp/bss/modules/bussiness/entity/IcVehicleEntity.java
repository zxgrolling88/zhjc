package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * ic卡和车辆绑定表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2019-05-13 20:12:55
 */
@Data
@TableName("biz_ic_vehicle")
public class IcVehicleEntity extends  BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * IC卡编码
	 */
	private String code;
	/**
	 * IC卡名称
	 */
	private String name;
	/**
	 * 车辆主键
	 */
	private Long vecPk;
	/**
	 * 车牌号
	 */
	private String vecNo;
	/**
	 * 司机
	 */
	private String driverName;
	/**
	 * 分类编码
	 */
	private String catCd;



}
