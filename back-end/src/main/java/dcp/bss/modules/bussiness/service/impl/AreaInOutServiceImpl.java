package dcp.bss.modules.bussiness.service.impl;

import dcp.bss.web.util.Query;
import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.AreaInOutDao;
import dcp.bss.modules.bussiness.entity.AreaInOutEntity;
import dcp.bss.modules.bussiness.service.AreaInOutService;


@Service("areaInOutService")
public class AreaInOutServiceImpl extends ServiceImpl<AreaInOutDao, AreaInOutEntity> implements AreaInOutService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<AreaInOutEntity> page = this.selectPage(
                new Query<AreaInOutEntity>(params).getPage(),
                new EntityWrapper<AreaInOutEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<AreaInOutEntity> queryViewVo) {
        Page<AreaInOutEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<AreaInOutEntity> download(QueryViewVo<AreaInOutEntity> queryViewVo) {
        List<AreaInOutEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
