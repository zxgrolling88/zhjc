package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;
import java.util.Timer;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import dcp.bss.common.model.CmdLockBean;
import dcp.bss.modules.bussiness.entity.DeviceEntity;
import dcp.bss.modules.bussiness.entity.DeviceOprLogEntity;
import dcp.bss.modules.bussiness.service.DeviceService;
import dcp.bss.web.exception.RRException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import dcp.bss.modules.bussiness.service.LockStatusService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("lockstatus")
@Slf4j
public class LockStatusController {

    @Autowired
    private DeviceService deviceService;
    @Autowired
    private LockStatusService lockStatusService;
    @Autowired
    private StringRedisTemplate template;

    /**
     * 列表
     */
    @RequestMapping("/page")
    // @RequiresPermissions("bussiness:lockstatus:list")
    public R list(QueryViewVo<LockStatusEntity> queryViewVo) {
        PageUtils page = lockStatusService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:lockstatus:info")
    public R info(@PathVariable("id") Long id) {
        LockStatusEntity lockStatus = lockStatusService.selectById(id);

        return R.ok().put("lockStatus", lockStatus);
    }


    /**
     * 信息
     */
    @RequestMapping("/queryByVecNo")
    // @RequiresPermissions("bussiness:lockstatus:info")
    public R queryByVecNo(String vecNo) {
        List<LockStatusEntity> lockStatusEntities = lockStatusService.selectList(
                new EntityWrapper<LockStatusEntity>().eq("vec_no", vecNo));
        return R.ok().put("data", lockStatusEntities);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:lockstatus:save")
    public R save(@RequestBody LockStatusEntity lockStatus) {
        lockStatusService.insert(lockStatus);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:lockstatus:update")
    public R update(@RequestBody LockStatusEntity lockStatus) {
        lockStatusService.updateById(lockStatus);

        return R.ok();
    }


    /**
     * 解封  https://www.jhyj56.com/ztjk/lockstatus/unlock
     */
    @RequestMapping("/unlock")
    // @RequiresPermissions("bussiness:lockstatus:update")
    public R unlock(@RequestParam(value = "lockNum", required = true) String lockNum,Double lng,Double lat) {
        return lockStatusService.unlock(lockNum,lng,lat);
    }


    /**
     * 锁定
     */
    @RequestMapping("/lock")
    // @RequiresPermissions("bussiness:lockstatus:update")
    public R lock(@RequestParam(value = "lockNum", required = true) String lockNum,Double lng,Double lat) {
        return lockStatusService.lock(lockNum,lng,lat);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:lockstatus:delete")
    public R delete(@RequestBody Long[] ids) {
        lockStatusService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


}
