package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import dcp.bss.modules.bussiness.entity.LockStatusEntity;
import dcp.bss.modules.bussiness.entity.ShipmentEntity;
import dcp.bss.modules.bussiness.service.LockStatusService;
import dcp.bss.modules.bussiness.service.ShipmentService;
import dcp.bss.modules.sys.controller.AbstractController;
import dcp.bss.modules.sys.entity.SysUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.GpsEntity;
import dcp.bss.modules.bussiness.service.GpsService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * GPS实时表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("gps")
@Slf4j
public class GpsController  extends AbstractController {
    @Autowired
    private GpsService gpsService;
    @Autowired
    private LockStatusService lockStatusService;
    @Autowired
    private ShipmentService shipmentService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("bussiness:gps:list")
    public R list() {
        Wrapper<LockStatusEntity> ew =   new EntityWrapper<LockStatusEntity>();
        SysUserEntity sysUserEntity = getUser();

        if (sysUserEntity.getIpPk() == 0) {
        } else {
            ew.eq("carrier_name",sysUserEntity.getIpName());
        }
        List<LockStatusEntity> lockStatusEntities = lockStatusService.selectList(
                ew);
        return R.ok().put("datas", lockStatusEntities);
    }

    @RequestMapping("/lastest")
    public R lastest(String vecNo) {
        List<LockStatusEntity> lockStatusEntities = lockStatusService.selectList(
                new EntityWrapper<LockStatusEntity>().eq("vec_no", vecNo) );
        ShipmentEntity shipmentEntity = shipmentService.selectOne(
                new EntityWrapper<ShipmentEntity>().eq("vec_no", vecNo));
        return R.ok().put("gps", lockStatusEntities).put("ship", shipmentEntity);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:gps:info")
    public R info(@PathVariable("id") Long id) {
        GpsEntity gps = gpsService.selectById(id);

        return R.ok().put("gps", gps);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:gps:save")
    public R save(@RequestBody GpsEntity gps) {
        gpsService.insert(gps);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:gps:update")
    public R update(@RequestBody GpsEntity gps) {
        gpsService.updateById(gps);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:gps:delete")
    public R delete(@RequestBody Long[] ids) {
        gpsService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }




}
