package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.AreaInOutEntity;
import dcp.bss.modules.bussiness.service.AreaInOutService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("areainout")
@Slf4j
public class AreaInOutController {
    @Autowired
    private AreaInOutService areaInOutService;

    /**
         * 列表
         */
    @RequestMapping("/page")
   // @RequiresPermissions("bussiness:areainout:list")
    public R list(QueryViewVo<AreaInOutEntity> queryViewVo) {
        PageUtils page = areaInOutService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
   // @RequiresPermissions("bussiness:areainout:info")
    public R info(@PathVariable("id") String id){
			AreaInOutEntity areaInOut = areaInOutService.selectById(id);

        return R.ok().put("areaInOut", areaInOut);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
   // @RequiresPermissions("bussiness:areainout:save")
    public R save(@RequestBody AreaInOutEntity areaInOut){
			areaInOutService.insert(areaInOut);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
   // @RequiresPermissions("bussiness:areainout:update")
    public R update(@RequestBody AreaInOutEntity areaInOut){
			areaInOutService.updateById(areaInOut);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
   // @RequiresPermissions("bussiness:areainout:delete")
    public R delete(@RequestBody String[] ids){
			areaInOutService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
	 * 导出数据到excel
	 *
	 * @param response
	 * @param queryViewVo
	 * @return
	 */
    @RequestMapping("/download")
  //  @RequiresPermissions("bussiness:areainout:download")
    public void download(HttpServletResponse response, QueryViewVo<AreaInOutEntity> queryViewVo) {
        try {
            List<AreaInOutEntity> download = areaInOutService.download(queryViewVo);
            XSSFWorkbook sfworkbook = new XSSFWorkbook();
            ExcelUtil excelUtil = new ExcelUtil();
            SXSSFWorkbook createWorkbook = excelUtil.createWorkbook(sfworkbook);
            ExcelUtil.WriteExcel<AreaInOutEntity> writeexcel = new  ExcelUtil.WriteExcel<AreaInOutEntity>() {

                @Override
                public void writeHead(XSSFRow contentxfrow) {
                                            XSSFCell createCell1 = contentxfrow.createCell(0);
                        createCell1.setCellValue("主键");
                                            XSSFCell createCell2 = contentxfrow.createCell(1);
                        createCell2.setCellValue("围栏id");
                                            XSSFCell createCell3 = contentxfrow.createCell(2);
                        createCell3.setCellValue("围栏名称");
                                            XSSFCell createCell4 = contentxfrow.createCell(3);
                        createCell4.setCellValue("车辆主键");
                                            XSSFCell createCell5 = contentxfrow.createCell(4);
                        createCell5.setCellValue("车牌号");
                                            XSSFCell createCell6 = contentxfrow.createCell(5);
                        createCell6.setCellValue("1表示进,2表示出");
                                            XSSFCell createCell7 = contentxfrow.createCell(6);
                        createCell7.setCellValue("进入围栏时间");
                                            XSSFCell createCell8 = contentxfrow.createCell(7);
                        createCell8.setCellValue("离开围栏时间");
                                            XSSFCell createCell9 = contentxfrow.createCell(8);
                        createCell9.setCellValue("进入经纬度");
                                            XSSFCell createCell10 = contentxfrow.createCell(9);
                        createCell10.setCellValue("进入纬度");
                                            XSSFCell createCell11 = contentxfrow.createCell(10);
                        createCell11.setCellValue("离开经度");
                                            XSSFCell createCell12 = contentxfrow.createCell(11);
                        createCell12.setCellValue("离开纬度");
                                    }

                @Override
                public void write(XSSFRow contentxfrow, AreaInOutEntity t) {
                                            XSSFCell createCell1 = contentxfrow.createCell(0);
                        createCell1.setCellValue(t.getId());
                                            XSSFCell createCell2 = contentxfrow.createCell(1);
                        createCell2.setCellValue(t.getGeoId());
                                            XSSFCell createCell3 = contentxfrow.createCell(2);
                        createCell3.setCellValue(t.getGeoName());
                                            XSSFCell createCell4 = contentxfrow.createCell(3);
                        createCell4.setCellValue(t.getVehicleId());
                                            XSSFCell createCell5 = contentxfrow.createCell(4);
                        createCell5.setCellValue(t.getVehicleNo());
                                            XSSFCell createCell6 = contentxfrow.createCell(5);
                        createCell6.setCellValue(t.getType());
                                            XSSFCell createCell7 = contentxfrow.createCell(6);
                        createCell7.setCellValue(t.getEnterDate());
                                            XSSFCell createCell8 = contentxfrow.createCell(7);
                        createCell8.setCellValue(t.getLeaveDate());
                                            XSSFCell createCell9 = contentxfrow.createCell(8);
                        createCell9.setCellValue(t.getLng());
                                            XSSFCell createCell10 = contentxfrow.createCell(9);
                        createCell10.setCellValue(t.getLat());
                                            XSSFCell createCell11 = contentxfrow.createCell(10);
                        createCell11.setCellValue(t.getLeaveLng());
                                            XSSFCell createCell12 = contentxfrow.createCell(11);
                        createCell12.setCellValue(t.getLeaveLat());
                                    }
            };
            excelUtil.createHead(sfworkbook, createWorkbook, writeexcel);
            excelUtil.writeXlsFile(sfworkbook, createWorkbook, download, writeexcel);
            /**
             * 保存文件
             */
            String saveXlsFile = excelUtil.saveXlsFile(sfworkbook);
            excelUtil.sendExcel(response, saveXlsFile);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }


}
