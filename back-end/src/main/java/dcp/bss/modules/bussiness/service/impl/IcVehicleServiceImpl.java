package dcp.bss.modules.bussiness.service.impl;

import dcp.bss.web.util.Query;
import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.IcVehicleDao;
import dcp.bss.modules.bussiness.entity.IcVehicleEntity;
import dcp.bss.modules.bussiness.service.IcVehicleService;


@Service("icVehicleService")
public class IcVehicleServiceImpl extends ServiceImpl<IcVehicleDao, IcVehicleEntity> implements IcVehicleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<IcVehicleEntity> page = this.selectPage(
                new Query<IcVehicleEntity>(params).getPage(),
                new EntityWrapper<IcVehicleEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<IcVehicleEntity> queryViewVo) {
        Page<IcVehicleEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<IcVehicleEntity> download(QueryViewVo<IcVehicleEntity> queryViewVo) {
        List<IcVehicleEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
