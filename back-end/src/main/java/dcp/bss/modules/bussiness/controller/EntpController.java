package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.EntpEntity;
import dcp.bss.modules.bussiness.service.EntpService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 企业信息表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("entp")
@Slf4j
public class EntpController {
    @Autowired
    private EntpService entpService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    // @RequiresPermissions("bussiness:entp:list")
    public R list(QueryViewVo<EntpEntity> queryViewVo) {
        PageUtils page = entpService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }

    @RequestMapping("/select")
    public R select(String catCd) {
        EntityWrapper entityWrapper =  new EntityWrapper<EntpEntity>();
        if (catCd!=null) {
            entityWrapper.eq("cat_cd", catCd);
        }
        entityWrapper.setSqlSelect("id", "name");
        List<HashMap<String,Object>> entityList = entpService.selectMaps(entityWrapper);
        return R.ok().put("data", entityList);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:entp:info")
    public R info(@PathVariable("id") Long id) {
        EntpEntity entp = entpService.selectById(id);

        return R.ok().put("entp", entp);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:entp:save")
    public R save(@RequestBody EntpEntity entp) {
        entpService.insert(entp);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:entp:update")
    public R update(@RequestBody EntpEntity entp) {
        entpService.updateById(entp);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:entp:delete")
    public R delete(@RequestBody Long[] ids) {
        entpService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }

}
