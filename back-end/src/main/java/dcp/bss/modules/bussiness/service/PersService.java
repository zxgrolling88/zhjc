package dcp.bss.modules.bussiness.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.bussiness.entity.PersEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * 人员信息表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
public interface PersService extends IService<PersEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<PersEntity> queryViewVo);

    List<PersEntity> download(QueryViewVo<PersEntity> queryViewVo);
}

