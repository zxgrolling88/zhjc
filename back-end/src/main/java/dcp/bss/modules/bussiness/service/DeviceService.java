package dcp.bss.modules.bussiness.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.bussiness.entity.DeviceEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
public interface DeviceService extends IService<DeviceEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<DeviceEntity> queryViewVo);

    List<DeviceEntity> download(QueryViewVo<DeviceEntity> queryViewVo);

    DeviceEntity selectByCode(String code);
}

