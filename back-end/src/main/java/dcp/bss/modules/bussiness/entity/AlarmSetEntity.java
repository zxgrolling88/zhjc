package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_alarm_set")
public class AlarmSetEntity extends  BaseEntity{

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 报警类型
	 */
	private String catCd;
	/**
	 * 报警名称
	 */
	private String catNmCn;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 报警阈值
	 */
	private String alarmValue;


}
