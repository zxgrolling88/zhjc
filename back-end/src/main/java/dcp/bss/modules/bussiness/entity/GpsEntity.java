package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * GPS实时表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_gps")
public class GpsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * GPS主键
	 */
	@TableId
	private Long id;
	/**
	 * 车号
	 */
	private String vecNo;
	/**
	 * 经度
	 */
	private Double longitude;
	/**
	 * 纬度
	 */
	private Double latitude;
	/**
	 * 速度
	 */
	private Double speed;
	/**
	 * 方向
	 */
	private Integer direction;
	/**
	 * 状态
	 */
	private Integer state;
	/**
	 * gps更新时间
	 */
	private Date gpsTime;
	/**
	 * 里程
	 */
	private Double mileages;


}
