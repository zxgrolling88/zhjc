package dcp.bss.modules.bussiness.entity;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 07:38
 */
public class CmdChangeIpBean {

    @NotEmpty
    private String  lockDevID;
    @NotEmpty
    private String ip;
    @NotEmpty
    private String port;//锁类型，1表示密码锁，2表示普通锁

    public String getLockDevID() {
        return lockDevID;
    }

    public void setLockDevID(String lockDevID) {
        this.lockDevID = lockDevID;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
