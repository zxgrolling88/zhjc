package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import dcp.bss.modules.sys.entity.SysUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.VecEntity;
import dcp.bss.modules.bussiness.service.VecService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 车辆表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("vec")
@Slf4j
public class VecController {
    @Autowired
    private VecService vecService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    // @RequiresPermissions("bussiness:vec:list")
    public R list(QueryViewVo<VecEntity> queryViewVo) {
        PageUtils page = vecService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{vecPk}")
    // @RequiresPermissions("bussiness:vec:info")
    public R info(@PathVariable("vecPk") Long vecPk) {
        VecEntity vec = vecService.selectById(vecPk);
        return R.ok().put("vec", vec);
    }


    /**
     * 信息
     */
    @RequestMapping("/select")
    // @RequiresPermissions("bussiness:vec:info")
    public R select() {
        List<VecEntity> vec = vecService.selectList(new EntityWrapper<VecEntity>().setSqlSelect("vec_pk","vec_no"));
        return R.ok().put("data", vec);
    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:vec:save")
    public R save(@RequestBody VecEntity vec) {
        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        vec.setCrtBy(user.getUsername());
        vec.setUpdBy(user.getUsername());
        vecService.insert(vec);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:vec:update")
    public R update(@RequestBody VecEntity vec) {

        SysUserEntity user = (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
        vec.setUpdBy(user.getUsername());
        vecService.updateById(vec);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:vec:delete")
    public R delete(@RequestBody Long[] vecPks) {
        vecService.deleteBatchIds(Arrays.asList(vecPks));

        return R.ok();
    }


}
