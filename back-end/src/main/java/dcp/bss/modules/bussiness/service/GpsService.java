package dcp.bss.modules.bussiness.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.bussiness.entity.GpsEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * GPS实时表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
public interface GpsService extends IService<GpsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<GpsEntity> queryViewVo);

    List<GpsEntity> download(QueryViewVo<GpsEntity> queryViewVo);
}

