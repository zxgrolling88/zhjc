package dcp.bss.modules.bussiness.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 围栏表
 * 
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@Data
@TableName("biz_device_opr_log")
public class DeviceOprLogEntity {
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@TableId
	private Long id;

	/**
	 * 设备编号
	 */
	private String deviceCode;
	/**
	 * 操作类型
	 */
	private String oprType;
	/**
	 * 操作人
	 */
	private String oprator;
	/**
	 * 操作时间
	 */
	private Date oprTm;

	private Date crtTm;

	private Double lng;
	private Double lat;

	private String lockStatus;

	private String  cmdSrc;//操作源

	private String  result;//操作结果
}
