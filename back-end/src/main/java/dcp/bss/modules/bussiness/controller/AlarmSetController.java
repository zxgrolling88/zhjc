package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.AlarmSetEntity;
import dcp.bss.modules.bussiness.service.AlarmSetService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;


/**
 * 围栏表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("alarmset")
@Slf4j
public class AlarmSetController {
    @Autowired
    private AlarmSetService alarmSetService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    // @RequiresPermissions("bussiness:alarmset:list")
    public R list(QueryViewVo<AlarmSetEntity> queryViewVo) {
        PageUtils page = alarmSetService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:alarmset:info")
    public R info(@PathVariable("id") Long id) {
        AlarmSetEntity alarmSet = alarmSetService.selectById(id);

        return R.ok().put("alarmSet", alarmSet);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:alarmset:save")
    public R save(@RequestBody AlarmSetEntity alarmSet) {
        alarmSetService.insert(alarmSet);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:alarmset:update")
    public R update(@RequestBody AlarmSetEntity alarmSet) {
        alarmSetService.updateById(alarmSet);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:alarmset:delete")
    public R delete(@RequestBody Long[] ids) {
        alarmSetService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
     * 导出数据到excel
     *
     * @param response
     * @param queryViewVo
     * @return
     */
    @RequestMapping("/download")
    //  @RequiresPermissions("bussiness:alarmset:download")
    public void download(HttpServletResponse response, QueryViewVo<AlarmSetEntity> queryViewVo) {
        try {
            List<AlarmSetEntity> download = alarmSetService.download(queryViewVo);
            XSSFWorkbook sfworkbook = new XSSFWorkbook();
            ExcelUtil excelUtil = new ExcelUtil();
            SXSSFWorkbook createWorkbook = excelUtil.createWorkbook(sfworkbook);
            ExcelUtil.WriteExcel<AlarmSetEntity> writeexcel = new ExcelUtil.WriteExcel<AlarmSetEntity>() {

                @Override
                public void writeHead(XSSFRow contentxfrow) {
                    XSSFCell createCell1 = contentxfrow.createCell(0);
                    createCell1.setCellValue("");
                    XSSFCell createCell2 = contentxfrow.createCell(1);
                    createCell2.setCellValue("名称");
                    XSSFCell createCell3 = contentxfrow.createCell(2);
                    createCell3.setCellValue("报警类型");
                    XSSFCell createCell4 = contentxfrow.createCell(3);
                    createCell4.setCellValue("报警名称");
                    XSSFCell createCell5 = contentxfrow.createCell(4);
                    createCell5.setCellValue("创建时间");
                    XSSFCell createCell6 = contentxfrow.createCell(5);
                    createCell6.setCellValue("创建者");
                    XSSFCell createCell7 = contentxfrow.createCell(6);
                    createCell7.setCellValue("更新者");
                    XSSFCell createCell8 = contentxfrow.createCell(7);
                    createCell8.setCellValue("更新时间");
                    XSSFCell createCell9 = contentxfrow.createCell(8);
                    createCell9.setCellValue("修订状态：1100.10 新建 1100.80 已删除");
                    XSSFCell createCell10 = contentxfrow.createCell(9);
                    createCell10.setCellValue("状态");
                    XSSFCell createCell11 = contentxfrow.createCell(10);
                    createCell11.setCellValue("报警阈值");
                }

                @Override
                public void write(XSSFRow contentxfrow, AlarmSetEntity t) {
                    XSSFCell createCell1 = contentxfrow.createCell(0);
                    createCell1.setCellValue(t.getId());
                    XSSFCell createCell2 = contentxfrow.createCell(1);
                    createCell2.setCellValue(t.getName());
                    XSSFCell createCell3 = contentxfrow.createCell(2);
                    createCell3.setCellValue(t.getCatCd());
                    XSSFCell createCell4 = contentxfrow.createCell(3);
                    createCell4.setCellValue(t.getCatNmCn());
                    XSSFCell createCell5 = contentxfrow.createCell(4);
                    createCell5.setCellValue(t.getCrtTm());
                    XSSFCell createCell6 = contentxfrow.createCell(5);
                    createCell6.setCellValue(t.getCrtBy());
                    XSSFCell createCell7 = contentxfrow.createCell(6);
                    createCell7.setCellValue(t.getUpdBy());
                    XSSFCell createCell8 = contentxfrow.createCell(7);
                    createCell8.setCellValue(t.getUpdTm());
                    XSSFCell createCell9 = contentxfrow.createCell(8);
                    createCell9.setCellValue(t.getEditFlag());
                    XSSFCell createCell10 = contentxfrow.createCell(9);
                    createCell10.setCellValue(t.getStatus());
                    XSSFCell createCell11 = contentxfrow.createCell(10);
                    createCell11.setCellValue(t.getAlarmValue());
                }
            };
            excelUtil.createHead(sfworkbook, createWorkbook, writeexcel);
            excelUtil.writeXlsFile(sfworkbook, createWorkbook, download, writeexcel);
            /**
             * 保存文件
             */
            String saveXlsFile = excelUtil.saveXlsFile(sfworkbook);
            excelUtil.sendExcel(response, saveXlsFile);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }


}
