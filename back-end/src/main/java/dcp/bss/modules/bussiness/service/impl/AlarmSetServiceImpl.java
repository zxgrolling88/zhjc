package dcp.bss.modules.bussiness.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
 import java.util.List;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import dcp.bss.common.util.PageUtils;
import dcp.bss.web.util.Query;
import dcp.bss.query.QueryViewVo;
import dcp.bss.modules.bussiness.dao.AlarmSetDao;
import dcp.bss.modules.bussiness.entity.AlarmSetEntity;
import dcp.bss.modules.bussiness.service.AlarmSetService;


@Service("alarmSetService")
public class AlarmSetServiceImpl extends ServiceImpl<AlarmSetDao, AlarmSetEntity> implements AlarmSetService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<AlarmSetEntity> page = this.selectPage(
                new Query<AlarmSetEntity>(params).getPage(),
                new EntityWrapper<AlarmSetEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(QueryViewVo<AlarmSetEntity> queryViewVo) {
        Page<AlarmSetEntity> page = this.selectPage(queryViewVo.getPageUtil(), queryViewVo.getWrapper());
        return new PageUtils(page);
    }

    @Override
    public List<AlarmSetEntity> download(QueryViewVo<AlarmSetEntity> queryViewVo) {
        List<AlarmSetEntity> list = this.selectList(queryViewVo.getWrapper());
        return list;
    }

}
