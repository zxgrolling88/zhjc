package dcp.bss.modules.bussiness.service;

import com.baomidou.mybatisplus.service.IService;
import dcp.bss.common.util.PageUtils;
import dcp.bss.modules.bussiness.entity.AreaInOutEntity;
import dcp.bss.query.QueryViewVo;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
public interface AreaInOutService extends IService<AreaInOutEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(QueryViewVo<AreaInOutEntity> queryViewVo);

    List<AreaInOutEntity> download(QueryViewVo<AreaInOutEntity> queryViewVo);
}

