package dcp.bss.modules.bussiness.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.List;

import dcp.bss.common.util.PageUtils;
import dcp.bss.common.util.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import dcp.bss.query.QueryViewVo;
import dcp.bss.web.util.ExcelUtil;
import dcp.bss.modules.bussiness.entity.AlarmEntity;
import dcp.bss.modules.bussiness.service.AlarmService;


/**
 * 报警表
 *
 * @author caochaofeng
 * @email caochaofeng@dacanginfo.com
 * @date 2018-08-19 17:21:27
 */
@RestController
@RequestMapping("alarm")
@Slf4j
public class AlarmController {
    @Autowired
    private AlarmService alarmService;

    /**
     * 列表
     */
    @RequestMapping("/page")
    // @RequiresPermissions("bussiness:alarm:list")
    public R list(QueryViewVo<AlarmEntity> queryViewVo) {
        PageUtils page = alarmService.queryPage(queryViewVo);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("bussiness:alarm:info")
    public R info(@PathVariable("id") Long id) {
        AlarmEntity alarm = alarmService.selectById(id);

        return R.ok().put("alarm", alarm);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("bussiness:alarm:save")
    public R save(@RequestBody AlarmEntity alarm) {
        alarmService.insert(alarm);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("bussiness:alarm:update")
    public R update(@RequestBody AlarmEntity alarm) {
        alarmService.updateById(alarm);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("bussiness:alarm:delete")
    public R delete(@RequestBody Long[] ids) {
        alarmService.deleteBatchIds(Arrays.asList(ids));

        return R.ok();
    }



}
