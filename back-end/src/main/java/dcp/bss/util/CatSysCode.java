/*
 * Class: CatCode
 * Description: 【注意】请不要手动编辑这个类，应采用Ant任务slp.toolkit.scripthelper.task.CopyScript从EXCEL中读取，详细请参考《后端开发手册》的“设置Ant运行环境”部分。
 * Version: 1.0
 * Author: jzhu
 * Creation date: 2014年12月30日
 * (C) Copyright 2010-2015 SmarterLogistics.com Corporation Limited.
 * All rights reserved.
 */
package dcp.bss.util;


import org.apache.commons.lang.StringUtils;

/**
 * @author jzhu
 */
public enum CatSysCode {


    Empty("", "", "", "", ""),
    COLON(":", "冒号", "COLON", "", ""),
    ZheJiang("330000", "浙江", "ZheJiang", "", ""),
    ZheJiang_NingBo_ZhenHai("330211", "浙江宁波镇海区", "ZheJiang_NingBo_ZhenHai", "", ""),
    ZheJiang_ShaoXing_ShangYu("330604", "浙江宁波上虞", "ZheJiang_ShaoXing_ShangYu", "", ""),

    ArgmtWtIcCd("ArgmtWtIcCd", "ArgmtWtIcCd值", "ArgmtWtIcCd", "", ""),
    ArgmtWtIcCdLoad("1", "装货", "load", "", ""),
    ArgmtWtIcCdUnload("-1", "卸货", "unload", "", ""),
    ArgmtWtIcCdTu("0", "途经", "tuJing", "", ""),
    ArgmtWtIcCdGuo("2", "过境", "guoJing", "", ""),

    CntrTantTypeYaLi("", "压力罐", "Yali", "", ""),
    CntrTantTypeChangYa("", "常压罐", "ChangYa", "", ""),

    IsValidTrue("1", "已验证", "Valid", "", ""),
    IsValidFalse("0", "未验证", "NotValid", "", ""),
    IsValidError("2", "验证失败", "ErrorValid", "", ""),

    LicApproveStatus("LicApproveStatus", "证照审核状态", "LicApproveStatus", "", ""),
    HandleFlagUnApprove("0", "待审核", "HandleFlagUnApprove", "", ""),
    HandleFlagApproved("1", "审核通过", "HandleFlagApproved", "", ""),
    HandleFlagApproveReject("2", "审核未通过", "HandleFlagApproveReject", "", ""),

    LicSubjectPers("Pers", "证照主体-人员证照", "LicSubjectPers", "", ""),
    LicSubjectVec("Vec", "证照主体-车辆证照", "LicSubjectVec", "", ""),
    LicSubjectEntp("Entp", "证照主体-企业证照", "LicSubjectEntp", "", ""),

    ApproveBaiscInfo("Baisc", "基本信息审核", "ApproveBaiscInfo", "", ""),
    ApproveBaiscInfoOfVec("Vec", "车辆基本信息审核", "ApproveBaiscInfoOfVec", "", ""),
    ApproveBaiscInfoOfPers("Pers", "人员基本信息审核", "ApproveBaiscInfoOfPers", "", ""),
    ApproveBaiscInfoOfEntp("Entp", "企业基本信息审核", "ApproveBaiscInfoOfEntp", "", ""),

    EntpChemAreaCode("EntpChemAreaCode", "企业是属于化工区，港区等Code值", "EntpChemAreaCode", "", ""),
    EntpChemAreaCode_InChem("1", "化工区", "EntpChemAreaCode_InChem", "", ""),


    RevisionStatus("1100", "修订状态", "RevisionStatus", "", ""),
    RevisionNew("1100.10", "新建", "RevisionNew", "", ""),
    RevisionDeleted("1100.80", "已删除", "RevisionDeleted", "", ""),

    WhetherStatus("1101", "是否状态", "WhetherStatus", "", ""),
    WhetherStatusYes("1101.10", "是", "WhetherStatusYes", "", ""),
    WhetherStatusNo("1101.80", "否", "WhetherStatusNo", "", ""),

    GeneralApplyStat("1102", "一般申请状态", "GeneralApplyStatus", "", ""),
    GeneralApplyStatCreate("1102.150", "创建", "GeneralApplyStatCreate", "1", ""),
    GeneralApplyStatUpdate("1102.153", "修改", "GeneralApplyStatUpdate", "3", ""),
    GeneralApplyStatCreateInvalid("1102.155", "创建作废", "GeneralApplyStatCreateInvalid", "5", ""),
    GeneralApplyStatPending("1102.160", "待比对", "GeneralApplyStatPending", "10", ""),
    GeneralApplyStatPass("1102.165", "比对通过", "GeneralApplyStatPass", "15", ""),
    GeneralApplyStatReject("1102.170", "比对不通过", "GeneralApplyStatReject", "20", ""),
    GeneralApplyStatInvalid("1102.220", "作废", "GeneralApplyStatInvalid", "50", ""),

    BusScope("1103", "企业经营范围", "BusScope", "", ""),

    TaxLevel("1104", "纳税级别", "TaxLevel", "", ""),
    TaxLevelNormal("1104.150", "一般纳税人", "TaxLevelNormal", "", ""),

    UploadStatus("1105", "上传状态", "UploadStatus", "", ""),
    UploadStatusNot("1105.150", "未同步至省运管", "UploadStatusNot", "0", ""),
    UploadStatusDoing("1105.160", "正在同步", "UploadStatusDoing", "10", ""),
    UploadStatusFailure("1105.170", "同步至省运管失败", "UploadStatusFailure", "20", ""),
    UploadStatusSuccess("1105.180", "同步至省运管成功", "UploadStatusSuccess", "30", ""),
    UploadStatusSuccessBySelf("1105.190", "企业自行同步", "UploadStatusSuccessBySelf", "40", ""),

    RtePlanSyncStatus("1106", "电子路单同步状态", "RtePlanSyncStatus", "", ""),
    RtePlanSyncNot("1106.150", "未同步", "RtePlanSyncNot", "0", ""),
    RtePlanSyncDoing("1106.160", "正在同步至省运管", "RtePlanSyncDoing", "10", ""),
    RtePlanSyncFailure("1106.170", "同步省运管失败", "RtePlanSyncFailure", "20", ""),
    RtePlanSyncSuccess("1106.180", "同步省运管成功", "RtePlanSyncSuccess", "30", ""),

    RtePlanEndStatus("1108", "电子路单结束状态", "RtePlanEndStatus", "", ""),
    RtePlanEndNot("1108.150", "未完结", "RtePlanEndNot", "", ""),
    RtePlanEndDoing("1108.155", "完结进行中", "RtePlanEndDoing", "", ""),
    RtePlanEndFailure("1108.160", "完结失败", "RtePlanEndFailure", "", ""),
    RtePlanEndSuccess("1108.165", "完结成功", "RtePlanEndSuccess", "", ""),

    RteLineType("1107", "通行证线路类型", "RteLineType", "", ""),
    RteLineTypeLine("1107.150", "线路", "RteLineTypeLine", "0", ""),
    RteLineTypeArea("1107.160", "区域", "RteLineTypeArea", "10", ""),

    MeasureUnit("1115", "计量单位", "MeasureUnit", "", ""),
    MeasureUnitTon("1115.145", "吨", "MeasureUnitTon", "", ""),
    MeasureUnitKG("1115.150", "千克", "MeasureUnitKG", "", ""),
    MeasureUnitG("1115.155", "克", "MeasureUnitG", "", ""),
    MeasureUnitPound("1115.160", "磅", "MeasureUnitPound", "", ""),
    MeasureUnitMeter("1115.165", "米", "MeasureUnitMeter", "", ""),
    MeasureUnitDeciMeter("1115.170", "分米", "MeasureUnitDeciMeter", "", ""),
    MeasureUnitCentiMeter("1115.175", "厘米", "MeasureUnitCentiMeter", "", ""),
    MeasureUnitZhang("1115.180", "丈", "MeasureUnitZhang", "", ""),
    MeasureUnitChi("1115.185", "尺", "MeasureUnitChi", "", ""),
    MeasureUnitCun("1115.190", "寸", "MeasureUnitCun", "", ""),
    MeasureUnitFoot("1115.195", "英尺", "MeasureUnitFoot", "", ""),
    MeasureUnitInch("1115.200", "英寸", "MeasureUnitInch", "", ""),
    MeasureUnitStere("1115.205", "立方米", "MeasureUnitStere", "", ""),
    MeasureUnitLitre("1115.210", "升", "MeasureUnitLitre", "", ""),
    MeasureUnitGallon("1115.215", "加仑", "MeasureUnitGallon", "", ""),
    MeasureUnitPackage("1115.220", "包", "MeasureUnitPackage", "", ""),
    MeasureUnitSet("1115.225", "台", "MeasureUnitSet", "", ""),


    Schedule("1116", "时程", "Schedule", "", ""),
    SchedOnce("1116.150", "单次", "SchedOnce", "", ""),
    SchedCycle("1116.155", "周期", "SchedCycle", "", ""),
    SchedCycleEveryDay("1116.155.150", "天", "SchedCycleEveryDay", "", ""),
    SchedCycleEveryWeek("1116.155.155", "周", "SchedCycleEveryWeek", "", ""),
    SchedCycleEveryMonth("1116.155.156", "月", "SchedCycleEveryMonth", "", ""),
    SchedCycleTheDateEveryMonth("1116.155.160", "每月日历日", "SchedCycleTheDateEveryMonth", "", ""),
    SchedCycleTheDayEveryMonth("1116.155.165", "每月第几日", "SchedCycleTheDayEveryMonth", "", ""),
    SchedCycleEveryYear("1116.155.170", "年", "SchedCycleEveryYear", "", ""),

    TaskHolidayPolicy("1117", "任务遇节假日时处理策略", "TaskHolidayPolicy", "", ""),

    TaskHolidayPolicyPostpone("1117.150", "顺延", "TaskHolidayPolicyPostpone", "", ""),
    TaskHolidayPolicyCancel("1117.155", "取消", "TaskHolidayPolicyCancel", "", ""),
    TaskHolidayPolicyNormal("1117.160", "正常", "TaskHolidayPolicyNormal", "", ""),


    TaskMgmtStat("1118", "任务管理状态", "TaskMgmtStat", "", ""),
    TaskMgmtStatEnabled("1118.150", "启用", "TaskMgmtStatEnabled", "", ""),
    TaskMgmtStatDisabled("1118.155", "禁用", "TaskMgmtStatDisabled", "", ""),


    TaskExeStat("1119", "任务执行状态", "TaskExeStat", "", ""),
    TaskExeStatIdle("1119.150", "待执行", "TaskExeStatIdle", "", ""),
    TaskExeStatRunning("1119.155", "执行中", "TaskExeStatRunning", "", ""),


    Curr("1123", "币种", "Curr", "", ""),
    CNY("1123.150", "人民币", "CNY", "", ""),
    USD("1123.155", "美元", "USD", "", ""),


    PymtMeth("1127", "支付方式", "PymtMeth", "", ""),
    PymtMethCash("1127.150", "现金", "PymtMethCash", "", ""),
    PymtMethLC("1127.155", "信用证", "PymtMethLC", "", ""),
    PymtMetheCommerce("1127.160", "电子商务", "PymtMetheCommerce", "", ""),

    Invoice("1128", "发票状态", "Invoice", "", ""),
    InvoiceIng("1128.150", "待开票", "Invoiced", "", ""),
    Invoiced("1128.155", "已开票", "Invoiced", "", ""),
    InvoiceCancel("1128.160", "取消开票", "InvoiceCancel", "", ""),

    Refund("1129", "退款状态", "Refund", "", ""),
    RefundIng("1129.150", "待退款", "RefundIng", "", ""),
    Refunded("1129.155", "已退款", "Refunded", "", ""),
    RefundCancel("1129.160", "取消退款", "RefundCancel", "", ""),


    SuplMeth("1130", "供货方式", "SuplMeth", "", ""),
    SuplMethLongRange("1130.150", "长期供货", "SuplMethLongRange", "", ""),

    Location("1140", "位置", "Location", "", ""),
    ContryAndDistrict("1140.150", "国家或地区", "ContryAndDistrict", "", ""),

    OTHERS("1140.150.389", "其它国家或地区", "OTHERS", "", ""),
    ChinaAdminDistrict("1140.155", "中国行政区", "ChinaAdminDistrict", "", ""),

    LocGeoAddr("1140.160", "地理位置", "LocGeoAddr", "", ""),
    LocBusinessRegion("1140.165", "商业大区", "LocBusinessRegion", "", ""),
    LocBusinessContactInfo("1140.170", "商务联系方式", "LocBusinessContactInfo", "", ""),
    LocFamilyContactInfo("1140.175", "家庭联系方式", "LocFamilyContactInfo", "", ""),

    Vehicle("1180", "运输工具", "Vehicle", "", ""),
    VehicleTractor("1180.154", "牵引车", "VehicleTractor", "", ""),
    VehicleTractorHeavy("1180.154.150", "重型半挂牵引车", "VehicleTractorHeavy", "", ""),
    VehicleTractorMedium("1180.154.155", "中型半挂牵引车", "VehicleTractorMedium", "", ""),
    VehicleTractorLight("1180.154.160", "轻型半挂牵引车", "VehicleTractorMedium", "", ""),
    VehicleTractorDuty("1180.154.165", "重型罐式货车", "VehicleTractorDuty", "", ""),
    VehicleTractorDutyMedium("1180.154.166", "中型罐式货车", "VehicleTractorDutyMedium", "", ""),
    VehicleTractorBoxTruck("1180.154.170", "仓栅式货车", "VehicleTractorBoxTruck", "", ""),
    VehicleTractorGeneralTruck("1180.154.175", "普通货车", "VehicleTractorGeneralTruck", "", ""),
    VehicleTractorVan("1180.154.180", "厢式货车", "VehicleTractorVan", "", ""),
    VehicleTractorVanMedium("1180.154.182", "中型厢式货车", "VehicleTractorVanMedium", "", ""),
    VehicleTractorLorries("1180.154.185", "栏板车", "VehicleTractorLorries", "", ""),

    VehicleTrailer("1180.155", "挂车", "VehicleTrailer", "", ""),
    VehicleTrailerHalf("1180.155.150", "半挂车", "VehicleTrailerHalf", "", ""),
    VehTraiHalfHeavyNormal("1180.155.150.150", "重型普通半挂车", "VehTraiHalfHeavyNormal", "", ""),
    VehTraiHalfHeavyVan("1180.155.150.155", "重型厢式半挂车", "VehTraiHalfHeavyVan", "", ""),
    VehTraiHalfHeavyTank("1180.155.150.160", "重型罐式半挂车", "VehTraiHalfHeavyTank", "", ""),
    VehTraiHalfHeavyContainer("1180.155.150.165", "重型集装箱半挂车", "VehTraiHalfHeavyContainer", "", ""),
    VehTraiHalfHeavyDump("1180.155.150.170", "重型自卸半挂车", "VehTraiHalfHeavyDump", "", ""),
    VehTraiHalfHeavySpecStruct("1180.155.150.175", "重型特殊结构半挂车", "VehTraiHalfHeavySpecStruct", "", ""),
    VehTraiHalfHeavyBarnType("1180.155.150.180", "重型仓栅式半挂车", "VehTraiHalfHeavyBarnType", "", ""), //18.2.27 add
    VehTraiHalfMediumNormal("1180.155.150.250", "中型普通半挂车", "VehTraiHalfMediumNormal", "", ""),
    VehTraiHalfMediumVan("1180.155.150.255", "中型厢式半挂车", "VehTraiHalfMediumVan", "", ""),
    VehTraiHalfMediumTank("1180.155.150.260", "中型罐式半挂车", "VehTraiHalfMediumTank", "", ""),
    VehTraiHalfMediumContainer("1180.155.150.265", "中型集装箱半挂车", "VehTraiHalfMediumContainer", "", ""),
    VehTraiHalfMediumDump("1180.155.150.270", "中型自卸半挂车", "VehTraiHalfMediumDump", "", ""),
    VehTraiHalfMediumSpecStruct("1180.155.150.275", "中型特殊结构半挂车", "VehTraiHalfMediumSpecStruct", "", ""),
    VehTraiHalfLightNormal("1180.155.150.350", "轻型普通半挂车", "VehTraiHalfLightNormal", "", ""),
    VehTraiHalfLightVan("1180.155.150.355", "轻型厢式半挂车", "VehTraiHalfLightVan", "", ""),
    VehTraiHalfLightTank("1180.155.150.360", "轻型罐式半挂车", "VehTraiHalfLightTank", "", ""),
    VehTraiHalfLightContainer("1180.155.150.365", "轻型集装箱半挂车", "VehTraiHalfLightContainer", "", ""),
    VehTraiHalfLightDump("1180.155.150.370", "轻型自卸半挂车", "VehTraiHalfLightDump", "", ""),
    VehTraiHalfLightSpecStruct("1180.155.150.375", "轻型特殊结构半挂车", "VehTraiHalfLightSpecStruct", "", ""),
    VehTraiHalfHeavySemiTrailer("1180.155.150.380", "重型平板半挂车", "VehTraiHalfHeavySemiTrailer", "", ""),
    VehTraiHalfLightSemiTrailer("1180.155.150.385", "轻型平板半挂车", "VehTraiHalfLightSemiTrailer", "", ""),

    VehicleTrailerWhole("1180.155.155", "全挂车", "VehicleTrailerWhole", "", ""),
    VehTraiWholeHeavyNormal("1180.155.155.150", "重型普通全挂车", "VehTraiWholeHeavyNormal", "", ""),
    VehTraiWholeHeavyVan("1180.155.155.155", "重型厢式全挂车", "VehTraiWholeHeavyVan", "", ""),
    VehTraiWholeHeavyTank("1180.155.155.160", "重型罐式全挂车", "VehTraiWholeHeavyTank", "", ""),
    VehTraiWholeHeavyContainer("1180.155.155.165", "重型集装箱全挂车", "VehTraiWholeHeavyContainer", "", ""),
    VehTraiWholeHeavyDump("1180.155.155.170", "重型自卸全挂车", "VehTraiWholeHeavyDump", "", ""),
    VehTraiWholeHeavySpecStruct("1180.155.155.175", "重型特殊结构全挂车", "VehTraiWholeHeavySpecStruct", "", ""),
    VehTraiWholeMediumNormal("1180.155.155.250", "中型普通全挂车", "VehTraiWholeMediumNormal", "", ""),
    VehTraiWholeMediumVan("1180.155.155.255", "中型厢式全挂车", "VehTraiWholeMediumVan", "", ""),
    VehTraiWholeMediumTank("1180.155.155.260", "中型罐式全挂车", "VehTraiWholeMediumTank", "", ""),
    VehTraiWholeMediumContainer("1180.155.155.265", "中型集装箱全挂车", "VehTraiWholeMediumContainer", "", ""),
    VehTraiWholeMediumDump("1180.155.155.270", "中型自卸全挂车", "VehTraiWholeMediumDump", "", ""),
    VehTraiWholeMediumSpecStruct("1180.155.155.275", "中型特殊结构全挂车", "VehTraiWholeMediumSpecStruct", "", ""),
    VehTraiWholeLightNormal("1180.155.155.350", "轻型普通全挂车", "VehTraiWholeLightNormal", "", ""),
    VehTraiWholeLightVan("1180.155.155.355", "轻型厢式全挂车", "VehTraiWholeLightVan", "", ""),
    VehTraiWholeLightTank("1180.155.155.360", "轻型罐式全挂车", "VehTraiWholeLightTank", "", ""),
    VehTraiWholeLightContainer("1180.155.155.365", "轻型集装箱全挂车", "VehTraiWholeLightContainer", "", ""),
    VehTraiWholeLightDump("1180.155.155.370", "轻型自卸全挂车", "VehTraiWholeLightDump", "", ""),
    VehTraiWholeLightSpecStruct("1180.155.155.375", "轻型特殊结构全挂车", "VehTraiWholeLightSpecStruct", "", ""),
    VehTraiHeavyPlateFullTrailer("1180.155.155.380", "重型平板全挂车", "VehTraiHeavyPlateFullTrailer", "", ""),
    VehTraiMediumFlatFullTrailer("1180.155.155.385", "中型平板全挂车", "VehTraiMediumFlatFullTrailer", "", ""),
    VehTraiLightFlatFullTrailer("1180.155.155.390", "轻型平板全挂车", "VehTraiLightFlatFullTrailer", "", ""),


    VehicleOther("1180.157", "其他", "VehicleOther", "", ""),
    VehicleTank("1180.156", "罐体", "VehicleTank", "", ""),


    VehicleStat("1190", "车辆状态", "VehicleStat", "", ""),
    VehicleStatNomal("1190.150", "正常", "VehicleStatNomal", "", ""),
    VehicleStatLocked("1190.155", "冻结", "VehicleStatLocked", "", ""),
    VehicleStatCancel("1190.160", "注销", "VehicleStatCancel", "", ""),

    VehicleAndDevice("1200", "运输工具与设备", "VehicleAndDevice", "", ""),

    DataType("1995", "数据类型", "DataType", "", ""),
    DataTypeString("1995.150", "字符串", "DataTypeString", "", ""),
    DataTypeInteger("1995.155", "整型", "DataTypeInteger", "", ""),
    DataTypeShort("1995.156", "短整型", "DataTypeShort", "", ""),
    DataTypeDouble("1995.160", "浮点数", "DataTypeDouble", "", ""),
    DataTypeDate("1995.165", "日期", "DataTypeDate", "", ""),
    DataTypeTimestamp("1995.170", "时间戳", "DataTypeTimestamp", "", ""),


    InvolvedParty("2100", "相关方", "InvolvedParty", "", ""),
    IpHuman("2100.145", "人员", "IpHuman", "", ""),
    IpCargoOwner("2100.150", "货主", "IpCargoOwner", "", ""),
    IpSupplier("2100.155", "供应商", "IpSupplier", "", ""),
    IpCarrier("2100.160", "承运商", "IpCarrier", "", ""),
    IpCarrierWithWarehouse("2100.162", "承运商（带仓库）", "IpCarrierWithWarehouse", "", ""),
    IpCarrierWithWarehouseYgLog("2100.162.150", "永贵物流", "IpCarrierWithWarehouseYgLog", "", ""),
    IpDistributor("2100.165", "经销商", "IpDistributor", "", ""),
    IpCustomer("2100.170", "客户", "IpCustomer", "", ""),
    IpWarehouse("2100.175", "仓库", "IpWarehouse", "", ""),
    IpWareHourseDstrb("2100.175.100", "配送仓库", "IpWareHourseDstrb", "", ""),
    IpWareHourseWholesale("2100.175.105", "批发仓库", "IpWareHourseWholesale", "", ""),
    IpStore("2100.180", "门店", "IpStore", "", ""),
    IpConsignorConsigneeWithWarehouse("2100.182", "收货企业", "IpConsignorConsigneeWithWarehouse", "", ""),
    IpCorp("2100.185", "公司", "IpCorp", "", ""),
    IpCorpProd("2100.185.150", "生产企业", "IpCorpProd", "", ""),
    IpCorpProdLoadPoint("2100.185.150.150", "生产企业充装点", "IpCorpProdLoadPoint", "", ""),
    IpSyCorpProdLoadPoint("2100.185.150.155", "上虞生产企业充装点", "IpSyCorpProdLoadPoint", "", ""),
    IpCorpWashPoint("2100.185.160", "洗车点", "IpCorpWashPoint", "", ""),

    IpCorpTrans("2100.185.155", "运输企业", "IpCorpTrans", "", ""),
    IpCorpWh("2100.185.160", "仓储企业", "IpCorpWh", "", ""),
    IpBusUnit("2100.190", "业务单元", "IpBusUnit", "", ""),
    IpDept("2100.195", "部门", "IpDept", "", ""),
    IpBank("2100.200", "银行", "IpBank", "", ""),


    IpCorpBusinessType("2100.202", "企业业务分类", "IpCorpBusinessType", "", ""),
    IpCorpBusinessTypeProd("2100.202.205", "生产企业", "IpCorpBusinessTypeProd", "", ""),
    IpCorpBusinessTypeTrans("2100.202.210", "运输企业", "IpCorpBusinessTypeTrans", "", ""),
    IpCorpBusinessTypeStorate("2100.202.215", "仓储企业", "IpCorpBusinessTypeStorate", "", ""),


    IpStaff("2100.205", "员工", "IpStaff", "", ""),
    IpDriver("2100.205.150", "驾驶员", "IpDriver", "", ""),
    IpGuards("2100.205.190", "押运员", "IpGuards", "", ""),
    IpDriverAndGuards("2100.205.191", "驾驶员/押运员", "IpDriverAndGuards", "", ""),
    IpLoaderAndUnloader("2100.205.195", "装卸员", "IpLoaderAndUnloader", "", ""),
    IpSafer("2100.205.200", "安全员", "IpSafer", "", ""),
    IpCorpOwner("2100.205.205", "企业负责人", "IpCorpOwner", "", ""),
    IpGPSViewer("2100.205.210", "GPS监控人员", "IpGPSViewer", "", ""),

    IpGovernment("2100.206", "政府部门", "IpGovernment", "", ""),
    IpGovCheckPoint("2100.206.150", "政府部门登记点", "IpGovCheckPoint", "", ""),

    IpGovPolice("2100.206.155", "公安局", "IpGovPolice", "", ""),
    IpGovTraffic("2100.206.160", "交通部门", "IpGovTraffic", "", ""),
    IpGovMarketSupervision("2100.206.165", "市场监督局", "IpGovMarketSupervision", "", ""),
    IpGovSafetySupervision("2100.206.170", "安监局", "IpGovSafetySupervision", "", ""),
    IpGovCheckLic("2100.206.175", "证件审核人员", "IpGovCheckLic", "", ""),


    IpExternalSystem("2100.210", "外部系统", "IpExternalSystem", "", ""),


    IpStat("2101", "相关方状态", "IpStat", "", ""),
    IpStatNomal("2101.150", "正常", "IpStatNomal", "", ""),
    IpStatLocked("2101.155", "冻结", "IpStatLocked", "", ""),
    IpStatCancel("2101.160", "注销", "IpStatCancel", "", ""),

    IpStatPersService("2101.165.150", "在职", "IpStatPersService", "", ""),
    IpStatPersTurnOver("2101.165.155", "离职", "IpStatPersTurnOver", "", ""),


    IpLevel("2102", "相关方级别", "IpLevel", "", ""),
    IpLevelScale("2102.100", "规模级别", "IpLevelScale", "", ""),
    IpLevelScaleOthers("2102.100.100", "其他", "IpLevelScaleOthers", "", ""),

    IpPersSex("2103", "相关方人员性别", "IpPersSex", "", ""),
    IpPersSexMale("2103.150", "男", "M", "", ""),
    IpPersSexFemale("2103.155", "女", "F", "", ""),


    Arrangement("2105", "合约", "Arrangement", "", ""),


    ArgmtPymtRcpt("2105.180", "发票", "ArgmtPymtRcpt", "", ""),

    ArgmtPymtRcptNormal("2105.180.185", "普通发票", "ArgmtPymtRcptNormal", "", ""),
    ArgmtPymtRcptSpecial("2105.180.190", "专业发票", "ArgmtPymtRcptSpecial", "", ""),
    ArgmtPymtRcptVAT("2105.180.195", "增值税专用发票", "ArgmtPymtRcptVAT", "", ""),
    ArgmtReconStmtCashDeposit("2105.190.200", "现存", "ArgmtReconStmtCashDeposit", "", ""),
    ArgmtReconStmtTransferPymt("2105.190.205", "转支", "ArgmtReconStmtTransferPymt", "", ""),
    ArgmtReconStmtCashPymt("2105.190.210", "现支", "ArgmtReconStmtCashPymt", "", ""),
    ArgmtReconStmtTransferDeposit("2105.190.215", "转存", "ArgmtReconStmtTransferDeposit", "", ""),
    ArgmtReconStmtConsumption("2105.190.220", "消费", "ArgmtReconStmtConsumption", "", ""),

    ArgmtStat("2106", "合约状态", "ArgmtStat", "", ""),


    ArgmtStatRtePlan("2106.215", "路单状态", "ArgmtStatRtePlan", "", ""),


    ArgmtCd("2107", "合约编号前缀", "ArgmtCd", "", ""),
    ArgmtCdDSOShipOrd("2107.250", "配运模块运单编号前缀", "ArgmtCdDSOShipOrd", "", ""),
    ArgmtCdDRPRtePlan("2107.255", "配运模块路单编号前缀", "ArgmtCdDRPRtePlan", "", ""),
    /**
     * 例子：
     * ArgmtCdPPPPurchasePlan("2107.150", "采购模块采购计划编号前缀", "ArgmtCdPPPPurchasePlan", "", ""),
     * ArgmtCdSINIncomeNoteStmt("2107.265", "批发模块收入单前缀", "ArgmtCdSINIncomeNoteStmt", "", ""),
     **/




    Event("2550", "事件", "Event", "", ""),
    EventSyncDataReceive("2550.150", "系统同步数据接收", "EventSyncDataReceive", "", ""),
    EventSmsSend("2550.155", "系统短信发送", "EventSmsSend", "", ""),


    EventEmailSend("2550.165", "系统邮件发送", "EventAlarm", "", ""),
    ArgmtAndOtherArgmtItm("2560", "合约与其他合约项", "ArgmtAndOtherArgmtItm", "", ""),
    ArgmtAndOtherArgmtItmShipOrdAndRtePlanItm("2560.150", "配运单与配运路单项", "ArgmtAndOtherArgmtItmShipOrdAndRtePlanItm", "", ""),

    ProdCat("3015", "产品", "ProdCat", "", ""),
    ProdCatChem("3015.150", "危化品", "ProdCatChem", "", ""),

    ProdCatChemLevel1("3015.150.110", "爆炸品", "ProdCatChemLevel1", "", ""),
    ProdCatChemLevel11("3015.150.110.110", "具有整体爆炸危险的物质和物品", "ProdCatChemLevel11", "", ""),
    ProdCatChemLevel12("3015.150.110.120", "有迸射危险，但无整体爆炸危险的物质和物品", "ProdCatChemLevel12", "", ""),
    ProdCatChemLevel13("3015.150.110.130", "具有燃烧危险并有局部爆炸危险或局部进射危险或者两种危险都有，但无整体爆炸危险的物质和物品 ", "ProdCatChemLevel13", "", ""),
    ProdCatChemLevel14("3015.150.110.140", "不呈现重大危险的物质和物品", "ProdCatChemLevel14", "", ""),
    ProdCatChemLevel15("3015.150.110.150", "有整体爆炸危险的非常不敏感物质", "ProdCatChemLevel15", "", ""),
    ProdCatChemLevel16("3015.150.110.160", "无整体爆炸危险的极端不敏感物质", "ProdCatChemLevel16", "", ""),

    ProdCatChemLevel2("3015.150.120", "气体", "ProdCatChemLevel2", "", ""),
    ProdCatChemLevel21("3015.150.120.110", "易燃气体", "ProdCatChemLevel21", "", ""),
    ProdCatChemLevel22("3015.150.120.120", "非易燃无毒气体", "ProdCatChemLevel22", "", ""),
    ProdCatChemLevel23("3015.150.120.130", "毒性气体", "ProdCatChemLevel23", "", ""),

    ProdCatChemLevel3("3015.150.130", "易燃液体", "ProdCatChemLevel3", "", ""),

    ProdCatChemLevel4("3015.150.140", "易燃固体、易于自燃的物质、遇水放出易燃气体的物质", "ProdCatChemLevel4", "", ""),
    ProdCatChemLevel41("3015.150.140.110", "易燃固体、自反应物质和固态退敏爆炸品", "ProdCatChemLevel41", "", ""),
    ProdCatChemLevel42("3015.150.140.120", "易于自燃的物质", "ProdCatChemLevel42", "", ""),
    ProdCatChemLevel43("3015.150.140.130", "遇水放出易燃气体的物质", "ProdCatChemLevel43", "", ""),

    ProdCatChemLevel5("3015.150.150", "氧化性物质和有机过氧化物", "ProdCatChemLevel5", "", ""),
    ProdCatChemLevel51("3015.150.140.110", "氧化性物质", "ProdCatChemLevel51", "", ""),
    ProdCatChemLevel52("3015.150.140.120", "有机过氧化物", "ProdCatChemLevel52", "", ""),

    ProdCatChemLevel6("3015.150.160", "毒性物质和感染性物质", "ProdCatChemLevel6", "", ""),
    ProdCatChemLevel61("3015.150.160.110", "毒性物质", "ProdCatChemLevel61", "", ""),
    ProdCatChemLevel62("3015.150.160.120", "感染性物质", "ProdCatChemLevel62", "", ""),

    ProdCatChemLevel7("3015.150.170", "放射性物质", "ProdCatChemLevel7", "", ""),

    ProdCatChemLevel8("3015.150.180", "腐蚀性物质", "ProdCatChemLevel8", "", ""),

    ProdCatChemLevel9("3015.150.190", "杂项危险物质和物品，包括危害环境物质", "ProdCatChemLevel9", "", ""),


    LicCat("6015", "证照类型", "LicCat", "", ""),
    LicCatBasic("6015.150", "基本证照", "LicCatBasic", "", ""),
    LicCatCatgy("6015.165", "品类证照", "LicCatCatgy", "", ""),
    LicCatOldProd("6015.180", "旧品证照", "LicCatOldProd", "", ""),
    LicCatNewProd("6015.195", "新品证照", "LicCatNewProd", "", ""),
    LicCatPassPort("6015.200", "通行证", "LicCatPassPort", "", ""),
    LicCatPassPortScan("6015.200.150", "通行证扫描件", "LicCatPassPortScan", "", ""),

    LicCatBasicBusLicns("6015.150.150", "营业执照", "LicCatBasicBusLicns", "", ""),
    LicCatBasicOrgCode("6015.150.165", "组织机构代码", "LicCatBasicOrgCode", "", ""),
    LicCatBasicStTax("6015.150.180", "税务登记国税", "LicCatBasicStTax", "", ""),
    LicCatBasicLocTax("6015.150.195", "税务登记地税", "LicCatBasicLocTax", "", ""),
    LicCatQuarantine("6015.150.210", "卫生检疫证", "LicCatQuarantine", "", ""),
    LicCatQuality("6015.150.225", "检验报告", "LicCatQuality", "", ""),
    LicCatHygienePermit("6015.150.230", "卫生许可证", "LicCatHygienePermit", "", ""),
    LicCatAnimalAndPlant("6015.150.240", "动植物检疫证", "LicCatAnimalAndPlant", "", ""),
    LicCatProduction("6015.150.245", "生产许可证", "LicCatProduction", "", ""),
    LicCatTrademark("6015.150.250", "商标", "LicCatTrademark", "", ""),
    LicCatBarCode("6015.150.255", "条码", "LicCatBarCode", "", ""),
    LicCatBrand("6015.150.260", "品牌证", "LicCatBrand", "", ""),
    LicCatBrandAuthorization("6015.150.265", "品牌授权", "LicCatBrandAuthorization", "", ""),
    LicCatImport("6015.150.270", "进口商品报关单", "LicCatImport", "", ""),
    LicCatGreenCert("6015.150.275", "绿色证", "LicCatGreenCert", "", ""),
    LicCatVillages("6015.150.280", "乡镇证明", "LicCatVillages", "", ""),
    LicCatTobaccoAndWine("6015.150.285", "烟酒类批证", "LicCatTobaccoAndWine", "", ""),
    LicCatHealthProducts("6015.150.285", "保健品批证", "LicCatHealthProducts", "", ""),
    LicCatHygiene("6015.150.290", "卫生批件", "LicCatHygiene", "", ""),
    LicCatCCC("6015.150.295", "3C认证", "LicCatCCC", "", ""),
    LicCatPublish("6015.150.300", "出版经营许可证", "LicCatPublish", "", ""),
    LicCatManufacture("6015.150.305", "制品经营许可证", "LicCatManufacture", "", ""),
    LicCatManufacturer("6015.150.310", "生产商证照", "LicCatManufacturer", "", ""),
    LicCatGuarantee("6015.150.315", "担保证照", "LicCatGuarantee", "", ""),


    LicCatPersonalPort("6015.210", "人员相关证照", "LicCatPersonalPort", "", ""),
    LicCatPersonalID("6015.210.150", "个人身份证", "LicCatPersonalID", "身份证", ""),
    LicCatDriverCard("6015.210.155", "驾驶证", "LicCatDriverCard", "驾驶证", ""),
    //LicCatGuardsCard("6015.210.160", "押运证", "LicCatGuardsCard", "押运证", ""),
    LicCatQuaCertDriver("6015.210.165", "从业资格证（驾驶）", "LicCatQuaCertDriver", "从业资格证（驾驶）", ""),
    LicCatQuaCertGuards("6015.210.170", "从业资格证（押运）", "LicCatQuaCertGuards", "从业资格证（押运）", ""),

    LicCatLaborContract("6015.210.175", "劳动合同", "LicCatLaborContract", "劳动合同", ""),
    LicCatSecurityResponseLic("6015.210.180", "安全责任状", "LicCatSecurityResponseLic", "安全责任状", ""),
    //LicCatTempResidencePer("6015.210.185", "暂住证", "LicCatTempResidencePer", "暂住证", ""),
    //LicCatResidencePermit("6015.210.190", "居住证", "LicCatResidencePermit", "居住证", ""),
    //LicCatLaborContractSupply("6015.210.195", "劳动合同补充协议", "LicCatLaborContractSupply", "劳动合同补充协议", ""),
    LicCatQuaCertLoad("6015.210.205", "从业资格证（装卸）", "LicCatQuaCertLoad", "从业资格证（装卸）", ""),
    LicCatPersonalOthers("6015.210.500", "其他", "LicCatPersonalOthers", "其他", ""),

    LicCatVecPort("6015.220", "车辆相关证照", "LicCatVecPort", "", ""),
    LicCatVecDriving("6015.220.150", "行驶证", "LicCatVecDriving", "行驶证", ""),
    LicCatVecOperating("6015.220.155", "营运证", "LicCatVecOperating", "营运证", ""),
    //LicCatInsuranceCard("6015.220.160", "机动车辆保险证", "LicCatInsuranceCard", "保险卡", ""),
    //LicCatCommerialInsuranceCard("6015.220.165", "商业保险证", "LicCatCommerialInsuranceCard", "商业险", ""),
    //LicCatTankTestCard("6015.220.170", "罐体检验卡", "LicCatTankTestCard", "罐体检验卡", ""),
    //LicCatTracfficAccidentInsurCard("6015.220.175", "交强险", "LicCatTracfficAccidentInsurCard", "交强险", ""),
    //LicCatAdditionalTax("6015.220.180", "附加税", "LicCatAdditionalTax", "附加税", ""),
    LicCatMotoRegiLic("6015.220.185", "机动车登记证", "LicCatMotoRegiLic", "机动车登记证", ""),
    LicCatVecBasicInfo("6015.220.195", "车辆基本情况表", "LicCatVecBasicInfo", "车辆基本情况表", ""),
    LicCatGpsInstallCert("6015.220.200", "GPS终端安装证书", "LicCatGpsInstallCert", "GPS终端安装证书", ""),
    LicCatCarrierLiabilityInsurance("6015.220.205", "承运人责任险", "LicCatCarrierLiabilityInsurance", "承运人责任险", ""),
    LicCatSafetyEquipmentListAndPic("6015.220.210", "安全设备清单及照片", "LicCatSafetyEquipmentListAndPic", "安全设备清单及照片", ""),
    LicCatMotoOthers("6015.220.500", "其他", "LicCatMotoOthers", "其他", ""),


    LicCatCorpPort("6015.230", "企业相关证照", "LicCatCorpPort", "", ""),
    LicCatBusinessLicense("6015.230.150", "企业法人营业执照", "LicCatBusinessLicense", "企业法人营业执照", ""),
    LicCatRodeTransportLicense("6015.230.155", "道路运输经营许可证", "LicCatRodeTransportLicense", "道路运输经营许可证", ""),
    LicCatOrgCode("6015.230.160", "组织机构代码", "LicCatOrgCode", "组织机构代码", ""),
    LicCatTaxRegCertificate("6015.230.165", "税务登记证", "LicCatTaxRegCertificate", "税务登记证", ""),
    LicCatTransportContract("6015.230.170", "运输合同", "LicCatTransportContract", "运输合同", ""),
    LicCatSocialCreditCode("6015.230.175", "统一社会信用代码证", "LicCatSocialCreditCode", "统一社会信用代码证", ""),
    LicCatEntpSecurityResponse("6015.230.180", "企业安全责任承诺书", "LicCatEntpSecurityResponse", "企业安全责任承诺书", ""),
    LicCatCorpOthers("6015.230.500", "其他", "LicCatCorpOthers", "其他", ""),


    LicCatTankPort("6015.240", "罐体相关证照", "LicCatTankPort", "", ""),
    LicCatTankTestReport("6015.240.150", "罐体检验报告", "LicCatTankTestReport", "", ""),
    //LicCatExamine("6015.240.155", "检验", "LicCatExamine", "", ""),
    //LicCatFeature("6015.240.160", "特性", "LicCatFeature", "", ""),
    //LicCatMethane("6015.240.165", "常压", "LicCatMethane", "", ""),
    //LicCatManage("6015.240.170", "管理", "LicCatManage", "", ""),
    LicCatTankRegCert("6015.240.180", "移动压力容器登记证", "LicCatTankRegCert", "移动压力容器登记证", ""),
    //LicCatTankTechCharact("6015.240.185", "移动压力容器技术特性", "LicCatTankTechCharact", "移动压力容器技术特性", ""),
    LicCatDangerGoodsSafetyCard("6015.240.190", "危险品安全卡", "LicCatDangerGoodsSafetyCard", "危险品安全卡", ""),
    LicCatTankOthers("6015.240.500", "其他", "LicCatCorpOthers", "其他", ""),


    CertifStat("6020", "证照状态", "CertifStat", "", ""),
    CertifStatDraft("6020.150", "待审核", "CertifStatDraft", "", ""),
    CertifStatRejected("6020.155", "审核未通过", "CertifStatRejected", "", ""),
    CertifStatApproved("6020.160", "审核已通过", "CertifStatApproved", "", ""),
    CertifStatArchived("6020.165", "已归档", "CertifStatArchived", "", ""),





    MsgSystem("7900", "消息系统", "MsgSystem", "", ""),

    MsgSystemStatus("7901", "消息状态", "MsgSystemStatus", "", ""),
    MsgSystemStatusRead("7901.150", "已阅读", "MsgSystemStatusRead", "", ""),
    MsgSystemStatusUnRead("7901.155", "未阅读", "MsgSystemStatusUnRead", "", ""),

    MsgSendSms("7902", "消息发送至短信", "MsgSendSms", "", ""),
    MsgSendSmsYes("7902.150", "发送至短信", "Y", "", ""),
    MsgSendSmsNo("7902.155", "不发送至短信", "N", "", ""),


    MsgSystemCat("7903", "消息", "MsgSystemCat", "", ""),
    MsgSystemCatOfAdmin("7903.155", "管理员消息", "MsgSystemCatOfAdmin", "", ""),

    MsgSendSmsStatus("7904", "消息发送至短信的状态", "MsgSendSmsStatus", "", ""),
    MsgSendSmsStatusYes("7904.150", "发送短信成功", "Y", "", ""),
    MsgSendSmsStatusNo("7904.155", "发送短信失败", "N", "", ""),

    BusinessActivity("8000", "业务行为", "BusinessActivity", "", ""),


    RsrcAct("8005", "资源操作", "RsrcAct", "", ""),
    RsrcActQuery("8005.150", "查询", "RsrcActQuery", "150", ""),
    RsrcActAdd("8005.155", "新增", "RsrcActAdd", "155", ""),
    RsrcActUpdate("8005.160", "修改", "RsrcActUpdate", "160", ""),
    RsrcActConfirm("8005.165", "确认", "RsrcActConfirm", "165", ""),
    RsrcActDelete("8005.170", "删除", "RsrcActDelete", "170", ""),
    RsrcActArrange("8005.175", "安排供货", "RsrcActArrange", "175", ""),
    RsrcActImport("8005.180", "导入", "RsrcActImport", "180", ""),
    RsrcActExport("8005.185", "导出", "RsrcActExport", "185", ""),
    RsrcActArchive("8005.190", "归档", "RsrcActArchive", "190", ""),
    RsrcActRequest("8005.195", "发起申请", "RsrcActRequest", "195", ""),
    RsrcActImgUpload("8005.200", "图片上传", "RsrcActImgUpload", "200", ""),
    RsrcActImgPreview("8005.205", "图片预览", "RsrcActImgPreview", "205", ""),
    RsrcActApprove("8005.210", "审核通过", "RsrcActApprove", "210", ""),
    RsrcActDisapprove("8005.215", "审核不通过", "RsrcActDisapprove", "215", ""),
    RsrcActReject("8005.220", "驳回", "RsrcActReject", "220", ""),

    RsrcAndRsrc("8008", "资源", "RsrcAndRsrc", "", ""),
    RsrcAndRsrcLeftGroupRight("8008.150", "资源业务分组与资源", "RsrcAndRsrcLeftGroupRight", "", ""),


    Resource("8010", "资源", "Resource", "", ""),
    RsrcURL("8010.150", "URL", "RsrcURL", "", ""),
    RsrcMenuItem("8010.155", "菜单项", "RsrcMenuItem", "", ""),
    RsrcInfoRow("8010.160", "信息行", "RsrcInfoRow", "", ""),
    RsrcInfoCol("8010.165", "信息列", "RsrcInfoCol", "", ""),
    RsrcImg("8010.170", "图片", "RsrcImg", "", ""),
    RsrcVideo("8010.175", "视频", "RsrcVideo", "", ""),
    RsrcVideoHelpVideo("8010.175.150", "帮助视频", "RsrcVideoHelpVideo", "", ""),
    RsrcBusGrp("8010.180", "资源业务分组", "RsrcBusGrp", "", ""),
    ExcelTemplate("8010.185", "EXCEL模板", "ExcelTemplate", "", ""),
    RsrcDoc("8010.190", "文档", "RsrcDoc", "", ""),
    RsrcDocHelpDoc("8010.190.150", "帮助文档", "RsrcDocHelpDoc", "", ""),

    //企业相关证件和报警 8010.20开头
    EntpLicRsrcExceedAlarm("1050.8010.20", "企业证件过期预警", "EntpLicRsrcExceedAlarm", "企业证件过期预警", ""),
    BusinessLicRsrc("8010.200", "企业营业执照", "BusinessLicRsrc", "", ""),
    BusinessLicRsrcFront("8010.200.150", "企业营业执照正面", "BusinessLicRsrcFront", "", ""),

    BusinessLicRsrcExceedAlarm("1050.8010.200", "企业营业执照过期报警", "BusinessLicRsrcExceedAlarm", "企业营业执照过期报警", ""),
    BusinessLicRsrcPreAlarm("1000.8010.200", "企业营业执照到期预警", "BusinessLicRsrcPreAlarm", "企业营业执照到期预警", ""),

    EntpOrgCodeCertRsrc("8010.201", "企业组织机构代码证", "EntpOrgCodeCertRsrc", "", ""),
    EntpOrgCodeCertRsrcFront("8010.201.150", "企业组织机构代码证正面", "EntpOrgCodeCertRsrcFront", "", ""),

    EntpOrgCodeCertRsrcExceedAlarm("1050.8010.201", "企业组织机构代码证过期报警", "EntpOrgCodeCertRsrcExceedAlarm", "企业组织机构代码证过期报警", ""),
    EntpOrgCodeCertRsrcPreAlarm("1000.8010.201", "企业组织机构代码证到期预警", "EntpOrgCodeCertRsrcPreAlarm", "企业组织机构代码证到期预警", ""),


    EntpTaxRegRsrc("8010.202", "企业税务登记证", "EntpTaxRegRsrc", "", ""),
    EntpTaxRegRsrcFront("8010.202.150", "企业税务登记证正面", "EntpTaxRegRsrcFront", "", ""),

    EntpTaxRegRsrcExceedAlarm("1050.8010.202", "企业税务登记证过期报警", "EntpTaxRegRsrcExceedAlarm", "企业税务登记证过期报警", ""),
    EntpTaxRegRsrcPreAlarm("1000.8010.202", "企业税务登记证到期预警", "EntpTaxRegRsrcPreAlarm", "企业税务登记证到期预警", ""),

    EntpRoadLicRsrc("8010.203", "企业道路运输经营许可证", "EntpRoadLicRsrc", "", ""),
    EntpRoadLicRsrcFront("8010.203.150", "企业道路运输经营许可证正面", "EntpRoadLicRsrcFront", "", ""),

    EntpRoadLicRsrcExceedAlarm("1050.8010.203", "企业道路运输经营许可证过期报警", "EntpRoadLicRsrcExceedAlarm", "企业道路运输经营许可证过期报警", ""),
    EntpRoadLicRsrcPreAlarm("1000.8010.203", "企业道路运输经营许可证到期预警", "EntpRoadLicRsrcPreAlarm", "企业道路运输经营许可证到期预警", ""),

    EntpSafeReponseRsrc("8010.204", "企业安全责任承诺书", "EntpSafeReponseRsrc", "", ""),
    EntpSafeReponseRsrcFront("8010.204.150", "企业安全责任承诺书正面", "EntpSafeReponseRsrcFront", "", ""),

    EntpSafeReponseRsrcExceedAlarm("1050.8010.204", "企业安全责任承诺书过期报警", "EntpSafeReponseRsrcExceedAlarm", "企业安全责任承诺书过期报警", ""),
    EntpSafeReponseRsrcPreAlarm("1000.8010.204", "企业安全责任承诺书到期预警", "EntpSafeReponseRsrcPreAlarm", "企业安全责任承诺书到期预警", ""),

    ChemBusiLicRsrc("8010.205", "危险化学品经营许可证", "ChemBusiLicRsrc", "", ""),
    ChemBusiLicRsrcFront("8010.205.150", "危险化学品经营许可证正面", "ChemBusiLicRsrcFront", "", ""),


    ChemBusiLicRsrcExceedAlarm("1050.8010.205", "危险化学品经营许可证过期报警", "ChemBusiLicRsrcExceedAlarm", "危险化学品经营许可证过期报警", ""),
    ChemBusiLicRsrcPreAlarm("1000.8010.205", "危险化学品经营许可证到期预警", "ChemBusiLicRsrcPreAlarm", "危险化学品经营许可证到期预警", ""),

    PortBusiLicRsrc("8010.206", "港口经营许可证", "PortBusiLicRsrc", "", ""),
    PortBusiLicRsrcFront("8010.206.150", "港口经营许可证", "PortBusiLicRsrcFront", "", ""),

    PortBusiLicRsrcExceedAlarm("1050.8010.206", "港口经营许可证过期报警", "PortBusiLicRsrcExceedAlarm", "港口经营许可证过期报警", ""),
    PortBusiLicRsrcPreAlarm("1000.8010.206", "港口经营许可证到期预警", "PortBusiLicRsrcPreAlarm", "港口经营许可证到期预警", ""),

    EntpBasicInfo("8010.207", "企业基本信息", "EntpBasicInfo", "", ""),

    //车辆相关证件和报警 8010.30
    VecLicRsrcExceedAlarm("1050.8010.30", "车辆证件过期预警", "VecLicRsrcExceedAlarm", "车辆证件过期预警", ""),
    VecTransLicRsrc("8010.300", "车辆道路运输证", "VecTransLicRsrc", "", ""),
    MainTaxiLicRsrcFront("8010.300.150", "营运证正本正面", "MainTaxiLicRsrcFront", "", ""),
    MainProxLicRsrcFront("8010.300.151", "待理证正面", "MainProxLicRsrcFront", "", ""),
    MainProxLicRsrcBehind("8010.300.152", "待理证反面", "MainProxLicRsrcBehind", "", ""),
    VecTecRankEstRsrcFront("8010.300.153", "车辆技术等级评定卡", "VecTecRankEstRsrcFront", "", ""),

    VecTransLicRsrcExceedAlarm("1050.8010.300", "车辆道路运输证过期报警", "VecTransLicRsrcExceedAlarm", "车辆道路运输证过期报警", ""),
    VecTransLicRsrcPreAlarm("1000.8010.300", "车辆道路运输证到期预警", "VecTransLicRsrcPreAlarm", "车辆道路运输证到期预警", ""),

    LicCatMotoRegiLicRsrc("8010.301", "机动车登记证", "LicCatMotoRegiLicRsrc", "机动车登记证", ""),
    LicCatMotoRegiLicInfoRsrc("8010.301.150", "机动车登记证信息栏", "LicCatMotoRegiLicInfoRsrc", "机动车登记证信息栏", ""),
    LicCatMotoRegiLicFirstInfoRsrc("8010.301.151", "机动车登记证登记栏(最近一次登记记录页)", "LicCatMotoRegiLicFirstInfoRsrc", "机动车登记证登记栏(最近一次登记记录页)", ""),


    VecDrivingRsrc("8010.302", "车辆行驶证", "VecDrivingRsrc", "", ""),

    MainVecDrivingRsrcFront("8010.302.150", "行驶证正本正面", "MainVecDrivingRsrcFront", "", ""),
    MainVecDrivingRsrcBehind("8010.302.151", "行驶证正本反面", "MainVecDrivingRsrcBehind", "", ""),
    SubVecDrivingRsrcFront("8010.302.152", "行驶证副本正面", "SubVecDrivingRsrcFront", "", ""),
    SubVecDrivingRsrcBehind("8010.302.153", "行驶证副本反面", "SubVecDrivingRsrcBehind", "", ""),


    EventAlarm("2550.160", "报警", "EventAlarm", "", ""),
    EventInnerGeofenceAlarm("2550.160.155", "驶入围栏报警", "EventInnerGeofenceAlarm", "", ""),
    EventOuterGeofenceAlarm("2550.160.160", "驶出围栏报警", "EventOuterGeofenceAlarm", "", ""),
    EventOverDriveAlarm("2550.160.165", "疲劳驾驶报警", "EventOverDriveAlarm", "", ""),
    EventOverStayAlarm("2550.160.275", "停车报警", "EventOverStayAlarm", "", ""),
    EventDeviateRouteAlarm("2550.160.170", "偏离路线报警", "EventDeviateRouteAlarm", "", ""),
    EventOverSpeedAlarm("2550.160.150", "超速报警", "EventOverSpeedAlarm", "", ""),
    EventOverLoadAlarm("2550.160.175", "超重报警", "EventOverLoadAlarm", "", ""),
    EventOverWeighAlarm("2550.160.180", "超载报警", "EventOverWeighAlarm", "", ""),
    TraiOverWeighAlarm("2550.160.180.150", "挂车超载报警", "TraiOverWeighAlarm", "", ""),
    TracOverWeighAlarm("2550.160.180.160", "牵引车超载报警", "TracOverWeighAlarm", "", ""),
    EventOverBusScopeAlarm("2550.160.185", "超经营范围报警", "EventOverBusScopeAlarm", "", ""),
    TraiOverBusScopeAlarm("2550.160.185.150", "超挂车经营范围报警", "TraiOverBusScopeAlarm", "", ""),
    TracOverBusScopeAlarm("2550.160.185.160", "超牵引车经营范围报警", "TracOverBusScopeAlarm", "", ""),
    LicNotExistAlarm("2550.7120.210", "证件缺失报警", "LicNotExistAlarm", "证件缺失报警", ""),
    NotHavingGps("2550.7120.160", "无GPS报警", "NotHavingGps", "2550.7120.160", ""),
    NotRteplan("2550.7120.190", "无电子运单报警", "NotRteplan", "2550.7120.190", ""),
    NotRecord("2550.7120.155", "未登记报警", "NotRecord", "2550.7120.155", ""),
    TractorNotRecord("2550.7120.155.100", "牵引车未登记报警", "TractorNotRecord", "2550.7120.155.100", ""),
    TrailerNotRecord("2550.7120.155.105", "挂车未登记报警", "TractorNotRecord", "2550.7120.155.105", ""),

    AlarmAutoHandlerYes("2550.7120.160", "报警已自动处理", "AlarmAutoHandlerYes", "1", ""),
    AlarmPushNo("2550.7120.155.165", "报警未推送", "AlarmPushNo", "0", ""),

    AlarmHandlerYes("2550.7120.150", "报警已处理", "AlarmHandlerYes", "3", ""),
    AlarmHandlerNo("2550.7120.155", "报警未处理", "AlarmHandlerNo", "2", ""),

    PreAlarm("2550.170", "预警", "PreAlarm", "", ""),
    PreOverStayAlarm("2550.170.275", "停车预警", "PreOverStayAlarm", "", ""),
    PreOverSpeedAlarm("2550.170.150", "超速预警", "PreOverSpeedAlarm", "", ""),
    PreDeviateRouteAlarm("2550.170.170", "偏离路线预警", "PreDeviateRouteAlarm", "", ""),


    Preference("8015", "首选项", "Preference", "", ""),
    PrefLookAndFeel("8015.150", "外观首选项", "PrefLookAndFeel", "", ""),
    PrefOtherPref("8015.155", "其他首选项", "PrefOtherPref", "", ""),
    PrefLookAndFeelMarauder("8015.150.150", "活点地图", "PrefLookAndFeelMarauder", "", ""),
    PrefLookAndFeelComt("8015.150.160", "社区数据库配置", "PrefLookAndFeelComt", "", ""),
    Authorization("8020", "访问授权", "Authorization", "", ""),
    AuthorRead("8020.150", "读取", "AuthorRead", "", ""),
    AuthorAdd("8020.155", "增加", "AuthorAdd", "", ""),
    AuthorDel("8020.160", "删除", "AuthorDel", "", ""),
    AuthorModify("8020.165", "修改", "AuthorModify", "", ""),

    /**
     * 操作日志分类编码
     */
    OpraLogAct("8030", "操作日志", "OpraLogAct", "", ""),
    OpraLogActAdd("8030.150", "新增", "OpraLogActAdd", "", ""),
    OpraLogActUpd("8030.155", "修改", "OpraLogActUpd", "", ""),
    OpraLogActDel("8030.160", "删除", "OpraLogActDel", "", ""),
    OpraLogActQuery("8030.165", "查询", "OpraLogActQuery", "", ""),

    OpraLogCat("8031", "操作日志类型", "OpraLogCat", "", ""),
    OpraLogCatRtePlan("8031.150", "电子运单", "OpraLogCatRtePlan", "", ""),
    OpraLogCatPers("8031.160", "人员", "OpraLogCatPers", "", ""),
    OpraLogCatVec("8031.170", "车辆", "OpraLogCatVec", "", ""),
    OpraLogCatEntp("8031.180", "企业", "OpraLogCatEntp", "", ""),
    OpraLogCatCntr("8031.190", "罐体", "OpraLogCatCntr", "", ""),


    SysStat("9010", "系统状态", "SysStat", "", ""),
    SysStatVecLimit("9010.150", "车辆限制其他属性输入", "SysStatVecLimit", "", ""),
    /**
     * SysStatAccptRcptCnt("9010.159", "验收单记录数", "SysStatAccptRcptCnt", "", ""),
     */

    SysParam("9015", "系统参数", "SysParam", "", ""),
    SysParamDefaultAsmtDayNum("9015.150", "默认评价设置天数", "SysParamDefaultAsmtDayNum", "5", ""),

    AliMQMsgTag_PersMsgTag("9017", "人员消息标签", "AliMQMsgTag_PersMsgTag", "", ""),

    AliMQMsgTag_VecMsgTag("9018", "车辆消息标签", "AliMQMsgTag_VecMsgTag", "", ""),

    AliMQMsgTag_EntpMsgTag("9018", "企业消息标签", "AliMQMsgTag_EntpMsgTag", "", ""),

    AliMQMsgTag_TankMsgTag("9018", "罐体消息标签", "AliMQMsgTag_TankMsgTag", "", ""),

    AliMQMsgTag_ArgmtWtMsgTag("9018", "电子运单消息标签", "AliMQMsgTag_ArgmtWtMsgTag", "", ""),;

    /**
     * 分类代码Code
     */
    private String cd;

    /**
     * 中文名称
     */
    private String nmCn;

    /**
     * 英文名称
     */
    private String nmEn;

    /**
     * 业务排序编号
     */
    private String busOrd;

    /**
     * 显示排序编号
     */
    private String viewOrd;


    private CatSysCode(String cd, String nmCn, String nmEn, String busOrd, String viewOrd) {
        this.cd = cd;
        this.nmEn = nmEn;
        this.nmCn = nmCn;
        this.busOrd = busOrd;
        this.viewOrd = viewOrd;
    }

    public String cd() {
        return cd;
    }

    public String nmEn() {
        return nmEn;
    }

    public String nmCn() {
        return nmCn;
    }

    public String busOrd() {
        return busOrd;
    }

    public String viewOrd() {
        return viewOrd;
    }


    /**
     * 通过Cd值找出对应的CatSysCode
     *
     * @param cd
     * @return
     */
    public static CatSysCode findCatSysCodeByCd(String cd) {
        for (CatSysCode code : CatSysCode.values()) {
            if (cd.equals(code.cd())) {
                return code;
            }
        }
        return null;
    }

    /**
     * 通过Cd值找出对应的CatSysCode
     *
     * @param codeNmEn
     * @return
     */
    public static CatSysCode findCatSysCodeByNmEn(String codeNmEn) {
        for (CatSysCode code : CatSysCode.values()) {
            if (codeNmEn.equals(code.nmEn)) {
                return code;
            }
        }
        return null;
    }

    /**
     * 通过Cd值找出对应的CatSysCode
     *
     * @param codeNmCn
     * @return
     */
    public static CatSysCode findCatSysCodeByNmCn(String codeNmCn) {
        if (StringUtils.isNotBlank(codeNmCn)) {
            for (CatSysCode code : CatSysCode.values()) {
                if (codeNmCn.equals(code.nmCn)) {
                    return code;
                }
            }
        }
        return null;
    }

    /**
     * 返回上一级父类型的CatSysCode
     *
     * @param sonCd 子类型的CatSysCode
     * @return
     */
    public static CatSysCode findFatherSysCodeByCd(String sonCd) {
        int lastDotIndex = sonCd.lastIndexOf(".");
        String fatherCd = sonCd.substring(0, lastDotIndex);
        for (CatSysCode code : CatSysCode.values()) {
            if (fatherCd.equals(code.cd())) {
                return code;
            }
        }
        return null;
    }

    public static void main(String args[]) {
        CatSysCode catSysCode =    findFatherSysCodeByCd("8010.304.150");
        System.out.println(catSysCode.cd() + "-" + catSysCode.nmEn() + "-" + catSysCode.nmCn());

    }
}