package dcp.bss.common.util;

/**
 * @project: zhjc
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2019-05-11 06:13
 */

/**
 * @(#)jizhanyun.com for java
 *
 * 基站云平台接口演示FOR JAVA
 *
 * @基站云
 * @version 1.00
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BaseStationAPI {

    private static String templateLbsUrl = "http://api.lbs.heclouds.com/api/gsmlbs";
    private static String sicode = "6E82CCDBD851388857C497F2F44C8D5E";
    private static HttpURLConnection connection;

    /**
     * 基站定位
     *
     * @param mcc 国家代码：中国代码;460
     * @param mnc 网络类型0移动1联通
     * @param lac 小区号
     * @param cell 基站号
     *
     * @return lng	经度
     * @return lat	纬度
     * @return g_lng	纠偏后的经度(适用于高德/腾讯搜搜/搜狐搜狗/阿里云/灵图/谷歌地图显示)
     * @return g_lat	纠偏后的纬度(适用于高德/腾讯搜搜/搜狐搜狗/阿里云/灵图/谷歌地图显示)
     * @return b_lng	纠偏后的经度(适用于百度/图吧地图显示)
     * @return b_lat	纠偏后的纬度(适用于百度/图吧地图显示)
     */
    public static String getLocation(String mcc,String mnc,Integer lac, Integer cell) {
        String formParam = "mcc=" + mcc + "&mnc=" + mnc + "&lac=" + lac+ "&cell=" + cell + "&apikey=" + sicode;
        StringBuffer sb = new StringBuffer();
        int code = 0;
        try {
            connection = (HttpURLConnection) new URL(templateLbsUrl).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(5000);
            connection.setConnectTimeout(5000);

            connection.getOutputStream().write(formParam.getBytes());
            connection.getOutputStream().close();
            code = connection.getResponseCode();
            InputStream stream = null;
            if (code == 200) {
                stream = connection.getInputStream();
            } else {
                stream = connection.getErrorStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(stream, "utf-8"));
            String str = null;
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
            System.out.println("调用位置接口" + (code == 200 ? "成功" : "失败") + "返回结果：" + sb.toString());
            if(code == 200) {
                return sb.toString();
            }else{
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }


    }

    public static void main(String[] args) {
        getLocation("460","00",34860,62041);


    }

}
