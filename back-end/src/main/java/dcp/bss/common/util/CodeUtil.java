package dcp.bss.common.util;


import java.util.Date;

/**
 * @author yuyi
 */
public class CodeUtil {

    public static String getRandomOnlyCode(String codePrefix) {
        String timestampFormat = DateUtils.formatDate(new Date(),DateUtils.VAL_TIMESTAMP_FORMAT);
        timestampFormat = timestampFormat.replace("-", "");
        timestampFormat = timestampFormat.replace(".", "");
        return codePrefix + timestampFormat + getRandomThreeeDigit();
    }
    
    public static int getRandomThreeeDigit() {
        int i=(int)(Math.random()*900)+100; 
        return i;
    }
    public static void main(String [] srgs) { 
    //int i=(int)(Math.random()*900)+100; 
    //int i= new java.util.Random().nextInt(900)+100;也可以
        String i = getRandomOnlyCode("DDR");
    System.out.println(i); 

    } 
}
