package dcp.bss.common.util;

import org.apache.commons.lang.StringUtils;

import java.util.Random;

public class PwdUtil {

	private static String patten = "^(?![a-zA-Z]+$)(?![A-Z0-9]+$)(?![A-Z\\W_]+$)(?![a-z0-9]+$)(?![a-z\\W_]+$)(?![0-9\\W_]+$)[a-zA-Z0-9\\W_]{8,}$";


	//随机密码生成
	private static String makeRandomPassword(int len) {
		char charr[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random r = new Random();

		for (int x = 0; x < len; ++x) {
			sb.append(charr[r.nextInt(charr.length)]);
		}
		return sb.toString();
	}
	//获取验证过的随机密码
	public static String getRandomPassword(int len) {
		String result = null;
		result = makeRandomPassword(len);
		if (result.matches(".*[a-z]{1,}.*") && result.matches(".*[A-Z]{1,}.*") && result.matches(".*[0-9]{1,}.*") ) {
			return result;
		}
		return getRandomPassword(len);
	}
	public static boolean match(String password) {
		if (StringUtils.isBlank(password)) {
			return false;
		}
		return password.matches(patten);
	}


	public static void main(String[] args) {

		String password = getRandomPassword(8);
		System.out.println(password);


	}
}

