package dcp.bss.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CollUtils {

	/**
     * 获取集合的第一个对象
     * @param c
     * @return
     */
    public static final <T> T get0(Collection<T> c) {
        if (CollectionUtils.isEmpty(c)) {
            return null;
        }
        return c.iterator().next();
    }
    
    /**
     * 一个集合，拼接成一个带符号的字符串；{"1", "2"} -> ('1', '2')
     * @param c
     * @return
     */
    public static final String toSymbolString(Collection<String> c) {
        StringBuffer strCond = new StringBuffer("(");
        boolean isFirst = true;
        for (String tmpStatus: c) {
            if (isFirst) {
                strCond.append("'").append(tmpStatus).append("'");
                isFirst = false;
            } else
                strCond.append(",'").append(tmpStatus).append("'");
        }
        strCond.append(")");
        return strCond.toString();
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static List<Long> toLongList(String args) {
        if (null == args) {
            return null;
        }
        String[] strs = StringUtils.split(args, ",");
        List<Long> longList = new ArrayList<Long>();
        for (String str : strs) {
            longList.add(Long.valueOf(str));
        }
        return longList;
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static List<String> toStringList(String args) {
        if (null == args) {
            return null;
        }
        String[] strs = StringUtils.split(args, ",");
        List<String> strList = new ArrayList<String>();
        for (String str : strs) {
            strList.add(str);
        }
        return strList;
    }
    
}
