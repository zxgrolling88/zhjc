package dcp.bss.common.util;

public class PageTools {

	public static String getLimitSql(Integer page, Integer limit) {
		return new StringBuffer(" limit ").append(getStartRow(page, limit))
				.append(", ").append(limit).append(" ").toString();
	}
	
	public static Integer getStartRow(Integer page, Integer limit) {
		if (null == page) {
			page = 0;
		}
		if (null == limit) {
			page = 15;
		}
		return (page - 1) * limit;
	}
}
