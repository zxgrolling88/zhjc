/**
 * Project: SLP_Comn_JAR
 * Class CollectionUtil
 * Version 1.0
 * File Created at 2014-12-11
 * $Id$
 * 
 * Copyright 2010-2015 SmarterLogistics.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * SmarterLogistics Company. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with SmarterLogistics.com.
 */
package dcp.bss.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * collection相关工具方法
 * 
 * @author yi.yuy
 *
 */
public class CollectionUtils {

    /**
     * 判断Collection是否null或者size为0
     * @param c
     * @return
     */
    public static final boolean isEmpty(Collection<?> c) {
        return null == c || 0 == c.size()? true : false;
    }
    
    /**
     * 判断Collection是否不为null或者size不为0
     * @param c
     * @return
     */
    public static final boolean isNotEmpty(Collection<?> c) {
        return !isEmpty(c);
    }
    
    /**
     * 判断指定的一个或者多个集合，是否存在空集合
     * @param colls 参数集合， 一个或者多个
     * @return
     */
    public static final boolean isEmpty(Collection<?>... colls) {
        boolean result = false;
        for (Collection<?> coll : colls) {
            if (null == coll || coll.isEmpty()) {
                result = true;
                break;
            }
        }
        return result;
    }
    
    /**
     * 判断指定的一个或者多个集合，是否都是非空集合
     * @param colls 参数集合， 一个或者多个
     * @return
     */
    public static final boolean isNotEmpty(Collection<?>... colls) {
        return !isEmpty(colls);
    }
    
    /**
     * 获取集合的第一个对象
     * @param c
     * @return
     */
    public static final <T> T getFirstObj(Collection<T> c) {
        if (isEmpty(c)) {
            return null;
        }
        return c.iterator().next();
    }
    
    /**
     * 一个集合，拼接成一个带符号的字符串；{"1", "2"} -> ('1', '2')
     * @param c
     * @return
     */
    public static final String toSymbolString(Collection<String> c) {
        StringBuffer strCond = new StringBuffer("(");
        boolean isFirst = true;
        for (String tmpStatus: c) {
            if (isFirst) {
                strCond.append("'").append(tmpStatus).append("'");
                isFirst = false;
            } else
                strCond.append(",'").append(tmpStatus).append("'");
        }
        strCond.append(")");
        return strCond.toString();
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static List<Long> toLongList(String args) {
        if (null == args) {
            return null;
        }
        String[] strs = StringUtils.split(args, ",");
        List<Long> longList = new ArrayList<Long>();
        for (String str : strs) {
            longList.add(Long.valueOf(str));
        }
        return longList;
    }
    
    /**
     * String to list 如："1,2,3" -> ("1","2","3")
     * @param args
     * @return
     */
    public static List<String> toStringList(String args) {
        if (null == args) {
            return null;
        }
        String[] strs = StringUtils.split(args, ",");
        List<String> strList = new ArrayList<String>();
        for (String str : strs) {
            strList.add(str);
        }
        return strList;
    }
}
