package dcp.bss.common.util;


import java.io.InputStream;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonUtils {

	private static final ObjectMapper mapper = new ObjectMapper();
	
	@SuppressWarnings("rawtypes")
	public static final Class<Map> DEFAULT_TYPE = Map.class;
	
	private JsonUtils () {}
      
    @SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(InputStream ins) {
		try {
			return mapper.readValue(ins, DEFAULT_TYPE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null; 
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(String content) {
		try {
			return mapper.readValue(content, DEFAULT_TYPE);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null; 
	}
	
	public static String toString(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
}
