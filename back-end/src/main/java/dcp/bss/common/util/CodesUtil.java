package dcp.bss.common.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 
 * @author yuyi
 *
 */
public class CodesUtil {

	public static final String CODE_STR = "codeStr";
	public static final String CODE_IMG = "codeImg";
	
	public static char[] codeSequence = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
		      'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
		      'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	
	public static char[] numSequence = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	
	public static Map<String, Object> getCodes(int width, int height, int codeCount, 
			int xx, int fontHeight, int codeY) {
		// 定义图像buffer
	    BufferedImage buffImg = new BufferedImage(width, height,
	        BufferedImage.TYPE_INT_RGB);
        //Graphics2D gd = buffImg.createGraphics();
	    //Graphics2D gd = (Graphics2D) buffImg.getGraphics();
	    Graphics gd = buffImg.getGraphics();
	    // 创建一个随机数生成器类
	    Random random = new Random();
	    // 将图像填充为白色
	    gd.setColor(Color.WHITE);
	    gd.fillRect(0, 0, width, height);

	    // 创建字体，字体的大小应该根据图片的高度来定。
	    Font font = new Font("Fixedsys", Font.BOLD, fontHeight);
	    // 设置字体。
	    gd.setFont(font);

	    // 画边框。
	    gd.setColor(Color.BLACK);
	    gd.drawRect(0, 0, width - 1, height - 1);

	    // 随机产生40条干扰线，使图象中的认证码不易被其它程序探测到。
	    gd.setColor(Color.BLACK);
	    for (int i = 0; i < 10; i++) {
	      int x = random.nextInt(width);
	      int y = random.nextInt(height);
	      int xl = random.nextInt(12);
	      int yl = random.nextInt(12);
	      gd.drawLine(x, y, x + xl, y + yl);
	    }

	    // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
	    StringBuffer randomCode = new StringBuffer();
	    int red = 0, green = 0, blue = 0;

	    // 随机产生codeCount数字的验证码。
	    for (int i = 0; i < codeCount; i++) {
	      // 得到随机产生的验证码数字。
	      String code = getCode(random);
	      // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
	      red = random.nextInt(255);
	      green = random.nextInt(255);
	      blue = random.nextInt(255);

	      // 用随机产生的颜色将验证码绘制到图像中。
	      gd.setColor(new Color(red, green, blue));
	      gd.drawString(code, (i + 1) * xx, codeY);

	      // 将产生的四个随机数组合在一起。
	      randomCode.append(code);
	    }
	    Map<String, Object> map = new HashMap<String, Object>();
	    map.put(CODE_STR, randomCode.toString());
	    map.put(CODE_IMG, buffImg);
	    return map;
	}
	
	/*public static BufferedImage getCodeImg(String codeStr, int width, int height, int xx, int fontHeight, int codeY) {
		// 定义图像buffer
	    BufferedImage buffImg = new BufferedImage(width, height,
	        BufferedImage.TYPE_INT_RGB);
        //Graphics2D gd = buffImg.createGraphics();
	    //Graphics2D gd = (Graphics2D) buffImg.getGraphics();
	    Graphics gd = buffImg.getGraphics();
	    // 创建一个随机数生成器类
	    Random random = new Random();
	    // 将图像填充为白色
	    gd.setColor(Color.WHITE);
	    gd.fillRect(0, 0, width, height);

	    // 创建字体，字体的大小应该根据图片的高度来定。
	    Font font = new Font("Fixedsys", Font.BOLD, fontHeight);
	    // 设置字体。
	    gd.setFont(font);

	    // 画边框。
	    gd.setColor(Color.BLACK);
	    gd.drawRect(0, 0, width - 1, height - 1);

	    // 随机产生40条干扰线，使图象中的认证码不易被其它程序探测到。
	    gd.setColor(Color.BLACK);
	    for (int i = 0; i < 10; i++) {
	      int x = random.nextInt(width);
	      int y = random.nextInt(height);
	      int xl = random.nextInt(12);
	      int yl = random.nextInt(12);
	      gd.drawLine(x, y, x + xl, y + yl);
	    }

	    // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
	    StringBuffer randomCode = new StringBuffer();
	    int red = 0, green = 0, blue = 0;

	    // 随机产生codeCount数字的验证码。
	    for (int i = 0; i < codeStr.length(); i++) {
	      // 得到随机产生的验证码数字。
	      String code = String.valueOf(codeStr.charAt(i));
	      // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
	      red = random.nextInt(255);
	      green = random.nextInt(255);
	      blue = random.nextInt(255);

	      // 用随机产生的颜色将验证码绘制到图像中。
	      gd.setColor(new Color(red, green, blue));
	      gd.drawString(code, (i + 1) * xx, codeY);

	      // 将产生的四个随机数组合在一起。
	      randomCode.append(code);
	    }
	    return buffImg;
	}*/
	
	public static String getNumCode(int len) {
		StringBuffer randomCode = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < len; i++) {
			randomCode.append(String.valueOf(numSequence[random.nextInt(10)]));
		}
		return randomCode.toString();
	}
	
	public static String getCode(Random random) {
		return String.valueOf(codeSequence[random.nextInt(36)]);
	}
}
