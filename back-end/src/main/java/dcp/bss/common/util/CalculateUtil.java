/*
 * Class: CalculateUtil
 * Description:
 * Version: 1.0
 * Author: HaiTao
 * Creation date: 2013-8-12
 * (C) Copyright IBM Corporation 2011. All rights reserved.
 */
package dcp.bss.common.util;



/**
 * 
 * @author HaiTao 计算距离的工具类
 * 
 */
public class CalculateUtil {

	private static double EARTH_RADIUS = 6378.137,
			x_pi = Math.PI * 3000.0 / 180.0, a = 6378245.0,
			ee = 0.00669342162296594323;

	private static double transformLat(double x, double y) {
		double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y
				+ 0.2 * Math.sqrt(Math.abs(x));
		ret += (20.0 * Math.sin(6.0 * x * Math.PI) + 20.0 * Math.sin(2.0 * x
				* Math.PI)) * 2.0 / 3.0;
		ret += (20.0 * Math.sin(y * Math.PI) + 40.0 * Math.sin(y / 3.0
				* Math.PI)) * 2.0 / 3.0;
		ret += (160.0 * Math.sin(y / 12.0 * Math.PI) + 320 * Math.sin(y
				* Math.PI / 30.0)) * 2.0 / 3.0;
		return ret;
	}


	/**
	 * 根据地球上任意两点的经纬度计算两点间的距离 C = sin(MLatA)*sin(MLatB)*cos(MLonA-MLonB) +
	 * cos(MLatA)*cos(MLatB) Distance = R*Arccos(C)*Pi/180
	 *
	 * @param longt1
	 * @param lat1
	 * @param longt2
	 * @param lat2
	 * @return
	 */
	public static double getDistance(double longt1, double lat1, double longt2,
			double lat2) {
		double x, y, distance;
		x = (longt2 - longt1) * Math.PI * EARTH_RADIUS * 1000
				* Math.cos(((lat1 + lat2) / 2) * Math.PI / 180) / 180;
		y = (lat2 - lat1) * Math.PI * EARTH_RADIUS * 1000 / 180;
		distance = Math.hypot(x, y);
		return distance;
	}



	public static double Multiply(double px0, double py0, double px1,
			double py1, double px2, double py2) {
		return ((px1 - px0) * (py2 - py0) - (px2 - px0) * (py1 - py0));
	}

	public static boolean isPointOnLine(double px0, double py0, double px1,
			double py1, double px2, double py2) {
		boolean flag = false;
		double ESP = 1e-9;
		if ((Math.abs(Multiply(px0, py0, px1, py1, px2, py2)) < ESP)
				&& ((px0 - px1) * (px0 - px2) <= 0)
				&& ((py0 - py1) * (py0 - py2) <= 0)) {
			flag = true;
		}
		return flag;
	}

	public static boolean isIntersect(double px1, double py1, double px2,
			double py2, double px3, double py3, double px4, double py4) {
		boolean flag = false;
		double d = (px2 - px1) * (py4 - py3) - (py2 - py1) * (px4 - px3);
		if (d != 0) {
			double r = ((py1 - py3) * (px4 - px3) - (px1 - px3) * (py4 - py3))
					/ d;
			double s = ((py1 - py3) * (px2 - px1) - (px1 - px3) * (py2 - py1))
					/ d;
			if ((r >= 0) && (r <= 1) && (s >= 0) && (s <= 1)) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * 根据经纬度，获取两点间的距离
	 *
	 * @author caochaofeng
	 * @param lng1
	 *            经度
	 * @param lat1
	 *            纬度
	 * @param lng2
	 * @param lat2
	 * @return
	 *
	 * @date 2012-10-10
	 */
	public static double getDistatce(double lng1, double lat1, double lng2,
			double lat2) {
		double radLat1 = lat1 * Math.PI / 180;
		double radLat2 = lat2 * Math.PI / 180;
		double a = radLat1 - radLat2;
		double b = lng1 * Math.PI / 180 - lng2 * Math.PI / 180;
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * 6378137.0;// 取WGS84标准参考椭球中的地球长半径(单位:m)
		s = Math.round(s * 10000) / 10000;

		return s;
	}


	public static void main(String args[]) {

		System.out.println(getDistance(118.793759, 32.29168, 118.806657,
				32.280153));
	}
}
