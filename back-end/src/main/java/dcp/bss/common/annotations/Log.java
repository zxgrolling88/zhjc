package dcp.bss.common.annotations;

import java.lang.annotation.*;

/**
 * <p>
 * 系统日志注解
 * </p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

	/**
	 * 操作描述
	 */
	String value() default "";

}
