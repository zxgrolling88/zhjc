package dcp.bss.common.model;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yi.yuy
 *
 */
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1594988109036520738L;

    public static final String COOKIE_KEY = "userInfo";
	
	// 用户账号主键
    private String username; //登录账号
    private String nickname; //用户昵称
	private Long userId;  //系统用户主键
	
	private Long id;  //业务实例主键
	
	private List<String> roleNameList;
	
    //其他属性
    private Map<String, Object> attrMap;
	
    public void addAttr(String key, Object val) {
        if (null == attrMap) {
            attrMap = new HashMap<String, Object>();
        }
        attrMap.put(key, val);
    }
    
    public UserInfo() {}
    
	public UserInfo(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return username;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getid() {
		return id;
	}

	public void setid(Long id) {
		this.id = id;
	}

	public Map<String, Object> getAttrMap() {
		return attrMap;
	}

	public void setAttrMap(Map<String, Object> attrMap) {
		this.attrMap = attrMap;
	}

	public List<String> getRoleNameList() {
		return roleNameList;
	}

	public void setRoleNameList(List<String> roleNameList) {
		this.roleNameList = roleNameList;
	}
	
	
}
