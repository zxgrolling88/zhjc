/*
 * Class: Paging<T>
 * Description:分页封装类
 * Version: 1.0
 * Author: Vinda
 * Creation date: 2013-6-27
 * (C) Copyright 2010-2015 SmarterLogistics.com Corporation Limited.
 * All rights reserved.
 */
package dcp.bss.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * 分页查询结果
 * 
 * @author yi.yuy
 * update by Meli
 *
 */
public class Paging<T>  implements Serializable{

    public static final int DEFAULT_PAGESIZE = 20;
    public static final String DEFAULT_PAGESIZE_STR = "20";
    
    public static final int DEFAULT_SHOW_PAGE_NUM = 5;
    
    private int pageNo;
    
    private int pageSize;
    
    private int total;
    
    private List<T> result = new ArrayList<T>();
    
    private Map<String,?> summary;
    
    public Paging() {}
    
    /** 构造方法  */
    public Paging(int pageNo, int pageSize) {
        setPageNo(pageNo);
        setPageSize(pageSize);
    }
    
    public Paging(int pageNo, int pageSize, int total) {
        setPageNo(pageNo);
        setPageSize(pageSize);
        this.total = total;
    }
    
    public Paging(int pageNo, int pageSize, int total, List<T> result) {
        setPageNo(pageNo);
        setPageSize(pageSize);
        this.total = total;
        this.result = result;
    }

    
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo < 1 ? 1 : pageNo;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize < 1 ? DEFAULT_PAGESIZE : pageSize;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }
    
    
    /** 开始行，可以用于DB2分页使用  */
    public int getStartRow() {
        if(total == 0) return 0;
        if(pageNo <= 0) return 1;
        return (pageNo - 1) * pageSize + 1;
    }
        
    public int getEndRow() {
        if(pageNo <= 0) return DEFAULT_PAGESIZE;
        int endRow = pageNo * pageSize;
        return endRow <= total ? endRow : total;
    }
    /** 结束行，可以用于DB2分页使用  */
    
    /** 得到总页数  */
    public int getTotalPage() {
        int totalPage = total / pageSize;
        if ((total % pageSize) == 0) {
            return totalPage;
        }
        return totalPage + 1;
    }
    
    /** 分页展示，如：[6,7,8,9,10]  */
    public int[] getShowPageNum() {
        int totalPage = getTotalPage();
        if (totalPage <= DEFAULT_SHOW_PAGE_NUM) {
            return createShowPageNum(totalPage, 1);
        } else {
            if (pageNo <= totalPage) {
                int pageNoNum = pageNo / DEFAULT_SHOW_PAGE_NUM + 1;
                if (pageNo % DEFAULT_SHOW_PAGE_NUM == 0) {
                    pageNoNum--;
                }    
                int showPageNum = pageNoNum * DEFAULT_SHOW_PAGE_NUM;
                if (showPageNum > totalPage) {
                    showPageNum = totalPage;
                }
                return createShowPageNum(showPageNum, showPageNum - DEFAULT_SHOW_PAGE_NUM + 1);
            } else {
                return null;
            }
        }
    }
    
    private int[] createShowPageNum(int num, int beginNum) {
        int[] showPageNums = new int[num - beginNum + 1];
        for (int i = beginNum; i <= num; i++) {
            showPageNums[i - beginNum] = i;
        }
        return showPageNums;
    }
    
    /**
     * @return the pageNo
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @return the currentRowNum
     */
    public int getCurrentRowNum() {
        return result.size();
    }

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @return the result
     */
    public List<T> getResult() {
        return result;
    }
    
    /**
     * @return the summary
     */
    public Map<String, ?> getSummary() {
        return summary;
    }
    
    
    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @param summary the summary to set
     */
    public void setSummary(Map<String, ?> summary) {
        this.summary = summary;
    }
}
