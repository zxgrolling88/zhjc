package dcp.bss.common.model;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 07:38
 */
public class CmdLockBean {

    private int  lockDevID;
    private int cmd;
    private String password;
    private Integer lockType;//锁类型，1表示密码锁，2表示普通锁

    public Integer getLockType() {
        return lockType;
    }

    public void setLockType(Integer lockType) {
        this.lockType = lockType;
    }

    public int getCmd() {
        return cmd;
    }

    public void setCmd(int cmd) {
        this.cmd = cmd;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLockDevID() {
        return lockDevID;
    }

    public void setLockDevID(int lockDevID) {
        this.lockDevID = lockDevID;
    }
}
