package dcp.bss.common.model;

import lombok.Data;

/**
 * Created by Administrator on 2016/6/11.
 */
@Data
public class GpsQuery {


    private String vecNo;
    private String beginTime;
    private String endTime;


}
