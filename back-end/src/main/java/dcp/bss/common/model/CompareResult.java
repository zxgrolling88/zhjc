package dcp.bss.common.model;

import lombok.Data;

@Data
public class CompareResult {

	private String opraItm;
	private String opraBefor;
	private String opraAfter;
	
}
