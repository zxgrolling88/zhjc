package dcp.bss.common.model;

import lombok.Data;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 07:38
 */
@Data
public class LockInfoBean {

    private int  lockDevID;
    private String sendtime;
    private Integer voltage;
    private String lockStatus;
    private String  cmdSRC;//操作源
    private String  bill;//操作源
    private String cmd;



}
