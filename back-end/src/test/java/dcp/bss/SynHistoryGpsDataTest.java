//package dcp.bss;
//
//import dcp.bss.common.util.DateUtils;
//import dcp.bss.modules.bussiness.entity.GpsEntity;
//import dcp.bss.modules.bussiness.entity.HistoryGpsEntity;
//import dcp.bss.modules.bussiness.service.GpsService;
//import dcp.bss.modules.bussiness.service.HistoryGpsService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynHistoryGpsDataTest {
//    @Autowired
//    private HistoryGpsService historyGpsService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//    @Test
//    public void test() throws ParseException {
//
//        Criteria criteria = Criteria.where("updateTime")
//                .gt("2018-08-23 22:12:39").lt("2018-08-24");
//        Query query = new Query().addCriteria(criteria);
//        List<HistoryGps> historyGpsList = mongoTemplate.find(query, HistoryGps.class);
//        List<HistoryGpsEntity> gpsEntities = new ArrayList<HistoryGpsEntity>();
//        for (HistoryGps historyGps : historyGpsList) {
//            HistoryGpsEntity historyGpsEntity = new HistoryGpsEntity();
//            BeanUtils.copyProperties(historyGps,historyGpsEntity);
//            historyGpsEntity.setGpsTime(DateUtils.covertStrToDate(historyGps.getUpdateTime(),DateUtils.FULL_ST_FORMAT));
//            historyGpsEntity.setState(1);
//            historyGpsEntity.setVecNo(historyGps.getVehicleNum());
//            historyGpsEntity.setDirection( (int)historyGps.getDirection());
//            gpsEntities.add(historyGpsEntity);
//            historyGpsService.insert(historyGpsEntity);
//        }
//
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
