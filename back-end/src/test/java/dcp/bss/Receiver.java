/* 
 * Class: Customer.java  
 * 
 * Description:客户表 
 * Version: 1.0 
 * Author: GaoHuanquan
 * 
 * Creation date: 2013-7-9
 * (C) Copyright IBM Corporation 2013. All rights reserved. 
 */
package dcp.bss;


/**
 * edit by caochaofeng 客户改成送达方 2012-09-02
 * 
 * @author: GaoHuanQuan
 * @version: 1.0 Create at: 2013-7-9
 */
public class Receiver {

	/**
	 * 送达方Id
	 */
	private long receiverKey;

	/**
	 * 送达方地址
	 */
	private String receiverName;

	/**
	 * 地址
	 */
	private String address;

	public long getReceiverKey() {
		return receiverKey;
	}

	public void setReceiverKey(long receiverKey) {
		this.receiverKey = receiverKey;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
