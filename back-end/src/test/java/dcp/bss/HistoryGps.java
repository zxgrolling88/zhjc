///**
// * Project Name:TrackTrace
// * File Name:HistoryGps.java
// * Package Name:com.ibm.tracktrace.entity
// * Date:2013-12-26下午7:20:15
// * Copyright (c) 2013, IBM All Rights Reserved.
// *
// */
//package dcp.bss;
//
//import java.io.Serializable;
//
//import org.springframework.data.mongodb.core.index.Indexed;
//import org.springframework.data.mongodb.core.mapping.Document;
//
///**
// * @author caochaofeng
// *
// */
//@Document
//public class HistoryGps implements Serializable {
//	/**
//	 *
//	 */
//	private static final long serialVersionUID = -8779327395099600441L;
//
//	/**
//	 * 车辆编号
//	 *
//	 */
//	private String vehicleNum;
//
//	/**
//	 * 方向
//	 *
//	 */
//	private double direction;
//
//	/**
//	 * 速度
//	 *
//	 */
//	private double speed;
//
//	/**
//	 * 经度
//	 *
//	 */
//	private double longitude;
//
//	/**
//	 * 纬度
//	 *
//	 */
//	private double latitude;
//
//	/**
//	 * 车辆位置更新时间
//	 *
//	 */
//	@Indexed
//	private String updateTime;
//
//	public String getVehicleNum() {
//		return vehicleNum;
//	}
//
//	public void setVehicleNum(String vehicleNum) {
//		this.vehicleNum = vehicleNum;
//	}
//
//	public double getSpeed() {
//		return speed;
//	}
//
//	public void setSpeed(double speed) {
//		this.speed = speed;
//	}
//
//	public double getLongitude() {
//		return longitude;
//	}
//
//	public void setLongitude(double longitude) {
//		this.longitude = longitude;
//	}
//
//	public double getLatitude() {
//		return latitude;
//	}
//
//	public void setLatitude(double latitude) {
//		this.latitude = latitude;
//	}
//
//	public String getUpdateTime() {
//		return updateTime;
//	}
//
//	public void setUpdateTime(String updateTime) {
//		this.updateTime = updateTime;
//	}
//
//	public double getDirection() {
//		return direction;
//	}
//
//	public void setDirection(double direction) {
//		this.direction = direction;
//	}
//
//	public String toString() {
//		return "{vehicleNum:" + vehicleNum + ",speed:" + speed + ",direction:"
//				+ direction + ",longitude:" + longitude + ",latitude:"
//				+ latitude + ",updateTime:" + updateTime + "}";
//	}
//
//}
