//package dcp.bss;
//
//import dcp.bss.modules.bussiness.entity.EntpEntity;
//import dcp.bss.modules.bussiness.entity.EntpLocEntity;
//import dcp.bss.modules.bussiness.service.EntpLocService;
//import dcp.bss.modules.bussiness.service.EntpService;
//import dcp.bss.util.CatSysCode;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynCustomerAreaDataTest {
//    @Autowired
//    private EntpLocService entpService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//    @Test
//    public void test() throws ParseException {
//
//
//        Query query = new Query();
//        query.with(new Sort(Sort.Direction.ASC, "_id"));
//        List<CusElockArea> cusElockAreaList = mongoTemplate.find(query, CusElockArea.class);
//        List<EntpLocEntity> entpEntities = new ArrayList<EntpLocEntity>();
//        for (CusElockArea cusElockArea : cusElockAreaList) {
//            EntpLocEntity entpLocEntity = new EntpLocEntity();
//            entpLocEntity.setName(cusElockArea.getName());
//            entpLocEntity.setEntpPk(cusElockArea.getKey());
//            entpLocEntity.setEntpName(cusElockArea.getName());
//            entpLocEntity.setLnglat(cusElockArea.getLng()+","+cusElockArea.getLat());
//            entpLocEntity.setRadius(cusElockArea.getRadius());
//            entpLocEntity.setType(1);
//            entpLocEntity.setUpdBy("admin");
//            entpLocEntity.setCrtBy("admin");
//            entpEntities.add(entpLocEntity);
//        }
//        entpService.insertBatch(entpEntities);
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
