package dcp.bss;

public class PwdTest {
	public static void main(String[] args) throws Exception {
		String patten = "^(?![a-zA-Z]+$)(?![A-Z0-9]+$)(?![A-Z\\W_]+$)(?![a-z0-9]+$)(?![a-z\\W_]+$)(?![0-9\\W_]+$)[a-zA-Z0-9\\W_]{8,}$";

		String password1 = "fukang";      // false
		String password2 = "Fukanggggg";  // false
		String password3 = "fukang123";   // false
		String password4 = "Fukang123";   // true
		String password5 = "##Fuk%%";     // false
		String password6 = "###fukang%%"; // false
		String password7 = "66FFFFFFFFFF";// false
		String password8 = "FF%66666";// true

		System.out.println(password1.matches(patten));
		System.out.println(password2.matches(patten));
		System.out.println(password3.matches(patten));
		System.out.println(password4.matches(patten));
		System.out.println(password5.matches(patten));
		System.out.println(password6.matches(patten));
		System.out.println(password7.matches(patten));
		System.out.println(password8.matches(patten));
	}
}
