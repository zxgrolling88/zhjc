///**
// * Project Name:TrackTrace
// * File Name:CusElockArea.java
// * Package Name:com.ibm.tracktrace.entity
// * Date:2014-2-12下午3:50:23
// * Copyright (c) 2014, IBM All Rights Reserved.
// *
// */
//package dcp.bss;
//
//
///**
// * 客户地点开锁区域
// *
// * @author caochaofeng
// *
// */
//public class CusElockArea {
//	/**
//	 * 客户Id
//	 */
//	private long key;
//	/**
//	 * 客户名字
//	 */
//	private String name;
//	/**
//	 * 客户开锁圆心经度
//	 */
//	private double lng;
//	/**
//	 * 客户开锁地点圆心纬度
//	 */
//	private double lat;
//
//	/**
//	 * 开锁区域范围半径
//	 */
//	private double radius;
//
//	/**
//	 * 安全开锁区类型
//	 */
//	@Transient
//	private int category;
//
//	public long getKey() {
//		return key;
//	}
//
//	public void setKey(long key) {
//		this.key = key;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public double getLng() {
//		return lng;
//	}
//
//	public void setLng(double lng) {
//		this.lng = lng;
//	}
//
//	public double getLat() {
//		return lat;
//	}
//
//	public void setLat(double lat) {
//		this.lat = lat;
//	}
//
//	/**
//	 * @return the radius
//	 */
//	public double getRadius() {
//		return radius;
//	}
//
//	/**
//	 * @param radius
//	 *            the radius to set
//	 */
//	public void setRadius(double radius) {
//		this.radius = radius;
//	}
//
//	/**
//	 * @return the category
//	 */
//	public int getCategory() {
//		return category;
//	}
//
//	/**
//	 * @param category
//	 *            the category to set
//	 */
//	public void setCategory(int category) {
//		this.category = category;
//	}
//
//
//}
