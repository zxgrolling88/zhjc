//package dcp.bss;
//
//import dcp.bss.modules.bussiness.entity.EntpLocEntity;
//import dcp.bss.modules.bussiness.service.EntpLocService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.geo.Point;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynCustomerRoadDataTest {
//    @Autowired
//    private EntpLocService entpService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//
//    @Test
//    public void test() throws ParseException {
//
//
//        Query query = new Query();
//        query.with(new Sort(Sort.Direction.ASC, "_id"));
//        List<CusRoad> cusRoadList = mongoTemplate.find(query, CusRoad.class);
//        List<EntpLocEntity> entpEntities = new ArrayList<EntpLocEntity>();
//        for (CusRoad cusRoad : cusRoadList) {
//            EntpLocEntity entpLocEntity = new EntpLocEntity();
//            entpLocEntity.setName(cusRoad.getReceiverName());
//            entpLocEntity.setEntpPk(cusRoad.getReceiverKey());
//            entpLocEntity.setEntpName(cusRoad.getReceiverName());
//            String points ="";
//            for(Point geoJsonPoint : cusRoad.getPoints()){
//                points+=   geoJsonPoint.getX();
//                points+=",";
//                points+=  geoJsonPoint.getY();
//                points+=";";
//            }
//            points= points.substring(0,points.length()-1);
//            entpLocEntity.setLnglat(points);
//            entpLocEntity.setType(5);
//            entpLocEntity.setUpdBy("admin");
//            entpLocEntity.setCrtBy("admin");
//            entpEntities.add(entpLocEntity);
//        }
//        entpService.insertBatch(entpEntities);
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
