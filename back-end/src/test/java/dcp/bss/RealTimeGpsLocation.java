package dcp.bss;


/**
 * @author caochaofeng
 * 
 */
public class RealTimeGpsLocation {

	/**
	 * 车辆编号
	 * 
	 */
	private String vehicleNum;

	private String groupname;

	/**
	 * 速度
	 * 
	 */
	private double speed;

	/**
	 * 方向
	 * 
	 */
	private double direction;

	/**
	 * 经度
	 * 
	 */
	private double longitude;

	/**
	 * 纬度
	 * 
	 */
	private double latitude;

	/**
	 * 车辆位置更新时间
	 * 
	 */
	private String updateTime;

	/**
	 * gps设备是否在线 1:在线，0：不在线
	 */
	private String onLine;

	private String posinfo;

	/**
	 * 距离终点多少公里
	 */
	private double endDistance;
	/**
	 * 距离终点多少时间
	 */
	private String estimateTime;


	private int lock1;
	private int lock2;
	private String lock1Status;
	private String lock2Status;

	public String getPosinfo() {
		return posinfo;
	}

	public void setPosinfo(String posinfo) {
		this.posinfo = posinfo;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public int getLock1() {
		return lock1;
	}

	public void setLock1(int lock1) {
		this.lock1 = lock1;
	}

	public int getLock2() {
		return lock2;
	}

	public void setLock2(int lock2) {
		this.lock2 = lock2;
	}

	public String getLock1Status() {
		return lock1Status;
	}

	public void setLock1Status(String lock1Status) {
		this.lock1Status = lock1Status;
	}

	public String getLock2Status() {
		return lock2Status;
	}

	public void setLock2Status(String lock2Status) {
		this.lock2Status = lock2Status;
	}

	public double getEndDistance() {
		return endDistance;
	}

	public void setEndDistance(double endDistance) {
		this.endDistance = endDistance;
	}

	public String getEstimateTime() {
		return estimateTime;
	}

	public void setEstimateTime(String estimateTime) {
		this.estimateTime = estimateTime;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getDirection() {
		return direction;
	}

	public void setDirection(double direction) {
		this.direction = direction;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getVehicleNum() {
		return vehicleNum;
	}

	public void setVehicleNum(String vehicleNum) {
		this.vehicleNum = vehicleNum;
	}

	public String getOnLine() {
		return onLine;
	}

	public void setOnLine(String onLine) {
		this.onLine = onLine;
	}

}
/**
 * "GpsLocation" : { "speed" : 0.0, "direction" : 0.0, "longitude" : 0.0,
 * "latitude" : 0.0, "latitude_fix" : 0.0, "longitude_fix" : 0.0, "isStop" :
 * false }
 */
