//package dcp.bss;
//
//import com.baomidou.mybatisplus.mapper.EntityWrapper;
//import dcp.bss.common.util.DateUtils;
//import dcp.bss.modules.bussiness.entity.ShipmentEntity;
//import dcp.bss.modules.bussiness.entity.VecEntity;
//import dcp.bss.modules.bussiness.service.ShipmentService;
//import dcp.bss.modules.bussiness.service.VecService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynDataTest {
//    @Autowired
//    private ShipmentService shipmentService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//    @Test
//    public void test() throws ParseException {
//
//
//        Criteria criteria = Criteria.where("loadTime")
//                .gte("2018-08-23");
//        Query query = new Query().addCriteria(criteria);
//        query.with(new Sort(Sort.Direction.ASC, "loadTime"));
//        List<Shipment> shipments = mongoTemplate.find(query, Shipment.class);
//        List<ShipmentEntity> shipmentEntities = new ArrayList<>();
//        for(Shipment shipment:shipments){
//            ShipmentEntity shipmentEntity = new ShipmentEntity();
//            BeanUtils.copyProperties(shipment,shipmentEntity);
//            shipmentEntity.setCarrierId(shipment.getShipperKey());
//            shipmentEntity.setCarrierName(shipment.getShipper());
//            shipmentEntity.setCustomerId(shipment.getCustomerKey());
//            shipmentEntity.setCode(shipment.getDeliverOrderNo());
//            shipmentEntity.setVecNo(shipment.getVehicleNum());
//            shipmentEntity.setDriverId(shipment.getDriverKey());
//            shipmentEntity.setOriginId(shipment.getWareHouseKey());
//            shipmentEntity.setLoadTime(DateUtils.covertStrToDate(shipment.getLoadTime(),DateUtils.FULL_ST_FORMAT));
//            shipmentEntity.setUpdBy("admin");
//            shipmentEntity.setCrtBy("admin");
//            shipmentEntities.add(shipmentEntity);
//        }
//        shipmentService.insertBatch(shipmentEntities);
//
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
