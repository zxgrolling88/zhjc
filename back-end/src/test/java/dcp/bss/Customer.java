///*
// * Class: AlarmConfigParam
// * Description:
// * Version: 1.0
// * Author: caochaofeng
// * Creation date: 2013-7-19
// * (C) Copyright IBM Corporation 2011. All rights reserved.
// */
//package dcp.bss;
//
//import java.util.List;
//
//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;
//
///**
// * @author caochaofeng
// *
// *         客户实体类
// */
//@Document
//public class Customer {
//
//	/**
//	 * 客户Id
//	 */
//	@Id
//	private long customerKey;
//
//	/**
//	 * 客户名字
//	 */
//	private String customerName;
//
//	/**
//	 * 送达方信息
//	 */
//	private List<Receiver> receivers;
//
//	public String getCustomerName() {
//		return customerName;
//	}
//
//	public void setCustomerName(String customerName) {
//		this.customerName = customerName;
//	}
//
//	public long getCustomerKey() {
//		return customerKey;
//	}
//
//	public void setCustomerKey(long customerKey) {
//		this.customerKey = customerKey;
//	}
//
//	/**
//	 * @return the receivers
//	 */
//	public List<Receiver> getReceivers() {
//		return receivers;
//	}
//
//	/**
//	 * @param receivers the receivers to set
//	 */
//	public void setReceivers(List<Receiver> receivers) {
//		this.receivers = receivers;
//	}
//
//}
