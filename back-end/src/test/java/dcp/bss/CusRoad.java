///*
// * Class: CusRoad
// *
// * Description:
// * Version: 1.0
// * Author: Gaohuanquan
// *
// * Creation date: 2013-7-29
// * (C) Copyright IBM Corporation 2011. All rights reserved.
// */
//package dcp.bss;
//
//import java.util.List;
//
//import org.springframework.data.annotation.Id;
//import org.springframework.data.geo.Point;
//import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
//import org.springframework.data.mongodb.core.mapping.Document;
//
//
///**
// * @author caochaofeng
// *
// *         道路实体类
// */
//@Document
//public class CusRoad {
//
//	@Id
//	private long roadId;
//
//	private String roadName;
//	/**
//	 * 送达方名字
//	 */
//
//	private String receiverName;
//
//	/**
//	 * 送达方主键
//	 */
//
//	private long receiverKey;
//
//	/**
//	 * 路线对应的点集合
//	 */
//	private List<Point> points;
//
//	public List<Point> getPoints() {
//		return points;
//	}
//
//	public void setPoints(List<Point> points) {
//		this.points = points;
//	}
//
//	public String getReceiverName() {
//		return receiverName;
//	}
//
//	public void setReceiverName(String receiverName) {
//		this.receiverName = receiverName;
//	}
//
//	public long getReceiverKey() {
//		return receiverKey;
//	}
//
//	public void setReceiverKey(long receiverKey) {
//		this.receiverKey = receiverKey;
//	}
//
//
//
//	public long getRoadId() {
//		return roadId;
//	}
//
//	public void setRoadId(long roadId) {
//		this.roadId = roadId;
//	}
//
//	public String getRoadName() {
//		return roadName;
//	}
//
//	public void setRoadName(String roadName) {
//		this.roadName = roadName;
//	}
//}
