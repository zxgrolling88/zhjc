/*
 * Class: Shipment
 * Description:
 * Version: 1.0
 * Author: Administrator
 * Creation date: 2013-8-23
 * (C) Copyright IBM Corporation 2011. All rights reserved.
 */
package dcp.bss;

import java.io.Serializable;


/**
 * 运单实体类
 * 
 * @author caochaofeng
 * 
 */
public class Shipment implements Serializable {
	/**
	 * 运单状态更新时间
	 */
	private String updateTime;

	private static final long serialVersionUID = 1L;

	private double loadCapacity;

	private String supercargoKey;

	private String businessScope;

	private String supercargoName;

	private String tankNo;

	private String supercargoPhone;

	private String goodsName;

	private String manufacturer;

	/**
	 * 车队
	 */
	private String fleetName;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 容量
	 */
	private Double volumn;

	/**
	 * 车牌号
	 */
	private String vehicleNum;

	/**
	 * 装车时间
	 */
	private String loadTime;

	/**
	 * 重量
	 */
	private Double weight;

	/**
	 * 承运商
	 */
	private String shipper;

	/**
	 * 承运商主键
	 */
	private Long shipperKey;

	/**
	 * 出发地
	 */
	private String origin;

	/**
	 * 目的地
	 */
	private String destination;

	/**
	 * 客户姓名
	 */
	private String customerName;

	/**
	 * 客户id
	 */
	private Long customerKey;

	/**
	 * 送达方名字
	 */
	private String receiverName;

	/**
	 * 送货地址
	 */
	private String shippingAddress;

	/**
	 * 运单状态
	 */
	private String status;

	/**
	 * 送达方id
	 */
	private Long receiveKey;

	/**
	 * 销售订单主键
	 */
	private Long salesOrderKey;

	private Long wareHouseKey;

	private String wareHouseName;

	/**
	 * 货物描述
	 */
	private String goodsDesc;

	/**
	 * 提货单号
	 */
	private String deliverOrderNo;

	/**
	 * 驾驶员姓名
	 */
	private String driverName;

	/**
	 * 驾驶员slf数据库对应主键
	 */
	private Long driverKey;

	/**
	 * 销售订单号
	 */
	private String sid;

	private boolean isArrivalReminder;

	/**
	 * 判断运单是否到达了终点，用于写入到达时间
	 */
	private boolean isArrival;

	/**
	 * 是否非法解锁过
	 */
	private boolean isAbnormalUnlock;

	public boolean isArrivalReminder() {
		return isArrivalReminder;
	}

	public void setArrivalReminder(boolean isArrivalReminder) {
		this.isArrivalReminder = isArrivalReminder;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getFleetName() {
		return fleetName;
	}

	public void setFleetName(String fleetName) {
		this.fleetName = fleetName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Double getVolumn() {
		return volumn;
	}

	public void setVolumn(Double volumn) {
		this.volumn = volumn;
	}

	public String getVehicleNum() {
		return vehicleNum;
	}

	public void setVehicleNum(String vehicleNum) {
		this.vehicleNum = vehicleNum;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public Long getShipperKey() {
		return shipperKey;
	}

	public void setShipperKey(Long shipperKey) {
		this.shipperKey = shipperKey;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getCustomerKey() {
		return customerKey;
	}

	public void setCustomerKey(Long customerKey) {
		this.customerKey = customerKey;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getReceiveKey() {
		return receiveKey;
	}

	public void setReceiveKey(Long receiveKey) {
		this.receiveKey = receiveKey;
	}

	public Long getSalesOrderKey() {
		return salesOrderKey;
	}

	public void setSalesOrderKey(Long salesOrderKey) {
		this.salesOrderKey = salesOrderKey;
	}

	public String getGoodsDesc() {
		return goodsDesc;
	}

	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	public String getDeliverOrderNo() {
		return deliverOrderNo;
	}

	public void setDeliverOrderNo(String deliverOrderNo) {
		this.deliverOrderNo = deliverOrderNo;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public Long getDriverKey() {
		return driverKey;
	}

	public void setDriverKey(Long driverKey) {
		this.driverKey = driverKey;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLoadTime() {
		return loadTime;
	}

	public void setLoadTime(String loadTime) {
		this.loadTime = loadTime;
	}

	/**
	 * @return the wareHouseKey
	 */
	public Long getWareHouseKey() {
		return wareHouseKey;
	}

	/**
	 * @param wareHouseKey
	 *            the wareHouseKey to set
	 */
	public void setWareHouseKey(Long wareHouseKey) {
		this.wareHouseKey = wareHouseKey;
	}

	/**
	 * @return the wareHouseName
	 */
	public String getWareHouseName() {
		return wareHouseName;
	}

	/**
	 * @param wareHouseName
	 *            the wareHouseName to set
	 */
	public void setWareHouseName(String wareHouseName) {
		this.wareHouseName = wareHouseName;
	}

	public String getSupercargoPhone() {
		return supercargoPhone;
	}

	public void setSupercargoPhone(String supercargoPhone) {
		this.supercargoPhone = supercargoPhone;
	}

	public String getSupercargoKey() {
		return supercargoKey;
	}

	public void setSupercargoKey(String supercargoKey) {
		this.supercargoKey = supercargoKey;
	}

	public double getLoadCapacity() {
		return loadCapacity;
	}

	public void setLoadCapacity(double loadCapacity) {
		this.loadCapacity = loadCapacity;
	}

	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	public String getSupercargoName() {
		return supercargoName;
	}

	public void setSupercargoName(String supercargoName) {
		this.supercargoName = supercargoName;
	}

	public String getTankNo() {
		return tankNo;
	}

	public void setTankNo(String tankNo) {
		this.tankNo = tankNo;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isArrival() {
		return isArrival;
	}

	public void setArrival(boolean isArrival) {
		this.isArrival = isArrival;
	}

	public boolean isAbnormalUnlock() {
		return isAbnormalUnlock;
	}

	public void setAbnormalUnlock(boolean isAbnormalUnlock) {
		this.isAbnormalUnlock = isAbnormalUnlock;
	}

}

/**
 * { "vehicle":{ "vehicleNum":"鲁Q-56789", "volumn":35, "weight":35,
 * "fleetName":"海安销售公司", "driver":"张三", "vehicleKey":3, "mobile":"11987066" },
 * "shippOrder":{ "shipper":"海安销售公司", "loadTime":1376668800000,
 * "origin":"海曙装车站", "quantity":35 }, "salesOrder":{ "sid":"XSEE-00001111",
 * "item_desc":"化学药品危险物", "customer":".华能国际电力股份有限公司", "destination":"宁波xx街道xx号"
 * }
 */
