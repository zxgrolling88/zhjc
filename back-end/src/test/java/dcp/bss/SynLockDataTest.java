//package dcp.bss;//package dcp.bss;
//
//import dcp.bss.common.util.DateUtils;
//import dcp.bss.modules.bussiness.entity.EntpLocEntity;
//import dcp.bss.modules.bussiness.entity.GpsEntity;
//import dcp.bss.modules.bussiness.entity.LockStatusEntity;
//import dcp.bss.modules.bussiness.service.EntpLocService;
//import dcp.bss.modules.bussiness.service.GpsService;
//import dcp.bss.modules.bussiness.service.LockStatusService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynLockDataTest {
//    @Autowired
//    private LockStatusService lockStatusService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//    @Test
//    public void test() throws ParseException {
//
//
//        Query query = new Query();
//        List<RealTimeGpsLocation> realTimeGpsLocations = mongoTemplate.find(query, RealTimeGpsLocation.class);
//        List<LockStatusEntity> lockStatusEntities = new ArrayList<LockStatusEntity>();
//        for (RealTimeGpsLocation realTimeGpsLocation : realTimeGpsLocations) {
//            LockStatusEntity lockStatusEntity = new LockStatusEntity();
//            lockStatusEntity.setLockCode(String.valueOf(realTimeGpsLocation.getLock1()));
//            lockStatusEntity.setVecNo(realTimeGpsLocation.getVehicleNum());
//            lockStatusEntity.setLockContent(realTimeGpsLocation.getLock1Status());
//            if(realTimeGpsLocation.getLock1Status().contains("解封")){
//                lockStatusEntity.setLockStatus("解封");
//            }
//            if(realTimeGpsLocation.getLock1Status().contains("锁定")){
//                lockStatusEntity.setLockStatus("锁定");
//            }
//            lockStatusEntities.add(lockStatusEntity);
//
//
//            LockStatusEntity lockStatusEntity1 = new LockStatusEntity();
//            lockStatusEntity1.setLockCode(String.valueOf(realTimeGpsLocation.getLock2()));
//            lockStatusEntity1.setVecNo(realTimeGpsLocation.getVehicleNum());
//            lockStatusEntity1.setLockContent(realTimeGpsLocation.getLock2Status());
//            if(realTimeGpsLocation.getLock2Status().contains("解封")){
//                lockStatusEntity1.setLockStatus("解封");
//            }
//            if(realTimeGpsLocation.getLock2Status().contains("锁定")){
//                lockStatusEntity1.setLockStatus("锁定");
//            }
//            lockStatusEntities.add(lockStatusEntity1);
//
//
//        }
//        lockStatusService.insertBatch(lockStatusEntities);
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
