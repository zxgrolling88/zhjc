package dcp.bss;//package dcp.bss;


import com.alibaba.fastjson.JSONObject;
import dcp.bss.common.model.CmdLockBean;
import dcp.bss.modules.bussiness.entity.VecEntity;
import dcp.bss.modules.bussiness.service.VecService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;

/**
 * 类的功能描述.
 * 自动生成代码测试类
 *
 * @Auther hxy
 * @Date 2017/4/25
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ZtjkApplication.class)
public class VecDataTest {
    @Autowired
    private VecService vecService;


    @Before
    public void before() {
    }

    @Test
    public void test() throws ParseException {

        VecEntity vecEntity = new VecEntity();
        vecEntity.setVecNo("12444");
        vecService.insert(vecEntity);
        System.out.println("执行完毕");

    }


}

