//package dcp.bss;
//
//import dcp.bss.common.util.DateUtils;
//import dcp.bss.modules.bussiness.entity.EntpEntity;
//import dcp.bss.modules.bussiness.entity.ShipmentEntity;
//import dcp.bss.modules.bussiness.service.EntpService;
//import dcp.bss.modules.bussiness.service.ShipmentService;
//import dcp.bss.util.CatSysCode;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynCustomerDataTest {
//    @Autowired
//    private EntpService entpService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//    @Test
//    public void test() throws ParseException {
//
//
//        Query query = new Query();
//        query.with(new Sort(Sort.Direction.ASC, "_id"));
//        List<Customer> customers = mongoTemplate.find(query, Customer.class);
//        List<EntpEntity> entpEntities = new ArrayList<EntpEntity>();
//        for (Customer customer : customers) {
//            EntpEntity entpEntity = new EntpEntity();
//            entpEntity.setId(customer.getCustomerKey());
//            entpEntity.setEntpName(customer.getCustomerName());
//            entpEntity.setCatCd(CatSysCode.IpConsignorConsigneeWithWarehouse.cd());
//            entpEntity.setCatNmCn(CatSysCode.IpConsignorConsigneeWithWarehouse.nmCn());
//            if (customer.getReceivers().size() > 0)
//                entpEntity.setLocation(customer.getReceivers().get(0).getAddress());
//            entpEntity.setUpdBy("admin");
//            entpEntity.setCrtBy("admin");
//            entpEntities.add(entpEntity);
//        }
//        entpService.insertBatch(entpEntities);
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
