//package dcp.bss;
//
//import dcp.bss.common.util.DateUtils;
//import dcp.bss.modules.bussiness.entity.EntpLocEntity;
//import dcp.bss.modules.bussiness.entity.GpsEntity;
//import dcp.bss.modules.bussiness.service.EntpLocService;
//import dcp.bss.modules.bussiness.service.GpsService;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 类的功能描述.
// * 自动生成代码测试类
// *
// * @Auther hxy
// * @Date 2017/4/25
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = ZtjkApplication.class)
//public class SynGpsDataTest {
//    @Autowired
//    private GpsService gpsService;
//
//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    @Before
//    public void before() {
//    }
//
//    @Test
//    public void test() throws ParseException {
//
//
//        Query query = new Query();
//        List<RealTimeGpsLocation> realTimeGpsLocations = mongoTemplate.find(query, RealTimeGpsLocation.class);
//        List<GpsEntity> gpsEntities = new ArrayList<GpsEntity>();
//        for (RealTimeGpsLocation realTimeGpsLocation : realTimeGpsLocations) {
//            GpsEntity gpsEntity = new GpsEntity();
//            BeanUtils.copyProperties(realTimeGpsLocation,gpsEntity);
//            gpsEntity.setGpsTime(DateUtils.covertStrToDate(realTimeGpsLocation.getUpdateTime(),DateUtils.FULL_ST_FORMAT));
//            gpsEntity.setState(Integer.parseInt(realTimeGpsLocation.getOnLine()));
//            gpsEntity.setVecNo(realTimeGpsLocation.getVehicleNum());
//            gpsEntity.setDirection( (int)realTimeGpsLocation.getDirection());
//            gpsEntities.add(gpsEntity);
//        }
//        gpsService.insertBatch(gpsEntities);
//
//        System.out.println("执行完毕");
//
//    }
//
//
//}
//
