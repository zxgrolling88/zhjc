package dcp.bss;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


import javax.xml.bind.annotation.XmlElement;


public class CreateTable {


    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        createTable(Shipment.class, null);
// createTable(Book.class, null);
    }

    public static void createTable(Class obj, String tableName) throws IOException {


        Field[] fields = null;
        fields = obj.getDeclaredFields();
        Class annotationType = null;
        Object param = null;
        String column = null;
        XmlElement xmlElement = null;
        StringBuilder sb = null;
        sb = new StringBuilder(50);
        if (tableName == null || tableName.equals("")) {
//未传表明默认用类名
            tableName = obj.getName();
            tableName = tableName.substring(tableName.lastIndexOf(".") + 1);
        }
        sb.append("\r\ndrop table if exists  ").append(tableName).append(";\r\n");
        sb.append("create table ").append(tableName).append(" ( \r\n");
        System.out.println(tableName);
        boolean firstId = true;
        File file = null;
        for (Field f : fields) {
            column = f.getName();
            sb.append(column).append(" ");
            System.out.println(column + "," + f.getType());
            param = f.getType();
            sb.append(column);//一般第一个是主键
            if (param instanceof Integer) {
                sb.append(" INTEGER ");
            } else {
                sb.append(" VARCHAR(30) ");//根据需要自行修改
            }
            if (firstId) {//类型转换
                sb.append(" PRIMARY KEY ");
                firstId = false;
            }
            sb.append(",\n ");
        }
        String sql = null;
        sql = sb.toString();
        sql = sb.substring(0, sql.length() - 1) + " )ENGINE =INNODB DEFAULT  CHARSET= utf8;\r\n";
        System.out.println(sql);

    }

}
