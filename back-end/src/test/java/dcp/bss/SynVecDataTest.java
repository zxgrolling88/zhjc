package dcp.bss;//package dcp.bss;


import com.alibaba.fastjson.JSONObject;
import dcp.bss.common.model.CmdLockBean;
import dcp.bss.modules.bussiness.service.VecService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 类的功能描述.
 * 自动生成代码测试类
 *
 * @Auther hxy
 * @Date 2017/4/25
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ZtjkApplication.class)
public class SynVecDataTest {
    @Autowired
    private StringRedisTemplate template;


    @Before
    public void before() {
    }

    @Test
    public void test() throws ParseException {

        CmdLockBean cmdLockBean  =new CmdLockBean();
        //38是解封 32 是施封
        cmdLockBean.setCmd(38);
        cmdLockBean.setLockDevID(74000349);
        cmdLockBean.setLockType(1);
        cmdLockBean.setPassword("090005");
        template.convertAndSend("oprLock", JSONObject.toJSONString(cmdLockBean));
        System.out.println("执行完毕");

    }


}

