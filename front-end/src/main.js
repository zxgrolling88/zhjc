// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Router from 'vue-router'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import App from './App'
import router from '@/router'
import store from '@/store'

import * as filters from './filters' // global filters
import hasPermission from '@/directive/hasPermission'
import elFormRules from '@/plugins/elFormRules'

import request from '@/utils/request'
import '@/icons'

import 'normalize.css/normalize.css'
import 'animate.css/animate.css'
import '@/styles/app.default.css'
import '@/common/css/index.less'

import echarts from 'echarts'
import "echarts/lib/chart/map.js";
import "echarts/map/js/china.js";

Vue.use(Element)
// global directive register
Vue.use(hasPermission)
Vue.config.productionTip = false
Vue.use(elFormRules)

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 全局注册svg图标组件
require('@/common/lib/iconfont.js')
import svgIcon from '@/components/svg-icon.vue'
Vue.component('svg-icon', svgIcon)

// 挂载全局
Vue.prototype.$echarts = echarts

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
