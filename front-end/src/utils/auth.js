import Cookies from 'js-cookie'

const TokenKey = 'ZTJK-TOKEN'

export function getToken(){
	return Cookies.get(TokenKey);
}

export function setToken(token){
	return Cookies.set(TokenKey,token,{ expires: 2});
}

export function removeToken(){
	return Cookies.remove(TokenKey);
}
