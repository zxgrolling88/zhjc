// 企业证照信息配置
const entp = {
    // "企业营业执照": "8010.200"
    "8010.200":{
        "licType":"企业相关证照",
        "licCatCd":"8010.200",
        "licNm":"企业营业执照",
        "shortName":"营",                // 简称
        "heading":"企业营业执照",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见<br />如果三证合一企业，此处上传统一社会信用代码证",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.200.150":{
                "rsrcCd":"8010.200.150",
                "licNm":"企业营业执照",
                "exampleUrl":"static/img/lic/8010_200_150.jpg",
                "zjsb":true,
                "zjsbTypeId":"entp",                // 证件类型Id
                "zjsbLicType":"entp"                // 证件类别：企业
            }
        },
        "header":[
            // {'name':'统一社会信用代码或注册号', 'field':'licCd','type':'input','required':true,'class':'long-desc'},
            {'name':'统一社会信用代码', 'field':'licCd','type':'input','required':true,'validateType':'uscCd'},
            {'name':'有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"140px"
    },

    // "企业组织机构代码证":"8010.201"
    "8010.201":{
        "licType":"企业相关证照",
        "licCatCd":"8010.201",
        "licNm":"企业组织机构代码证",
        "shortName":"组",                // 简称
        "heading":"企业组织机构代码证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见<br />如果三证合一企业，此处上传统一社会信用代码证",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.201.150":{
                "rsrcCd":"8010.201.150",
                "licNm":"企业组织机构代码证",
                "exampleUrl":"static/img/lic/8010_201_150.jpg",
            }
        },
        "header":[
            {'name':'组织机构代码证号', 'field':'licCd','type':'input','required':true},
            {'name':'有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"140px"
    },

    // "企业税务登记证":"8010.202"
    "8010.202":{
        "licType":"企业相关证照",
        "licCatCd":"8010.202",
        "licNm":"企业税务登记证",
        "shortName":"税",                // 简称
        "heading":"企业税务登记证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见<br />如果三证合一企业，此处上传统一社会信用代码证",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.202.150":{
                "rsrcCd":"8010.202.150",
                "licNm":"企业税务登记证",
                "exampleUrl":"static/img/lic/8010_202_150.jpg",
            }
        },
        "header":[
            {'name':'税号', 'field':'licCd','type':'input','required':true},
            {'name':'有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"100px"
    },

    // "企业道路运输经营许可证":"8010.203"
    "8010.203":{
        "licType":"企业相关证照",
        "licCatCd":"8010.203",
        "licNm":"企业道路运输经营许可证",
        "shortName":"道",                // 简称
        "heading":"企业道路运输经营许可证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.203.150":{
                "rsrcCd":"8010.203.150",
                "licNm":"企业道路运输经营许可证",
                "exampleUrl":"static/img/lic/8010_203_150.jpg",
            }
        },
        "header":[
            {'name':'道路运输经营许可证号', 'field':'licCd','type':'input','required':true,'validateType':'opraLicNo'},
            {'name':'有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"170px"
    },

    // "企业安全责任承诺书":"8010.204"
    "8010.204":{
        "licType":"企业相关证照",
        "licCatCd":"8010.204",
        "licNm":"企业安全责任承诺书",
        "shortName":"安",                // 简称
        "heading":"企业安全责任承诺书",
        "title":"要求：下载->填写->盖公章->彩色件扫描上传<br /><a href='http://doc.dacyun.com/download/71/' target='_blank'>点击此处下载 道路危险货物运输企业安全承诺书.pdf</a>",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.204.150":{
                "rsrcCd":"8010.204.150",
                "licNm":"企业安全责任承诺书",
                "exampleUrl":"static/img/lic/8010_204_150.jpg",
            }
        },
        "header":[],
        "headerLabelWidth":"100px"
    }
}

// 人员证照信息配置
const pers = {
    // "身份证"："8010.400"
    "8010.400":{
        "licType":"人员相关证照",
        "licCatCd":"8010.400",
        "licNm":"身份证",
        "shortName":"身",                // 简称
        "heading":"身份证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":false,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.400.150":{
                "rsrcCd":"8010.400.150",
                "licNm":"身份证正面",
                "exampleUrl":"static/img/lic/8010_400_150.jpg"
            },
            "8010.400.151":{
                "rsrcCd":"8010.400.151",
                "licNm":"身份证反面",
                "exampleUrl":"static/img/lic/8010_400_151.jpg",
                "zjsb":true,
                "zjsbTypeId":3,             // 证件类型Id
                "zjsbLicType":"pers"        // 证件类别：人员
            }
        },
        "header":[
            {'name':'性别', 'field':'persSex','type':'select','options':[{'value':'M','label':'男'},{'value':'F','label':'女'}],'required':true,'readonly':true},
            {'name':'身份证号码', 'field':'persIdCard','type':'input','required':true,'validateType':'ID','readonly':true},
            {'name':'手机号码', 'field':'mob','type':'input','required':true,'validateType':'mobile','readonly':true},
            {'name':'身份证有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"120px"
    },

    // "劳动合同":"8010.401"
    "8010.401":{
        "licType":"人员相关证照",
        "licCatCd":"8010.401",
        "licNm":"劳动合同",
        "shortName":"劳",                // 简称
        "heading":"劳动合同",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.401.150":{
                "rsrcCd":"8010.401.150",
                "licNm":"劳动合同封面，含甲方乙方名称",
                "exampleUrl":"static/img/lic/8010_401_150.jpg",
            },
            "8010.401.151":{
                "rsrcCd":"8010.401.151",
                "licNm":"劳动合同内页，含劳动合约时间页",
                "exampleUrl":"static/img/lic/8010_401_151.jpg",
            },
            "8010.401.152":{
                "rsrcCd":"8010.401.152",
                "licNm":"劳动合同内页，含甲方乙方签字/敲章，含日期页",
                "exampleUrl":"static/img/lic/8010_401_152.jpg",
            }
        },
        "header":[
            {'name':'劳动合同有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"125px"
    },

    // "驾驶证（驾驶员岗位必须）":"8010.402"
    "8010.402":{
        "licType":"人员相关证照",
        "licCatCd":"8010.402",
        "licNm":"驾驶证",
        "shortName":"驾",                // 简称
        "heading":"驾驶证（驾驶员岗位必须）",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":false,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.402.150":{
                "rsrcCd":"8010.402.150",
                "licNm":"驾驶证正本正面",
                "exampleUrl":"static/img/lic/8010_402_150.jpg",
                "zjsb":true,
                "zjsbTypeId":5,             // 证件类型Id
                "zjsbLicType":"pers"        // 证件类别：人员
            },
            "8010.402.151":{
                "rsrcCd":"8010.402.151",
                "licNm":"驾驶证正本反面",
                "exampleUrl":"static/img/lic/8010_402_151.jpg"
            },
            "8010.402.152":{
                "rsrcCd":"8010.402.152",
                "licNm":"驾驶证副本正面",
                "exampleUrl":"static/img/lic/8010_402_152.jpg"
            },
            "8010.402.153":{
                "rsrcCd":"8010.402.153",
                "licNm":"驾驶证副本反面",
                "exampleUrl":"static/img/lic/8010_402_153.jpg"
            }
        },
        "header":[
            {'name':'驾驶证号', 'field':'licCd','type':'input','required':true,'readonly':true},
            {'name':'审验有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"100px"
    },

    // "驾驶员从业资格证（驾驶员必须）":"8010.403"
    "8010.403":{
        "licType":"人员相关证照",
        "licCatCd":"8010.403",
        "licNm":"驾驶员从业资格证",
        "shortName":"驾",                // 简称
        "heading":"驾驶员从业资格证（驾驶员必须）",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见，含从业资格证基本信息页",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.403.150":{
                "rsrcCd":"8010.403.150",
                "licNm":"驾驶员从业资格证",
                "exampleUrl":"static/img/lic/8010_403_150.jpg",
            }
        },
        "header":[
            {'name':'从业资格证号', 'field':'licCd','type':'input','required':true},
            {'name':'有效期截止日期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"125px"
    },

    // "押运员从业资格证（押运员必须）":"8010.404"
    "8010.404":{
        "licType":"人员相关证照",
        "licCatCd":"8010.404",
        "licNm":"押运员从业资格证",
        "shortName":"押",                // 简称
        "heading":"押运员从业资格证（押运员必须）",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见，含从业资格证基本信息页",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.404.150":{
                "rsrcCd":"8010.404.150",
                "licNm":"押运员从业资格证",
                "exampleUrl":"static/img/lic/8010_404_150.jpg",
            }
        },
        "header":[
            {'name':'从业资格证号', 'field':'licCd','type':'input','required':true},
            {'name':'有效期截止日期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"125px"
    },

    // "安全责任状":"8010.405"
    "8010.405":{
        "licType":"人员相关证照",
        "licCatCd":"8010.405",
        "licNm":"安全责任状",
        "shortName":"安",                // 简称
        "heading":"安全责任状",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.405.150":{
                "rsrcCd":"8010.405.150",
                "licNm":"含劳甲方乙方名称页",
                "exampleUrl":"static/img/lic/8010_405_150.jpg",
            },
            "8010.405.151":{
                "rsrcCd":"8010.405.151",
                "licNm":"含甲方乙方签字/敲章，含日期页",
                "exampleUrl":"static/img/lic/8010_405_151.jpg",
            }
        },
        "header":[
            {'name':'有效期截止日期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"125px"
    }
}

// 车辆证照信息配置
const vec = {
    // "车辆道路运输证":"8010.300"
    "8010.300":{
        "licType":"车辆相关证照",
        "licCatCd":"8010.300",
        "licNm":"车辆道路运输证",
        "shortName":"运",                // 简称
        "heading":"车辆道路运输证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.300.150":{
                "rsrcCd":"8010.300.150",
                "licNm":"营运证正本正本正面",
                "exampleUrl":"static/img/lic/8010_300_150.jpg",
            },
            "8010.300.151":{
                "rsrcCd":"8010.300.151",
                "licNm":"待理证正面",
                "exampleUrl":"static/img/lic/8010_300_151.jpg",
            },
            "8010.300.152":{
                "rsrcCd":"8010.300.152",
                "licNm":"待理证反面",
                "exampleUrl":"static/img/lic/8010_300_152.jpg",
            }
            // ,
            // "8010.300.153":{
            //     "rsrcCd":"8010.300.153",
            //     "licNm":"等级评定卡(挂车不需要)",
            //     "exampleUrl":"static/img/lic/8010_300_153.jpg",
            // }
        },
        "header":[
            {'name':'道路运输证号', 'field':'licCd','type':'input','required':true,'validateType':'opraLicNo','readonly':true},
            {'name':'年审有效期', 'field':'licVldTo','type':'date','required':true},
            // {'name':'等级评定有效期', 'field':'licVldDate','type':'date','required':true},
            {'name':'经营类型', 'field':'bizScope','type':'tree','treeOptions':[],'treeValue':'id','treeLabel':'label','treeKey':'id','required':true,'readonly':true}
        ],
        "headerLabelWidth":"125px"
    },

    // "机动车登记证":"8010.301"
    "8010.301":{
        "licType":"车辆相关证照",
        "licCatCd":"8010.301",
        "licNm":"机动车登记证",
        "shortName":"机",                // 简称
        "heading":"机动车登记证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":false,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.301.150":{
                "rsrcCd":"8010.301.150",
                "licNm":"机动车登记证信息栏",
                "exampleUrl":"static/img/lic/8010_301_150.jpg",
            },
            "8010.301.151":{
                "rsrcCd":"8010.301.151",
                "licNm":"机动车登记证登记栏(最近一次登记记录页)",
                "exampleUrl":"static/img/lic/8010_301_151.jpg",
            }
        },
        "header":[],
        "headerLabelWidth":"100px"
    },

    // "车辆行驶证":"8010.302"
    "8010.302":{
        "licType":"车辆相关证照",
        "licCatCd":"8010.302",
        "licNm":"车辆行驶证",
        "shortName":"行",                // 简称
        "heading":"车辆行驶证",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":false,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.302.150":{
                "rsrcCd":"8010.302.150",
                "licNm":"行驶证正本正面",
                "exampleUrl":"static/img/lic/8010_302_150.jpg"
            },
            "8010.302.151":{
                "rsrcCd":"8010.302.151",
                "licNm":"行驶证正本反面",
                "exampleUrl":"static/img/lic/8010_302_151.jpg"
            },
            "8010.302.152":{
                "rsrcCd":"8010.302.152",
                "licNm":"行驶证副本正面",
                "exampleUrl":"static/img/lic/8010_302_152.jpg"
            },
            "8010.302.153":{
                "rsrcCd":"8010.302.153",
                "licNm":"行驶证副本反面",
                "exampleUrl":"static/img/lic/8010_302_153.jpg"
            }
        },
        "header":[
            {'name':'车辆类型', 'field':'catNmCn','type':'select','options':[
                {value: "", label: "----牵引车----", disabled: true},
                {value: "重型半挂牵引车", label: "重型半挂牵引车"},
                {value: "中型半挂牵引车", label: "中型半挂牵引车"},
                {value: "轻型半挂牵引车", label: "轻型半挂牵引车"},
                {value: "重型罐式货车", label: "重型罐式货车"},
                {value: "中型罐式货车", label: "中型罐式货车"},
                {value: "仓栅式货车", label: "仓栅式货车"},
                {value: "普通货车", label: "普通货车"},
                {value: "厢式货车", label: "厢式货车"},
                {value: "中型厢式货车", label: "中型厢式货车"},
                {value: "栏板车", label: "栏板车"},
                {value: "", label: "----半挂车----", disabled: true},
                {value: "重型普通半挂车", label: "重型普通半挂车"},
                {value: "重型厢式半挂车", label: "重型厢式半挂车"},
                {value: "重型罐式半挂车", label: "重型罐式半挂车"},
                {value: "重型平板半挂车", label: "重型平板半挂车"},
                {value: "重型集装箱半挂车", label: "重型集装箱半挂车"},
                {value: "重型自卸半挂车", label: "重型自卸半挂车"},
                {value: "重型特殊结构半挂车", label: "重型特殊结构半挂车"},
                {value: "重型仓栅式半挂车", label: "重型仓栅式半挂车"},
                {value: "中型普通半挂车", label: "中型普通半挂车"},
                {value: "中型厢式半挂车", label: "中型厢式半挂车"},
                {value: "中型罐式半挂车", label: "中型罐式半挂车"},
                {value: "中型平板半挂车", label: "中型平板半挂车"},
                {value: "中型集装箱半挂车", label: "中型集装箱半挂车"},
                {value: "中型自卸半挂车", label: "中型自卸半挂车"},
                {value: "中型特殊结构半挂车", label: "中型特殊结构半挂车"},
                {value: "轻型普通半挂车", label: "轻型普通半挂车"},
                {value: "轻型厢式半挂车", label: "轻型厢式半挂车"},
                {value: "轻型罐式半挂车", label: "轻型罐式半挂车"},
                {value: "轻型集装箱半挂车", label: "轻型集装箱半挂车"},
                {value: "轻型平板半挂车", label: "轻型平板半挂车"},
                {value: "轻型自卸半挂车", label: "轻型自卸半挂车"},
                {value: "", label: "----全挂车----", disabled: true},
                {value: "重型普通全挂车", label: "重型普通全挂车"},
                {value: "重型厢式全挂车", label: "重型厢式全挂车"},
                {value: "重型罐式全挂车", label: "重型罐式全挂车"},
                {value: "重型平板全挂车", label: "重型平板全挂车"},
                {value: "重型集装箱全挂车", label: "重型集装箱全挂车"},
                {value: "重型自卸全挂车", label: "重型自卸全挂车"},
                {value: "重型特殊结构全挂车", label: "重型特殊结构全挂车"},
                {value: "中型普通全挂车", label: "中型普通全挂车"},
                {value: "中型厢式全挂车", label: "中型厢式全挂车"},
                {value: "中型罐式全挂车", label: "中型罐式全挂车"},
                {value: "中型平板全挂车", label: "中型平板全挂车"},
                {value: "中型集装箱全挂车", label: "中型集装箱全挂车"},
                {value: "中型自卸全挂车", label: "中型自卸全挂车"},
                {value: "中型特殊结构全挂车", label: "中型特殊结构全挂车"},
                {value: "轻型普通全挂车", label: "轻型普通全挂车"},
                {value: "轻型厢式全挂车", label: "轻型厢式全挂车"},
                {value: "轻型罐式全挂车", label: "轻型罐式全挂车"},
                {value: "轻型平板全挂车", label: "轻型平板全挂车"},
                {value: "轻型自卸全挂车", label: "轻型自卸全挂车"},
                {value: "其他", label: "其他"}
            ],'required':true,'readonly':true},
            {'name':'车牌类型', 'field':'plateType','type':'select','options':[{'value':'黄牌','label':'黄牌'},{'value':'蓝牌','label':'蓝牌'}],'required':true,'readonly':true},
            {'name':'整备质量(KG)', 'field':'selfWeight','type':'number','required':true,'readonly':true},
            {'name':'核载/准牵引质量(KG)', 'field':'apprvWeight','type':'number','required':true,'readonly':true},
            {'name':'检验有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"160px"
    },

    // "卫星定位终端安装证书":"8010.303"
    "8010.303":{
        "licType":"车辆相关证照",
        "licCatCd":"8010.303",
        "licNm":"卫星定位终端安装证书",
        "shortName":"卫",                // 简称
        "heading":"卫星定位终端安装证书",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.303.150":{
                "rsrcCd":"8010.303.150",
                "licNm":"卫星定位终端安装证书",
                "exampleUrl":"static/img/lic/8010_303_150.jpg",
            }
        },
        "header":[],
        "headerLabelWidth":"100px"
    },

    // "道路危险货物承运人责任保险单":"8010.304"
    "8010.304":{
        "licType":"车辆相关证照",
        "licCatCd":"8010.304",
        "licNm":"道路危险货物承运人责任保险单",
        "shortName":"险",                // 简称
        "heading":"道路危险货物承运人责任保险单",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.304.150":{
                "rsrcCd":"8010.304.150",
                "licNm":"道路危险货物承运人责任保险单",
                "exampleUrl":"static/img/lic/8010_304_150.jpg",
            },
            "8010.304.151":{
                "rsrcCd":"8010.304.151",
                "licNm":"道路危险货物承运人责任保险单(附加页)",
                "exampleUrl":"static/img/lic/8010_304_150.jpg",
            }
        },
        "header":[
            {'name':'保险有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"100px"
    },

    // "车辆安全设备配备照片":"8010.305"
    "8010.305":{
        "licType":"车辆相关证照",
        "licCatCd":"8010.305",
        "licNm":"车辆安全设备配备照片",
        "shortName":"安",                // 简称
        "heading":"车辆安全设备配备照片(牵引车必填，至少一张)",
        "title":"要求：彩色拍摄照片",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.305.150":{
                "rsrcCd":"8010.305.150",
                "licNm":"车辆安全设备配备照片",
                "exampleUrl":"static/img/lic/8010_305_150.jpg",
            },
            "8010.305.151":{
                "rsrcCd":"8010.305.151",
                "licNm":"车辆安全设备配备照片",
                "exampleUrl":"static/img/lic/8010_305_150.jpg",
            },
            "8010.305.152":{
                "rsrcCd":"8010.305.152",
                "licNm":"车辆安全设备配备照片",
                "exampleUrl":"static/img/lic/8010_305_150.jpg",
            },
            "8010.305.153":{
                "rsrcCd":"8010.305.153",
                "licNm":"车辆安全设备配备照片",
                "exampleUrl":"static/img/lic/8010_305_150.jpg",
            }
        },
        "header":[],
        "headerLabelWidth":"100px"
    }
}

// 罐体证照信息配置
const tank = {
    // "罐体检验报告":"8010.500"
    "8010.500":{
        "licType":"罐体相关证照",
        "licCatCd":"8010.500",
        "licNm":"罐体检验报告",
        "shortName":"检",                // 简称
        "heading":"罐体检验报告",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":false,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.500.150":{
                "rsrcCd":"8010.500.150",
                "licNm":"检验报告封面页",
                "exampleUrl":"static/img/lic/8010_500_150.jpg",
            },
            "8010.500.151":{
                "rsrcCd":"8010.500.151",
                "licNm":"检验报告细节页",
                "exampleUrl":"static/img/lic/8010_500_151.jpg",
            }
        },
        "header":[
            {'name':'报告编号', 'field':'licCd','type':'input','required':true},
            {'name':'下次检验日期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"120px"
    },

    // "压力罐容器登记证（压力罐容器必须）":"8010.501"
    "8010.501":{
        "licType":"罐体相关证照",
        "licCatCd":"8010.501",
        "licNm":"压力罐容器登记证",
        "shortName":"登",                // 简称
        "heading":"压力罐容器登记证（压力罐容器必须）",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":false,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.501.150":{
                "rsrcCd":"8010.501.150",
                "licNm":"管理页",
                "exampleUrl":"static/img/lic/8010_501_150.jpg",
            },
            "8010.501.151":{
                "rsrcCd":"8010.501.151",
                "licNm":"检验页",
                "exampleUrl":"static/img/lic/8010_501_151.jpg",
            },
            "8010.501.152":{
                "rsrcCd":"8010.501.152",
                "licNm":"特性页",
                "exampleUrl":"static/img/lic/8010_501_152.jpg",
            }
        },
        "header":[
            {'name':'使用登记证编号', 'field':'licCd','type':'input','required':true},
            {'name':'下次检验日期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"130px"
    }
}

// 企业危化品证照信息配置
const ench = {
    // "道路运输危险货物安全卡":"8010.600"
    "8010.600":{
        "licType":"企业危化品相关证照",
        "licCatCd":"8010.600",
        "licNm":"道路运输危险货物安全卡",
        "shortName":"卡",                // 简称
        "heading":"道路运输危险货物安全卡",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.600.150":{
                "rsrcCd":"8010.600.150",
                "licNm":"道路运输危险货物安全卡",
                "exampleUrl":"static/img/lic/8010_600_150.jpg",
            }
        },
        "header":[],
        "headerLabelWidth":"100px"
    }
}

// 通行证证照信息配置
const passport ={
    // "运输合同":"8010.701"
    "8010.701":{
        "licType":"通行证相关证照",
        "licCatCd":"8010.701",
        "licNm":"运输合同",
        "shortName":"运",                // 简称
        "heading":"运输合同",
        "title":"要求：彩色扫描件或彩色照片，内容清晰可见",
        "aprvAprvOfGongguan":true,      // 公路管理证照审核权限 approval authority of Gongguan
        "list":{
            "8010.701.150":{
                "rsrcCd":"8010.701.150",
                "licNm":"运输合同",
                "exampleUrl":"static/img/lic/8010_701_150.jpg"
            },
            "8010.701.151":{
                "rsrcCd":"8010.701.151",
                "licNm":"运输合同",
                "exampleUrl":"static/img/lic/8010_701_150.jpg"
            },
            "8010.701.152":{
                "rsrcCd":"8010.701.152",
                "licNm":"运输合同",
                "exampleUrl":"static/img/lic/8010_701_150.jpg"
            },
            "8010.701.153":{
                "rsrcCd":"8010.701.153",
                "licNm":"运输合同",
                "exampleUrl":"static/img/lic/8010_701_150.jpg"
            }
        },
        "header":[
            {'name':'有效期', 'field':'licVldTo','type':'date','required':true}
        ],
        "headerLabelWidth":"100px"
    }
}

export default {
    entp,
    pers,
    vec,
    tank,
    ench,
    passport
}

