import request from '@/utils/request'

/**
 *  json: 'application/json; charset=utf-8'
 *  form: 'application/x-www-form-urlencoded; charset=utf-8'
 */

// 获取验证码
export function getCodeKeySrc(codeKey){
  return request({
    url:`/captcha?uuid=${codeKey}`,
    responseType: 'arraybuffer',
    transformResponse: [function (response) {
      let res = 'data:image/png;base64,' + btoa(
        new Uint8Array(response)
          .reduce((data, byte) => data + String.fromCharCode(byte), '')
    )
      return res;
    }],
  })
}

// 用户名登录
export function loginByUsername(data){
	return request({
		url:'/sys/login',
		method:'post',
		data: data
	})
}

// 登出
export function logout() {
  return request({
    url: '/sys/logout',
    method: 'post'
  })
}

// 获取当前管理员信息
export function getUserInfo() {
  return request({
    url: '/sys/user/info',
    method: 'get'
  })
}

// 企业端用户通过手机号注册，获取手机验证码
export function getSmsCode(data){
	return request({
		url:'/code/getSmsCode',
		method:'post',
		params: data,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded'
		}
	})
}

// 验证手机号验证码
export function handleCheckMobileCode(data){
	return request({
		url:'/code/checkCode',
		method:'post',
		params: data,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded'
		}
	})
}

// 提交企业注册信息
export function handleRegister(data){
	return request({
		url:'/entp/register',
		method:'post',
		params: data,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded'
		}
	})
}
