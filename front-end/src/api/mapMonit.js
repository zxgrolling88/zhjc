import request from '@/utils/request'

// 获取所有车辆 无分页 用于地图描点
export  function getAllCar() {
  return request({
    url: `/gps/list`,
    method: 'post'
  })
}

// 获取车辆列表
// 传参方式: post方式传参用 data: {...data} ，get方式传参用 params: {...params}，一般是Form Data方式请求
// 修改post请求参数类型如下：
// data: qs.stringify({...data}) // Form Data方式请求，Content-Type:application/x-www-form-urlencoded
// data: {...data}  // Request Payload方式请求，Content-Type:applicatio/json（axios默认的传值方式）
export  function getCarList(param) {
  return request({
    url: `/lockstatus/page`,
    method: 'get',
    params:param,
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
})
}

// 获取指定车辆实时监控信息
export  function getMonitorCarInfo(vecNo) {
  return request({
    url: `/gps/lastest?vecNo=`+vecNo,
    method: 'get'
})
}
// 施封
export function lock (lockNum) {
  return request({
    url: '/lockstatus/lock?lockNum='+lockNum,
    method: 'post'
  })
}
// 解锁
export function openLock (lockNum) {
  return request({
    url: '/lockstatus/unlock?lockNum='+lockNum,
    method: 'post'
  })
}


// 获取指定车辆围栏信息
export  function getCarFenceInfo(vecNo) {
  return request({
    url: `/entploc/fence?vecNo=`+vecNo,
    method: 'get'
  })
}
// 获取轨迹
export  function getTrack(data) {
  return request({
    url: '/track/list',
    method: 'post',
    data: data
})
}

// 在途可视左侧车辆列表接口
export function getGpsVecListByPage(param){
	return request({
		url:'/gps/page',
		method:'get',
		params:param,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8'
		}
	})
}

// 历史轨迹左侧车辆列表接口
export function getVecListOfGpsTraceByPage(param){
	return request({
		url:'/gps/queryCarList',
		method:'get',
		params:param,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8'
		}
	})
}

// 在途可视实时定位接口
export function getAllVecListWithGPS(type){
	return request({
		url:'/gps/list',
		method:'get',
		params:{type:type},
		headers: {
			'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8'
		}
	})
}

// 获取车辆电子锁
export  function queryDeviceByVecNo(vecNo) {
  return request({
    url: `/device/queryByVecNo?vecNo=`+vecNo,
    method: 'get'
  })
}
// 根据日期，获取车辆gsp的历史轨迹
export function getVecGpsTraceByDate(data){
	return request({
		url:'/gps/today',
		method:'get',
		params:{
			t:data.t,
			v:data.v
		},
		headers: {
			'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8'
		}
	})
}
