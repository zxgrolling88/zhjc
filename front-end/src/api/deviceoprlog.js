import request from '@/utils/request'

// 获取列表
export function list (params) {
  return request({
    url: '/deviceoprlog/page',
    method: 'get',
    params: params,
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
  })
}


// 获取信息
export function info (id) {
  return request({
    url: '/deviceoprlog/info/' + id,
    method: 'get'
  })
}

// 添加
export function add (params) {
  return request({
    url: '/deviceoprlog/save',
    method: 'post',
    data: params
  })
}

// 修改
export function update (params) {
  return request({
    url: '/deviceoprlog/update',
    method: 'post',
    data: params
  })
}

// 删除
export function del (params) {
  return request({
    url: '/deviceoprlog/delete',
    method: 'post',
    data: params
  })
}
