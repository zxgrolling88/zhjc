import request from '@/utils/request'

export function getMenuListOld(){
	return request({
		url:'/sys/menu/nav',
		method:'get'
	})
}

export function getMenuList(){
	return request({
		url:'/sys/menu/all',
		method:'get'
	})
}
