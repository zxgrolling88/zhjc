import request from '@/utils/request'

// 获取首页基础数据
export  function getBasicData() {
  return request({
    url: `/home/basic`,
    method:'get',
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
})
}
// 运输量趋势情况
export function getCargoAmountTrend(){
  return request({
    url:'/home/getCargoAmountTrend',
    method:'get',
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
  })
}

// 报警分类情况
export function getAlarmType(){
  return request({
    url:'/home/alarmTypeAmount',
    method:'get',
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
  })
}
