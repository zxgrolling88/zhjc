import request from '@/utils/request'

// let baseUrl = process.env.BASE_API

// 传参方式: post方式传参用 data: {...data} ，get方式传参用 params: {...params}，一般是Form Data方式请求
// 修改post请求参数类型如下：
// data: qs.stringify({...data}) // Form Data方式请求，Content-Type:application/x-www-form-urlencoded
// data: {...data}  // Request Payload方式请求，Content-Type:application/json（axios默认的传值方式）
// 创建围栏
export  function createFence (data) {
  return request({
    url: `/entploc/save`,
    method: 'post',
    data: data
  })
}

// 获取围栏列表
export  function getFence (param) {
  return request({
    url: `/entploc/page`,
    method: 'get',
    param: param
  })
}

// 加载某个车辆所有绑定的围栏
export  function getFenceByCarNumber (data) {
  return request({
    url: `/fence/find_by_vecNo_or_fence_ids`,
    method: 'post',
    data: data
  })
}

// 解绑指定围栏的车辆
export  function unBindCar(data){
  return request({
    url: '/fence/delete/entity_name',
    method: 'post',
    data: data
  })
}

// 加载某个围栏所有绑定的车辆
export  function getCarNumberByFence (data) {
  return request({
    url: `/fence/get_vehicles_by_fenceid`,
    method: 'post',
    data: data
  })
}

// 删除围栏
export  function deleteFence (data) {
  return request({
    url: `/entploc/delete`,
    method: 'post',
    data: data
  })
}

// 围栏绑定车辆
export  function bindCar (data) {
  return request({
    url: `/fence/add_vecNo`,
    method: 'post',
    data: data
  })
}

// 获取报警信息
export  function getWarning(data){
  return request({
    url: `/fault_alarm/list`,
    method: 'post',
    data: data
  })
}

// 获取远程控制车辆的门窗状态
export  function getCarStatus(data){
  return request({
    url: `/car/get_state`,
    method: 'post',
    data: data
  })
}
