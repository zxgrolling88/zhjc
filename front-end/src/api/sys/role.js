import request from '@/utils/request'

// 列表
export function getRoleList(param){
	return request({
		url:'/sys/role/list',
		method:'get',
		params:param
	})
}

export function deleteRole(data){
	return request({
		url:'/sys/role/delete',
		method:'post',
		data:data
	})
}

export function getRoleInfo(id){
	return request({
		url:'/sys/role/info/'+id,
		method:'get'
	})
}

export function getSelectRole(){
	return request({
		url:'/sys/role/select',
		method:'get'
	})
}

export function addRole(data){
	return request({
		url:'/sys/role/save',
		method:'post',
		data: data,
		headers: {
			'Content-type': 'application/json;charset=UTF-8'
		}
	})
}

export function updRole(data){
	return request({
		url:'/sys/role/update',
		method:'post',
		data: data,
		headers: {
			'Content-type': 'application/json;charset=UTF-8'
		}
	})
}