import request from '@/utils/request'

// 用户列表
export function getUserList(param){
	return request({
		url:'/sys/user/list',
		method:'get',
		params:param
	})
}

export function deleteUser(data){
	return request({
		url:'/sys/user/delete',
		method:'post',
		data:data
	})
}

export function getUserInfo(id){
	return request({
		url:'/sys/user/info/'+id,
		method:'get'
	})
}

export function addUser(data){
	return request({
		url:'/sys/user/save',
		method:'post',
		data: data,
		headers: {
			'Content-type': 'application/json;charset=UTF-8'
		}
	})
}

export function updUser(data){
	return request({
		url:'/sys/user/update',
		method:'post',
		data: data,
		headers: {
			'Content-type': 'application/json;charset=UTF-8'
		}
	})
}