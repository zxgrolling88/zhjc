import request from '@/utils/request'

// 获取列表
export function list (params) {
  return request({
    url: '/vec/page',
    method: 'get',
    params: params,
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
  })
}

// 获取列表
export function select (params) {
  return request({
    url: '/vec/select',
    method: 'get',
    params: params,
    headers: {
      'Content-type': 'application/json;charset=UTF-8'
    }
  })
}
// 获取信息
export function info (id) {
  return request({
    url: '/vec/info/' + id,
    method: 'get'
  })
}

// 添加
export function add (params) {
  return request({
    url: '/vec/save',
    method: 'post',
    data: params
  })
}

// 修改
export function update (params) {
  return request({
    url: '/vec/update',
    method: 'post',
    data: params
  })
}

// 删除
export function del (params) {
  return request({
    url: '/vec/delete',
    method: 'post',
    data: params
  })
}
