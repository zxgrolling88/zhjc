import request from '@/utils/request'

// 获取列表
export function list (params) {
  return request({
    url: '/entp/page',
    method: 'get',
    params: params
  })
}
// 获取列表
export function select (params) {
  return request({
    url: '/entp/select',
    method: 'get',
    params: params
  })
}
// 获取信息
export function info (id) {
  return request({
    url: '/entp/info/' + id,
    method: 'get'
  })
}

// 添加
export function add (params) {
  return request({
    url: '/entp/save',
    method: 'post',
    data: params
  })
}

// 修改
export function update (params) {
  return request({
    url: '/entp/update',
    method: 'post',
    data: params
  })
}

// 删除
export function del (params) {
  return request({
    url: '/entp/delete',
    method: 'post',
    data: params
  })
}
