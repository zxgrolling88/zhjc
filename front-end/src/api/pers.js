import request from '@/utils/request'
// 获取列表
export function list (params) {
  return request({
    url: '/pers/list',
    method: 'get',
    params: params
  })
}

// 获取信息
export function info (id) {
  return request({
    url: '/pers/info/'+id,
    method: 'get'
  })
}

// 添加
export function add (params) {
  return request({
    url: '/pers/save',
    method: 'post',
    data: params
  })
}

// 修改
export function update (params) {
  return request({
    url: '/pers/update',
    method: 'post',
    data:params
  })
}

// 删除
export function del (params) {
  return request({
    url: '/pers/delete',
    method: 'post',
    data: params
  })
}
