import request from '@/utils/request'

// 根据手机号，获取手机验证码
export function getMobCode(data){
	return request({
		url:'/code/getSmsCodeOnlyMob',
		method:'post',
		params: {mob:data},
		headers: {
			'Content-type': 'application/x-www-form-urlencoded'
		}
	})
}

// 验证手机号验证码
export function checkMobileCode(data){
	return request({
		url:'/code/checkCode',
		method:'post',
		params: data,
		headers: {
			'Content-type': 'application/x-www-form-urlencoded'
		}
	})
}

// 修改密码
export function modifyPwd(data){
	return request({
		url:'/sys/user/password',
		method:'post',
		data: data
	})
}