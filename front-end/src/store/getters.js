
export default {
	token: state => state.user.token,
	username: state => state.user.username,
	isFirstLogin: state => state.user.isFirstLogin,
	sidebar: state => state.app.sidebar,
	menuList: state => state.app.menuList,
	permissions: state => state.user.permissions,
	addRouters: state => state.permission.addRouters,
}