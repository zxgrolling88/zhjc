import Cookies from 'js-cookie'
import { getMenuList } from '@/api/menu'

const app = {
  state: {
    sidebar: {
      	opened: !+Cookies.get('sidebarStatus')
    },
    menuList: []
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
		if (state.sidebar.opened) {
			Cookies.set('sidebarStatus', 1)
		} else {
			Cookies.set('sidebarStatus', 0)
		}
		state.sidebar.opened = !state.sidebar.opened
    },
    SET_MENULIST: (state, menuList) => {
      	state.menuList = menuList
    }
  },
  actions: {
    toggleSideBar({ commit }) {
      	commit('TOGGLE_SIDEBAR')
    },
    
    getMenuList({ dispatch,commit }) {
      return new Promise((resolve,reject) => {
        getMenuList().then(response => {
			if (response && response.code == 0) { // 菜单获取成功
				commit('SET_MENULIST',response.menuList);
				dispatch('setPermissions',response.permissions);
			}
			resolve(response);
        }).catch(error => {
          	reject(error)
        })
      })
    }
  }
}

export default app