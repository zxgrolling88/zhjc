
const maps = {
  state: {
    map:null,//地图实例
    roads:[],
    mapPlugins:{
      driving:null,
      myDrawingManagerObject:null,
      bdary:null
    }
  },
  mutations: {
    SET_MAP:(state,map) => {
      state.map = map;
    },
    SET_MAP_DRIVING:(state,driving) => {
      state.mapPlugins.driving = driving;
    },
    SET_MAP_DRAWING:(state,drawing) => {
      state.mapPlugins.myDrawingManagerObject = drawing;
    },
    SET_MAP_BDARY:(state,bdary) => {
      state.mapPlugins.bdary = bdary;
    },
    UPDATE_ROADS: (state,roads) =>{
      state.roads = roads;
    }
  },
  actions: {

  }
}

export default maps