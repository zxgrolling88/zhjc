package com.app.conf;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.management.modelmbean.XMLParseException;

import org.dom4j.DocumentException;

import soft.common.StaticClass;
import soft.common.XmlReadUtil;

/**
 * app配置
 * 
 * @author fanpei
 *
 */
public class AppConfig extends StaticClass {
	private static final String APPCONFIGNAME = "/appconfig.xml";
	/**
	 * 命令包位置
	 */
	public static String CMDPACKGENAME;

	/**
	 * 读取app配置文件
	 * 
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws DocumentException
	 * @throws XMLParseException
	 */
	public static void readConf()
			throws FileNotFoundException, UnsupportedEncodingException, DocumentException, XMLParseException {
		XmlReadUtil xmlreader = null;
		try {
			InputStream inputStream = AppConfig.class.getResourceAsStream(APPCONFIGNAME);
			xmlreader = new XmlReadUtil(inputStream);
			CMDPACKGENAME = xmlreader.getElementValue(xmlreader.getRootEle(), "cmdPackge");
		} finally {
			if (xmlreader != null)
				xmlreader.close();
		}

	}
}
