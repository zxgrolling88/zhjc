package com.app.ifs;

/**
 * 命令接收处理
 * 
 * @author fanpei
 * @date 2018-09-16 23:17
 *
 */
public interface ICmdRecvHandle {

	/**
	 * 解析数据
	 */
	void parse() throws Exception;

	/**
	 * 执行
	 */
	void excute() throws Exception;
}
