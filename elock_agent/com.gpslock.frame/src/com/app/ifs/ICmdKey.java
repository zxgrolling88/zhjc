package com.app.ifs;

/**
 * 命令标识接口
 * 
 * @author fanpei
 * @date 2018-09-18 22:54
 *
 */
public interface ICmdKey {
	/**
	 * 获取命令常量
	 * 
	 * @return
	 */
	String getCmdKey();
}
