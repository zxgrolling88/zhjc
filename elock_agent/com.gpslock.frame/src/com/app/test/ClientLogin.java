package com.app.test;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.commond.sys.CmdSend;
import com.app.protocol.Direction;
import soft.common.BytesHexStrTranslate;
import soft.common.StringUtil;

/**
 * 心跳信息
 * 
 * @author fanpei
 * @date 2018-09-19 22:25
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX07, funcCode = CmdFuncCode.OX07_Login)
public class ClientLogin extends CmdSend {

	public ClientLogin() {
		this.direc = Direction.UP;
	}

	@Override
	public byte[] buildBytes() {
		String str = "FEDD2769040301001FEA0D8196080000000800000000C0008A1D0200008000005520504B";
		byte[] sendDatas =    BytesHexStrTranslate.toBytes(str);
		return buildBytes(sendDatas);
	}

}
