package com.app.protocol;

/**
 * 协议数据方向
 * @author fanpei
 *
 */
public enum Direction {
	/**
	 * 上行数据
	 */
	UP,

	/**
	 * 下行数据
	 */
	DOWN;
}
