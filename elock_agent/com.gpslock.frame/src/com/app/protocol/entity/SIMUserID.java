package com.app.protocol.entity;

import com.app.exception.SIMUserIDErrorException;

import soft.common.StringUtil;
import soft.ifs.IBytesBuild;
import soft.ifs.IParse;

/**
 * SIM 用户信息
 * 
 * @author fanpei
 * @date 2018-09-20 22:04
 *
 */
public class SIMUserID implements IParse, IBytesBuild {
	/**
	 * ICCID CHINA用户前五位
	 */
	protected static final byte[] CHINASIMFLAG = new byte[] { 8, 9, 8, 6, 0 };
	/**
	 * IMSI用户信息长度
	 */
	public static final int IMSILEN = 15;
	/**
	 * ICCID 国别信息长度
	 */
	public static final int ICCIDSIMFLAGLEN = 5;
	/**
	 * ICCID用户信息长度
	 */
	public static final int ICCIDLEN = 20;

	public static final int ICCIDREALLEN = ICCIDLEN - ICCIDSIMFLAGLEN;

	/**
	 * SIM ID类型
	 */
	private MobleIDType mobleIDType;
	/**
	 * IMSI 共有 15 位，一个典型的 IMSI 号码为 460030912121001
	 */
	private byte[] IMSI;
	/**
	 * ICCID 一共 20 位，一个典型的 ICCID 号码为 898602B0101730418921 ,国内 SIM 卡前 5 位都是
	 * 89860后续版本的锁 没有特别要求都是传输 ICCID 后 15 位
	 */
	private byte[] ICCID;// 后 15 位

	/**
	 * 获取用户SIM用户信息长度
	 * 
	 * @return
	 */
	public int getSIMIDLen() {
		if (MobleIDType.IMSI == mobleIDType)
			return IMSILEN;
		else
			return ICCIDLEN - ICCIDSIMFLAGLEN;
	}

	/**
	 * 获取用户SIM用户信息
	 * 
	 * @return
	 */
	public byte[] getSIMIDs() {
		if (MobleIDType.IMSI == mobleIDType)
			return IMSI;
		else
			return ICCID;
	}

	public SIMUserID() {
		this.mobleIDType = MobleIDType.ICCID;
		this.ICCID = new byte[ICCIDREALLEN];
	}

	/**
	 * 用户信息构造函数
	 * 
	 * @param mobleIDType
	 *            SIM ID类型
	 * @param ids
	 *            用户信息
	 * @throws SIMUserIDErrorException
	 */
	public SIMUserID(MobleIDType mobleIDType, byte[] ids) throws SIMUserIDErrorException {
		this.mobleIDType = mobleIDType;
		this.ICCID = ids;

	}

	@Override
	public void parse() throws Exception {
		if (MobleIDType.IMSI == mobleIDType) {
			if (ICCID == null || IMSILEN != ICCID.length) {
				throw new SIMUserIDErrorException("SIM UerID IMSI is null or IMSI's len is not correct!");
			}
			IMSI = ICCID;
		} else {
			if (ICCID == null || ICCIDREALLEN != ICCID.length) {
				throw new SIMUserIDErrorException("SIM UerID ICCID is null or ICCID's len is not correct!");
			}
			// ICCID = ICCID; not need
		}
		clear();
	}

	@Override
	public byte[] buildBytes() {
		return getSIMIDs();
	}

	@Override
	public String toString() {
		String simidStr = null;
		byte[] datas = getSIMIDs();
		if (datas != null) {
			simidStr = StringUtil.bytes2HexStr(datas);
		}

		return simidStr;
	}

	/**
	 * SIM ID类型
	 * 
	 * @author fanpei
	 * @date 2018-09-20 22:19
	 *
	 */
	public enum MobleIDType {
		IMSI, ICCID;
	}

}
