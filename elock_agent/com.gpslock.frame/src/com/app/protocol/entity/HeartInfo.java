package com.app.protocol.entity;

import soft.common.StringUtil;
import soft.ifs.IBytesBuild;
import soft.ifs.IParse;
import soft.net.exception.DataIsNullException;

/**
 * 版本信息
 * 
 * @author fanpei
 *
 */
public class HeartInfo implements IParse, IBytesBuild {
	private String version = "";// 版本号
	private int heartInterval;// 心跳间隔

	public String getVersion() {
		return version;
	}

	public int getHeartInterval() {
		return heartInterval;
	}

	public HeartInfo(String version, int heartInterval) {
		this.version = version;
		this.heartInterval = heartInterval;
	}

	public HeartInfo(int heartInterval, byte[] datas) {
		this.heartInterval = heartInterval;
		this.datas = datas;
	}

	private byte[] datas;

	@Override
	public void parse() throws Exception {
		if (datas == null)
			throw new DataIsNullException("HeartInfo parse:datas is null!");
		version = StringUtil.getUtf8Str(datas);
		datas = null;
	}

	@Override
	public byte[] buildBytes() {
		return StringUtil.getUtf8Bytes(version);
	}

	@Override
	public String toString() {
		return version;
	}

}
