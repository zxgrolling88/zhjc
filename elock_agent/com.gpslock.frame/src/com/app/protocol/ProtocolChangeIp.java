package com.app.protocol;

import soft.common.BConvrtUtil;
import soft.common.BytesHexStrTranslate;
import soft.common.StringUtil;
import soft.ifs.IByteBuff;
import soft.ifs.IBytesBuild;
import soft.ifs.IParse;
import soft.net.exception.DataIsNullException;
import soft.net.exception.DecodeDataFailException;
import soft.net.model.AReadNetStream;
import soft.net.model.CusByteStream;

/**
 * 锁操作指令
 * <p>
 * 地址 功能代码 子功能数据量 数据长度 具体更改IP的数据内容 CRC
 0x31 0x01 子功能数据量 数据长度 IP,TCP端口号,UDP端口号，APN CR

 例如： 更改IP为 59.42.129.28 TCP 端口:2008 UDP 端口:2009 APN:CMNET
 310100353035392E3034322E3132392E3032382C323030382C323030392C434D4E455400000
 00000000000000000000000000000$$
 其中:
 IP: 3035392E3034322E3132392E303238 = 059.042.129.028
 0x2C:分隔符
 TCP_PORT:32303038 = 2008
 0x2C:分隔符
 UDP_PORT:32303039 = 2009
 0x2C:分隔符
 APN:434D4E45540000000000000000000000000000000000
 = CMNET,其他0x00 用于填充
 $$:请用CRC校验
 *
 * @author fanpei
 */

/**
 * @author fanpei
 */
public class ProtocolChangeIp extends AReadNetStream implements IParse, IBytesBuild {

    public final static int DOWN_SIZE = 48;


    /**
     * 固定15 个字节的ASCII字符串，第一个字节如果为0 则表示不需要更改IP 地
     址,之后字节用0x00填满IP 地址位:
     如：59.42.129.28就是：059.042.129.028
     *
     * @Size 15 个字节
     */
    private byte[] ip;

    /**
     * TCP 端口号：固定4 个字节的ASCII字符串，第一个字节如果为0 则表示不需要更改
     TCP 端口号，之后字节用0x00填满TCP 端口号位置：
     如：2008
     *
     * @Size 4 个字节
     */
    private byte[] tcpPort;


    /**
     * UDP 端口号：固定4 个字节的ASCII字符串，第一个字节如果为0x00 则表示不需要
     更改UDP 端口号，之后字节用0x00填满UDP 端口号位置
     *
     * @Size 4 个字节
     */
    private byte[] udpPort= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    /**
     * 固定22 个字节 第一个字节如果为0 则表示不需要更改APN, 之后字节用0x00
     填满APN 位置
     *
     * @Size 22 个字节
     */
    private byte[] apn = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00};

  
    /**

     * default constructor
     */
    public ProtocolChangeIp() {
        objinit();
    }

    /**
     * parse constructor
     *
     * @param srcBytes 原始bytes
     */
    public ProtocolChangeIp(byte[] srcBytes) {
        objinit();
    }


    /**
     * 对象初始化
     */
    private void objinit() {
        ip = new byte[15];
        tcpPort = new byte[4];
    }

    /**
     * 数据校验
     */
    public void validate() {


    }

    @Override
    public void parse() throws DecodeDataFailException, DataIsNullException {

    }

    @Override
    public void clear() {
    }


    @Override
    public byte[] buildBytes() throws Exception {
        byte[] sends = new byte[DOWN_SIZE];
        System.arraycopy(ip, 0, sends, 0, 15);
        sends[15] = (byte) 0x2C;
        System.arraycopy(tcpPort, 0, sends, 16, 4);
        sends[20] = (byte) 0x2C;
        System.arraycopy(udpPort, 0, sends, 21, 4);
        sends[25] = (byte) 0x2C;
        System.arraycopy(apn, 0, sends, 26, 22);
        return sends;
    }
    public static void main(String[] args) throws Exception {
        String ip = "112.74.131.151";
        String tcpPort = "1502";
        byte[] udpPort= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        byte[] apn = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00};
        String newIP = "";
        String[] ips = ip.split("\\.");
        for(String item:ips){
            if(item.length()==1){
                newIP+=	("00"+item);
            }
            if(item.length()==2){
                newIP+=	("0"+item);

            }
            if(item.length()==3){
                newIP+=	item;
            }
            newIP+=	".";
        }
        newIP= newIP.substring(0,newIP.length()-1);
        byte[] sends = new byte[DOWN_SIZE];
        System.arraycopy(newIP.getBytes(), 0, sends, 0, 15);
        sends[15] =(byte) 0x2C;
        System.arraycopy(tcpPort.getBytes(), 0, sends, 16, 4);
        sends[20] = (byte) 0x2C;
        System.arraycopy(udpPort, 0, sends, 21, 4);
        sends[25] = (byte) 0x2C;
        System.arraycopy(apn, 0, sends, 26, 22);


        System.out.println("ip：" + BytesHexStrTranslate.bytesToHexFun1(sends));

    }


    public byte[] getIp() {
        return ip;
    }

    public void setIp(byte[] ip) {
        this.ip = ip;
    }

    public byte[] getTcpPort() {
        return tcpPort;
    }

    public void setTcpPort(byte[] tcpPort) {
        this.tcpPort = tcpPort;
    }



}
