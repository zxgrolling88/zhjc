package com.app.protocol;

import soft.common.StaticClass;
import soft.common.StringUtil;

/**
 * 命令key助手
 * 
 * @author fanpei
 * @date 2018-09-18 22:57
 *
 */
public class CmdKeyUtil extends StaticClass {
	/**
	 * 组装命令标识
	 * 
	 * @param devAddr
	 * @param funcCode
	 * 
	 * @return
	 */
	public static String getCmdKey(byte devAddr, byte funcCode) {
		return StringUtil.getMsgStr("{}-{}", devAddr, funcCode);
	}
}
