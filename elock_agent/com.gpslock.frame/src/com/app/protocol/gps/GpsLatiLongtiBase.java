package com.app.protocol.gps;

import soft.common.StringUtil;

/**
 * 经纬度基类
 * 
 * @author fanpei
 * @date 2018-09-24 15:44
 *
 */
public abstract class GpsLatiLongtiBase {
	protected int degree;// 度数
	protected float fraction;// 分数

	public int getDegree() {
		return degree;
	}

	public float getFraction() {
		return fraction;
	}

	@Override
	public String toString() {
		return StringUtil.getMsgStr("{}度 {}分", degree, fraction);
	}

}
