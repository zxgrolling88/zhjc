package com.app.protocol.gps;

public class BitStatusFlag {

	public final static byte FIRST_STATE = 1 << 0; // 第一个状态0000 0001
	public final static byte SECOND_STATE = 1 << 1; // 第二个状态0000 0010
	public final static byte THIRD_STATE = 1 << 2;// 第三个状态 0000 0100
	public final static byte FOURTH_STATE = 1 << 3;// 第四个状态 0000 1000
	public final static byte FIFTH_STATE = 1 << 4;// 第五个状态 0001 0000
	public final static byte SIXTH_STATE = 1 << 5;// 第六个状态 0010 0000
	public final static byte SEVENTH_STATE = 1 << 6;// 第七个状态 0100 0000
	public final static byte EIGHTH_STATE = (byte) (1 << 7);// 第八个状态 1000 0000

	/**
	 * 判断是否存在某个状态
	 * 
	 * @param states       当前的位状态码
	 * @param operateState 判断是否存在的状态
	 * @return 是否存在 true存在,false不存在
	 * 
	 */
	public static boolean hasState(byte states, byte operateState) {
		return (states & operateState) != 0;
	}

}
