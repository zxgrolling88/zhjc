package com.app.protocol.gps;

/**
 * 定位状态
 * 
 * @author fanpei
 * @date 2018-09-24 16:51
 *
 */
public enum GpsLocationStaus {
	/**
	 * 定位无效（非精确定位）
	 */
	LocationInvalid(0),

	/**
	 * 2D 定位（非精确定位）
	 */
	Location2D(2),

	/**
	 * 3D 定位（精确定位）
	 */
	Location3D(3);

	private byte value;

	private GpsLocationStaus(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static GpsLocationStaus getGpsLocationStaus(byte value) {
		GpsLocationStaus ll = GpsLocationStaus.LocationInvalid;
		switch (value) {
		case 0:
			ll = GpsLocationStaus.LocationInvalid;
			break;
		case 2:
			ll = GpsLocationStaus.Location2D;
			break;
		case 3:
			ll = GpsLocationStaus.Location3D;
			break;
		default:
			break;
		}
		return ll;
	}
}
