package com.app.protocol.gps;

/**
 * 锁状态
 * 
 * @author fanpei
 * @date 2018-09-24 17:44
 *
 */
public enum GpsLockStatus {
	无(0x00),
	/**
	 * 开启态_停止态
	 */
	开启态_停止态(0x01),

	/**
	 * 待命态
	 */
	待命态(0x2),

	/**
	 * 未锁好
	 */
	未锁好(0x03),
	/**
	 * 施封态
	 */
	施封态(0x04),
	/**
	 * 本地施封态/锁定态
	 */
	本地施封态_锁定态(0x05),
	/**
	 * 解封态
	 */
	解封态(0x06),
	/**
	 * 本地解封态
	 */
	本地解封态(0x0B),
	/**
	 * 报警态
	 */
	报警态(0x07),

	/**
	 * 锁定报警态
	 */
	锁定报警态(0x08),
	/**
	 * 解除报警态
	 */
	解除报警态(0x09),
	/**
	 * 异常
	 */
	异常(0x0A);

	private byte value;

	private GpsLockStatus(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static GpsLockStatus getGpsLockStatus(int value) {
		GpsLockStatus ll = GpsLockStatus.无;
		switch (value) {
		case 0x01:
			ll = GpsLockStatus.开启态_停止态;
			break;
		case 0x02:
			ll = GpsLockStatus.待命态;
			break;
		case 0x03:
			ll = GpsLockStatus.未锁好;
			break;
		case 0x04:
			ll = GpsLockStatus.施封态;
			break;
		case 0x05:
			ll = GpsLockStatus.本地施封态_锁定态;
			break;
		case 0x06:
			ll = GpsLockStatus.解封态;
			break;

		case 0x0B:
			ll = GpsLockStatus.本地解封态;
			break;
		case 0x07:
			ll = GpsLockStatus.报警态;
			break;
		case 0x08:
			ll = GpsLockStatus.锁定报警态;
			break;
		case 0x09:
			ll = GpsLockStatus.解除报警态;
			break;
		case 0x0A:
			ll = GpsLockStatus.异常;
			break;

		default:
			break;
		}
		return ll;
	}
}
