package com.app.protocol.gps;

import soft.ifs.IBytesBuild;
import soft.ifs.IParse;
import soft.net.model.AReadNetStream;

import static soft.common.BConvrtUtil.byteToInt;

/**
 * 客户端状态
 *
 * @author fanpei
 * @date 2018-09-24 17:33
 *
 */
public class BaseStation   {
	protected int lac;// 小区号
	protected int cell;// 基站号
	/**
	 * 大小固定4字节
	 */
	public static final int SIZE = 4;
	public int getLac() {
		return lac;
	}

	public void setLac(int lac) {
		this.lac = lac;
	}

	public int getCell() {
		return cell;
	}

	public void setCell(int cell) {
		this.cell = cell;
	}

	public BaseStation(byte[] datas) {

		lac = 	byteToInt(new byte[]{ datas[1],datas[0]});
		cell = 	byteToInt(new byte[]{ datas[3],datas[2]});
//		System.out.println("基站小区号："+lac);
//		System.out.println("基站基站号："+cell);
	}


}
