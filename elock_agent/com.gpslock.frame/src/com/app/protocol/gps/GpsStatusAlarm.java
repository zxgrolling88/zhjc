package com.app.protocol.gps;

/**
 * 报警状态
 * 
 * @author fanpei
 * @date 2018-09-24 17:36
 *
 */
public class GpsStatusAlarm {
	byte s0;
	byte s1;
	byte lockCharge;// 锁电量（百分比）
	GpsLockStatus lockStatus;// 锁状态

	public byte getS0() {
		return s0;
	}

	public byte getS1() {
		return s1;
	}

	public byte getLockCharge() {
		return lockCharge;
	}

	public GpsLockStatus getLockStatus() {
		return lockStatus;
	}

	public GpsStatusAlarm(byte[] datas) {
		s0 = datas[0];
		s1 = datas[1];
		lockCharge = datas[2];
		lockStatus = GpsLockStatus.getGpsLockStatus(datas[3] >> 4);
	}

	public  static  void main(String args[]){
		GpsLockStatus	lockStatus = 	GpsLockStatus.getGpsLockStatus(0x50 >> 4);
		System.out.println(lockStatus.name());
	}
}
