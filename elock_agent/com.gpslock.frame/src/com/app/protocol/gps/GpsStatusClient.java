package com.app.protocol.gps;

/**
 * 客户端状态
 * 
 * @author fanpei
 * @date 2018-09-24 17:33
 *
 */
public class GpsStatusClient {
	byte s0;
	byte s1;
	byte s2;

	public byte getS0() {
		return s0;
	}

	public byte getS1() {
		return s1;
	}

	public byte getS2() {
		return s2;
	}

	public GpsStatusClient(byte[] datas) {
		s0 = datas[0];
		s1 = datas[1];
		s2 = datas[2];
	}

}
