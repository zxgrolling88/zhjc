package com.app.protocol.gps;

/**
 * 经纬度描述
 * 
 * @author fanpei
 * @date 2018-09-24 16:20
 *
 */
public enum GpsLatiLongtiDscr {
	/**
	 * 北纬东经
	 */
	NLatitudeELongti(0),

	/**
	 * 北纬西经
	 */
	NLatitudeWLongti(1),

	/**
	 * 南纬东经
	 */
	SLatitudeELongti(2),

	/**
	 * 南纬西经
	 */
	SLatitudeWLongti(3);

	private byte value;

	private GpsLatiLongtiDscr(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static GpsLatiLongtiDscr getLatiLongtiLocation(byte value) {
		GpsLatiLongtiDscr ll = GpsLatiLongtiDscr.NLatitudeELongti;
		switch (value) {
		case 0:
			ll = GpsLatiLongtiDscr.NLatitudeELongti;
			break;
		case 1:
			ll = GpsLatiLongtiDscr.NLatitudeWLongti;
			break;
		case 2:
			ll = GpsLatiLongtiDscr.SLatitudeELongti;
			break;
		case 3:
			ll = GpsLatiLongtiDscr.SLatitudeWLongti;
			break;

		default:
			break;
		}
		return ll;
	}
}
