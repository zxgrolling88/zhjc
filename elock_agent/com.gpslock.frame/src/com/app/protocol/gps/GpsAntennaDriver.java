package com.app.protocol.gps;

/**
 * GPS 天线状态＋定位状态＋司机编号
 * 
 * @description 排列顺序：D7 D6 D5 D4 D3 D2 D1 D0注意预留D7 D6 作为冗余位（日后定义使用）
 * 
 * @author fanpei
 * @date 2018-09-24 16:41
 *
 */
public class GpsAntennaDriver {
	GpsAntennaStatus anStatus;// 天线状态
	GpsLocationStaus locStaus;// 定位状态
	byte driverNum;// 司机编号

	/**
	 * 天线状态
	 * 
	 * @return
	 */
	public GpsAntennaStatus getAnStatus() {
		return anStatus;
	}

	/**
	 * 定位状态
	 * 
	 * @return
	 */
	public GpsLocationStaus getLocStaus() {
		return locStaus;
	}

	/**
	 * 司机编号
	 * 
	 * @return
	 */
	public byte getDriverNum() {
		return driverNum;
	}

	public GpsAntennaDriver(byte data) {
		byte anStatusbyte = (byte) ((2 << data) >> 6);// D5 D4
		anStatus = GpsAntennaStatus.getGpsAntennaStatus(anStatusbyte);
		byte locStausByte = (byte) ((4 << data) >> 6);// D3 D2
		locStaus = GpsLocationStaus.getGpsLocationStaus(locStausByte);
		driverNum = (byte) ((6 << data) >> 6);// D1 D0
	}
}
