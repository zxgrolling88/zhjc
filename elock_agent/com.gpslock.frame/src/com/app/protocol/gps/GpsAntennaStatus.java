package com.app.protocol.gps;

/**
 * 天线状态
 * 
 * @author fanpei
 * @date 2018-09-24 16:42
 *
 */
public enum GpsAntennaStatus {
	/**
	 * 正常
	 */
	Nomal(0),

	/**
	 * 开路
	 */
	OpenCircuit(1),

	/**
	 * 短路
	 */
	ShortCircuit(2),

	/**
	 * 非适合天线
	 */
	NonSuitable(3);

	private byte value;

	private GpsAntennaStatus(int value) {
		this.value = (byte) value;
	}

	public byte getValue() {
		return value;
	}

	public static GpsAntennaStatus getGpsAntennaStatus(byte value) {
		GpsAntennaStatus ll = GpsAntennaStatus.Nomal;
		switch (value) {
		case 0:
			ll = GpsAntennaStatus.Nomal;
			break;
		case 1:
			ll = GpsAntennaStatus.OpenCircuit;
			break;
		case 2:
			ll = GpsAntennaStatus.ShortCircuit;
			break;
		case 3:
			ll = GpsAntennaStatus.NonSuitable;
			break;

		default:
			break;
		}
		return ll;
	}
}
