package com.app.protocol.gps;

import soft.common.BytesHexStrTranslate;
import soft.common.StringUtil;

/**
 * 纬度(北纬)
 * 
 * @description 77 15 33 22 -》22 33 15 77,    22 度33.1577 分（北纬）
 *              按顺序第一个字节示度，第二个字节表示分，后两个字节表示分的小数。（每个字节表示两个十进制数字），解析 后为22 度33.1577
 *              分（北纬）
 * 
 * @author fanpei
 * @date 2018-09-24 15:39
 *
 */
public class GpsLatitude extends GpsLatiLongtiBase {

	/**
	 * @param datas 4字节数据 04685629
	 */
	public GpsLatitude(byte[] datas) {
		byte duHigh = (byte) (datas[2] & 0xf0);
		byte fenHigh = (byte) (datas[2] & 0x0f);
		this.degree = Integer.parseInt(StringUtil.getMsgStr("{}{}", datas[3], duHigh));// 度数

		byte fenlow = (byte) (datas[1] & 0xf0);
		byte fenxsHigh = (byte) (datas[1] & 0x0f);
		String fractionStr = StringUtil.getMsgStr("{}{}.{}{}", fenHigh, fenlow, fenxsHigh, datas[0]);
		this.fraction = Float.parseFloat(fractionStr);
	}
}
