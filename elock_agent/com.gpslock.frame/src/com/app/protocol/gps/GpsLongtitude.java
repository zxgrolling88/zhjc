package com.app.protocol.gps;

import soft.common.BytesHexStrTranslate;
import soft.common.StringUtil;

/**
 * 经度(东经)
 * 
 * @description 35 25 40 11 -》11 40 25 35-》114 度02.535 分（东经）
 *              按顺序第一个字节和第二个字节前半个字节表示度，第二个字节后半字节和第三个字节前半个字节表示分，后一个
 *              半字节表示分的小数。（每个字节表示两个十进制数字），解析后为114 度02.535 分（东经）
 * @author fanpei
 * @date 2018-09-24 15:57
 *
 */
public class GpsLongtitude extends GpsLatiLongtiBase {

	/**
	 * @param datas 4个字节 	31071412 》12 14 07 31-》121 度40.731 分（东经）
	 */
	public GpsLongtitude(byte[] datas) {

		System.out.println("longitude:"+ BytesHexStrTranslate.bytesToHexFun1(datas) );
		byte duHigh = (byte) (datas[2] & 0xf0);
		byte fenHigh = (byte) (datas[2] & 0x0f);
		this.degree = Integer.parseInt(StringUtil.getMsgStr("{}{}", datas[3], duHigh));// 度数

		byte fenlow = (byte) (datas[1] & 0xf0);
		byte fenxsHigh = (byte) (datas[1] & 0x0f);
		String fractionStr = StringUtil.getMsgStr("{}{}.{}{}", fenHigh, fenlow, fenxsHigh, datas[0]);
		this.fraction = Float.parseFloat(fractionStr);
	}
}
