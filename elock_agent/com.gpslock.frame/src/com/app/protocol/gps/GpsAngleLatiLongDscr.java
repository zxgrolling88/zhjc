package com.app.protocol.gps;

/**
 * gps度数和经纬度描述
 * 
 * @description （排列顺序：D7 D6 D5 D4 D3 D2 D1 D0 ），其中D5 D4 D3 D2 D1 D0 六位（0～36）用于表示
 *              方向，单位为10°（正北为0°顺时针转, D7 D6 两位用于表示GPS 标志
 * @author fanpei
 * @date 2018-09-24 16:18
 *
 */
public class GpsAngleLatiLongDscr {
	int degree;
	GpsLatiLongtiDscr dscr;

	/**
	 * 获取度数 正北为0°顺时针转
	 * 
	 * @return
	 */
	public int getDegree() {
		return degree;
	}

	/**
	 * 获取经纬度变化
	 * 
	 * @return
	 */
	public GpsLatiLongtiDscr getDscr() {
		return dscr;
	}

	public GpsAngleLatiLongDscr(byte data) {
		byte angleByte = (byte) ((2 << data) >> 2);// 去掉首2位
		this.degree = angleByte * 10;
		byte locByte = (byte) (data >> 6);// 获取首2位
		this.dscr = GpsLatiLongtiDscr.getLatiLongtiLocation(locByte);
	}
}
