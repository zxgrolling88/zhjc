package com.app.access;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;

import com.app.commond.sys.CmdFactoryCenter;
import com.app.conf.AppConfig;
import com.app.net.MyNetCreator;

import com.app.redis.JedisConnectionPool;
import com.app.redis.SubThread;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import soft.access.NetInitUtil;
import soft.common.StaticClass;
import soft.net.lconectserver.LConectServer;

/**
 * 框架初始化
 * 
 * @author fanpei
 * @date 2018-09-09 16:04
 *
 */
public class AppInit extends StaticClass {

	/**
	 * 框架初始化
	 * 
	 * @throws Exception
	 */
	public static void init() throws Exception {
		String runPath =null;
		//		String runPath = new File(URLDecoder.decode(url.getPath(), "utf-8")).getAbsolutePath();
		// String rootDir = HDFSPathUtil.getParentDir(runPath);
		NetInitUtil.initSysArgs(runPath);
		AppConfig.readConf();

		CmdFactoryCenter.loadAllCmd();
		LConectServer.init(new MyNetCreator());
		JedisPool jedisPool = JedisConnectionPool.getInstance();
		SubThread subThread = new SubThread(jedisPool);  //订阅者
		subThread.start();
		LConectServer server = new LConectServer();
		server.start();
		// ClientConfig cliConfig = new ClientConfig();// 读取配置文件
		// SConectClient.init(cliConfig, new ListennerCreator());

	}

	public static void main(String[] args) {
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
