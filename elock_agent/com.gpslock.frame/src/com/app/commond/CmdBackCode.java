package com.app.commond;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;

/**
 * 0x03 指令回码
 * 
 * @author fanpei
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX03, funcCode = CmdFuncCode.OX03_CmdRespCode)
public class CmdBackCode extends CmdBase {

	@Override
	public void parse() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void excute() throws Exception {

	}

}
