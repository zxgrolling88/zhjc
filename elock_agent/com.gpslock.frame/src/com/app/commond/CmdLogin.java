package com.app.commond;

import com.app.commond.recv.LoginInfo;
import com.app.commond.recv.RespHeart;
import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.exception.NotEnoughException;
import com.app.protocol.entity.ProtocolGPS;
import com.app.protocol.entity.SIMUserID;
import com.app.protocol.entity.SIMUserID.MobleIDType;

import soft.net.exception.DataIsNullException;

/**
 * 登录命令
 * 
 * @author fanpei
 * @date 2018-09-19 23:48
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX07, funcCode = CmdFuncCode.OX07_Login)
public class CmdLogin extends CmdBase {

	public static final MobleIDType TYPE = MobleIDType.ICCID;// 默认为ICCID

	private LoginInfo info;// 用户信息

	public CmdLogin() {
	}

	public CmdLogin(LoginInfo info) {
		this.info = info;
	}

	@Override
	public void validate() throws DataIsNullException, NotEnoughException {
		if (protocol.getDatas() == null) {
			throw new DataIsNullException("the user login info is null");
		}
		if (protocol.getDatas().length != ProtocolGPS.SIZE
				&& protocol.getDatas().length != (ProtocolGPS.SIZE + SIMUserID.ICCIDREALLEN)) {
			throw new NotEnoughException("the login userinfo bytes's length is not correct!");
		}
	}

	@Override
	public void parse() throws Exception {
		byte[] gps = new byte[ProtocolGPS.SIZE];
		byte[] simIDs = null;

		System.arraycopy(protocol.getDatas(), 0, gps, 0, ProtocolGPS.SIZE);
		if (protocol.getDatas().length == ProtocolGPS.SIZE + SIMUserID.ICCIDREALLEN) // 用户信息可能为空
		{
			simIDs = new byte[SIMUserID.ICCIDREALLEN];
			System.arraycopy(protocol.getDatas(), ProtocolGPS.SIZE, simIDs, 0, SIMUserID.ICCIDREALLEN);
		}

		// 3.LoginInfo
		info = new LoginInfo(TYPE, gps, simIDs);
		info.parse();
	}

	@Override
	public void excute() throws Exception {

		// 发送授时命令
		channel.sendData(new RespHeart());
	}

}
