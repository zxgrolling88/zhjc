package com.app.commond.recv;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBuild;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;

import soft.common.StringUtil;
import soft.ifs.IWriteLog;
import soft.log.LogWriter;
import soft.net.exception.DataIsNullException;

/**
 * 心跳回复
 * 
 * @author fanpei
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX21, funcCode = CmdFuncCode.OX21_CenterTimeSend)
public class RespHeart extends CmdBuild {
	private static final IWriteLog log = new LogWriter(RespHeart.class);
	private static DateFormat dateFormat = new SimpleDateFormat("YY/MM/dd,HH:mm:ssZ");
	private String servertime;// 服务器时间

	/**
	 * 获取时间时间字符串
	 * 
	 * @return
	 */
	private String getTimeStr() {
		servertime = dateFormat.format(new Date());
		return servertime.substring(0, servertime.length() - 2);
	}

	public RespHeart() {
		// TODO Auto-generated constructor stub
	}

	public RespHeart(byte[] times) {
		this.times = times;
	}

	@Override
	public byte[] buildBytes() {
		byte[] sendDatas = StringUtil.getUtf8Bytes(getTimeStr());
		return buildBytes(sendDatas);
	}

	private byte[] times;

	@Override
	public void parse() throws Exception {
		if (times == null)
			throw new DataIsNullException("RespHeart parse:tiems is null!");
		servertime = StringUtil.getUtf8Str(times);
		times = null;
		log.debug("parse respheartbeat success,server time:{}", servertime);
	}

}
