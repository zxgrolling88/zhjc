package com.app.commond.recv;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBuild;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.protocol.Direction;
import com.app.protocol.entity.ProtocolGPS;
import com.app.protocol.entity.SIMUserID;
import com.app.protocol.entity.SIMUserID.MobleIDType;

import soft.net.exception.DataIsNullException;

/**
 * 登录信息
 * 
 * @author fanpei
 * @date 2018-09-20 22:36
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX07, funcCode = CmdFuncCode.OX07_Login)
public class LoginInfo extends CmdBuild {
	private ProtocolGPS gpsData;// gps信息
	private SIMUserID simUserID;// 用户信息

	public ProtocolGPS getGpsData() {
		return gpsData;
	}

	public SIMUserID getSimUserID() {
		return simUserID;
	}

	/**
	 * default construtor
	 */
	public LoginInfo() {
	}

	/**
	 * 登录信息--client
	 * 
	 * @test
	 * @param lockDevID
	 */
	public LoginInfo(int lockDevID) {
		direc = Direction.UP;
		this.lockDevID = lockDevID;
		gpsData = new ProtocolGPS();
		simUserID = new SIMUserID();
	}

	/**
	 * 无用户信息登录信息construtor
	 * 
	 * @param gpsDatas
	 */
	public LoginInfo(ProtocolGPS gpsData) {
		this.gpsData = gpsData;
	}

	/**
	 * GPS construtor
	 * 
	 * @param gpsData
	 * @param simUserID
	 */
	public LoginInfo(ProtocolGPS gpsData, SIMUserID simUserID) {
		this.gpsData = gpsData;
		this.simUserID = simUserID;
	}

	public LoginInfo(MobleIDType type, byte[] gpsbytes, byte[] simidbytes) {
		this.type = type;
		this.gpsbytes = gpsbytes;
		this.simidbytes = simidbytes;
	}

	@Override
	public byte[] buildBytes() throws DataIsNullException {
		int len = ProtocolGPS.SIZE;
		if (gpsData == null)
			throw new DataIsNullException("LoginInfo build,gpsData is null!");

		gpsbytes = gpsData.buildBytes();
		if (simUserID != null) {
			simidbytes = simUserID.buildBytes();
			len += SIMUserID.ICCIDREALLEN;
		}
		byte[] sends = new byte[len];
		System.arraycopy(gpsbytes, 0, sends, 0, ProtocolGPS.SIZE);
		if (simUserID != null)
			System.arraycopy(simidbytes, 0, sends, ProtocolGPS.SIZE, SIMUserID.ICCIDREALLEN);
		return buildBytes(sends);
	}

	/// 解析用
	private byte[] gpsbytes;
	private byte[] simidbytes;
	private MobleIDType type;

	@Override
	public void parse() throws Exception {
		// 1.parse gps
		if (gpsbytes == null)
			throw new DataIsNullException("LoginInfo parse,gpsbytes is null!");
		gpsData = new ProtocolGPS(gpsbytes);
		gpsData.parse();
		//gpsData.validate();

		// 2.parse simuserid
		if (simidbytes != null) {
			simUserID = new SIMUserID(type, simidbytes);
			simUserID.parse();
		}

		clear();
	}

	@Override
	public void clear() {
		gpsbytes = null;
		simidbytes = null;
		type = null;
	}
}
