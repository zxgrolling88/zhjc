package com.app.commond.recv;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBuild;
import com.app.commond.sys.CmdDevAddr;
import com.app.protocol.Direction;
import com.app.protocol.entity.HeartInfo;

/**
 * 心跳信息
 * 
 * @author fanpei
 * @date 2018-09-19 22:25
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX05, funcCode = 10)
public class Heart extends CmdBuild {

	private HeartInfo version;// 版本号

	public Heart(String version, int lockDevId) {
		this.direc = Direction.UP;
		this.lockDevID = lockDevId;
		this.version = new HeartInfo(version, 0);
	}

	public Heart(int lockDevId, int heartInterval, byte[] datas) {
		this.lockDevID = lockDevId;
		this.version = new HeartInfo(heartInterval, datas);
	}

	@Override
	public byte[] buildBytes() {
		byte[] sendDatas = version.buildBytes();
		return buildBytes(sendDatas);
	}

	@Override
	public void parse() throws Exception {
		version.parse();

	}

}
