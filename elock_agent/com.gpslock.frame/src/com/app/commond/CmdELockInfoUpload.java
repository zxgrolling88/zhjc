package com.app.commond;

import com.app.commond.entity.GpsBean;
import com.app.commond.entity.LockInfoBean;
import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.exception.NotEnoughException;
import com.app.protocol.ProtocolLockInfo;
import com.app.protocol.entity.ProtocolGPS;
import com.app.protocol.gps.GpsLockStatus;
import com.app.redis.Publisher;
import soft.common.BConvrtUtil;
import soft.common.BytesHexStrTranslate;
import soft.common.DateUtils;
import soft.common.JSONUtil;
import soft.net.exception.DataIsNullException;

import java.text.ParseException;

/**
 * 电子锁信息上传
 *
 终端回复：
 操作回复（1B,回复 0x55）+Cmd（1B）+LockID(4B)+Gate（1B）+ Voltage(1B)+LockStatus（1B）
 + MotoStatus (1B)+ Time (4B)+Ver (1B) +绝对时间（6B）
 开始    操作回复   Cmd  LockID     Gate       Voltage  LockStatus    MotoStatus  Time             Ver    绝对时间       结束  子功能  子功能
 2A      55       62   06CB042C   04          37      50            02          14546EC9         03     190107154826 2303810200               190107154826 23  0381   0200
 2A      55       62   072D177C   04          33      20            00          14548424         03     190107171932 2303810100

 2A      55       62   06CB042C 0436500214554AB1031901080726412303810200 *
 * @author fanpei
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX03, funcCode = CmdFuncCode.OX03_ELockInfoUpload)
public class CmdELockInfoUpload extends CmdBase {
	private ProtocolLockInfo protocolLockInfo;// 电子锁信息
	public CmdELockInfoUpload() {
		protocolLockInfo = new ProtocolLockInfo();
	}

	@Override
	public void parse() throws Exception {
		if (protocol.getDatas() == null) {
			throw new DataIsNullException("the elock info is null");
		}

		if (protocol.getDatas().length != ProtocolLockInfo.UP_SIZE&&
				protocol.getDatas().length != (ProtocolLockInfo.UP_SIZE-4)) {
			throw new NotEnoughException("the elock info bytes's length is not correct!"+protocol.getDatas().length);
		}

		byte[] elockbytes = new byte[protocol.getDatas().length];
		System.arraycopy(protocol.getDatas(), 0, elockbytes, 0, protocol.getDatas().length);
		if (elockbytes == null) {
			throw new DataIsNullException("elock parse,elockbytes is null!");
		}
		protocolLockInfo = new ProtocolLockInfo(elockbytes);
		protocolLockInfo.parse();
		protocolLockInfo.validate();
		sendLockInfoToMq();
	}

	@Override
	public void excute() throws Exception {
		channel.sendData(protocolLockInfo);

	}
	private void sendLockInfoToMq() throws Exception {
		LockInfoBean lockInfoBean = new LockInfoBean();
		lockInfoBean.setCmd(BConvrtUtil.getLockRespStatus(protocolLockInfo.getCmd()));
		lockInfoBean.setCmdSRC(BConvrtUtil.getLockOprSource(protocolLockInfo.getCmdSRC()));
		lockInfoBean.setLockDevID(protocol.getLockDevID());
		GpsLockStatus	lockStatus = GpsLockStatus.getGpsLockStatus(protocolLockInfo.getLockStatus() >> 4);
		lockInfoBean.setLockStatus(lockStatus.name());
		lockInfoBean.setSendtime(DateUtils.formatHexTime(BytesHexStrTranslate.bytesToHexFun1(protocolLockInfo.getSendtime())));
		lockInfoBean.setVoltage(protocolLockInfo.getVoltage());
		Publisher.sendLockStatus(JSONUtil.genJsonStr(lockInfoBean));
		System.out.println("上行内容:"+JSONUtil.genJsonStr(lockInfoBean) );
	}

}
