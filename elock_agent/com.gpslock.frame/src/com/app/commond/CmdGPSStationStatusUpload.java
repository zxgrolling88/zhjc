package com.app.commond;

import com.app.commond.entity.GpsBean;
import com.app.commond.recv.RespHeart;
import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.exception.NotEnoughException;
import com.app.protocol.entity.ProtocolGPS;
import com.app.protocol.gps.BaseStation;
import com.app.redis.Publisher;
import soft.common.BytesHexStrTranslate;
import soft.common.DateUtils;
import soft.common.JSONUtil;
import soft.net.exception.DataIsNullException;

/**
 *  0x03 GPRS 带基站信息的 GPS 数据上传/点名回复
 *
 * @author fanpei
 */
@AppCmd(devAddr = CmdDevAddr.OX03, funcCode = CmdFuncCode.OX03_GPSStationStatusUpload)
public class CmdGPSStationStatusUpload extends CmdBase {

    private ProtocolGPS gpsData;// gps信息

    private BaseStation baseStation;// 基站信息

    @Override
    public void parse() throws Exception {
       // protocol.wait();
        if (protocol.getDatas() == null) {
            throw new DataIsNullException("the GPS info is null");
        }
        if (protocol.getDatas().length != ProtocolGPS.SIZE
                && protocol.getDatas().length != (ProtocolGPS.SIZE + 8)) {
            throw new NotEnoughException("the GPS info bytes's length is not correct!");
        }
        System.out.println(protocol.getLockDevID()+",protocol:"+ BytesHexStrTranslate.bytesToHexFun1(protocol.getDatas()) );
        byte[] gpsbytes = new byte[ProtocolGPS.SIZE];
        System.arraycopy(protocol.getDatas(), 0, gpsbytes, 0, ProtocolGPS.SIZE);
        if (gpsbytes == null) {
            throw new DataIsNullException("GPS parse,gpsbytes is null!");
        }
        gpsData = new ProtocolGPS(gpsbytes);
        gpsData.parse();
       // gpsData.validate();

        if (protocol.getDatas().length == (ProtocolGPS.SIZE + 8)) {
            byte[] baseStationbytes = new byte[BaseStation.SIZE];
            System.arraycopy(protocol.getDatas(), ProtocolGPS.SIZE, baseStationbytes, 0, BaseStation.SIZE);
            System.out.println(protocol.getLockDevID()+"baseStation:"+ BytesHexStrTranslate.bytesToHexFun1(baseStationbytes) );
            baseStation = new BaseStation(baseStationbytes);
        }

        sendGpsToMq();

    }

    private void sendGpsToMq(){
        GpsBean gpsBean = new GpsBean();
        gpsBean.setDate(DateUtils.formatDateTime(gpsData.getDate()));
        gpsBean.setGpsAntennaStatus(gpsData.getGpsStatusDriver().getAnStatus().getValue());
        gpsBean.setLockCharge(gpsData.getAlarmStatus().getLockCharge());
        gpsBean.setLockDevID(protocol.getLockDevID());
        gpsBean.setAlarmStatus((int)gpsData.getAlarmStatus().getS0());
        gpsBean.setLockStatus(gpsData.getAlarmStatus().getLockStatus().name());
        gpsBean.setLongitude(gpsData.getLongitude());
        gpsBean.setLatitude(gpsData.getLatitude());
        gpsBean.setTotalMile(gpsData.getTotalMile());
        gpsBean.setSpeed(gpsData.getSpeed());
        gpsBean.setDirection(gpsData.getDirecGPSFlag());
        gpsBean.setGpsLocationStaus(gpsData.getGpsStatusDriver().getLocStaus().getValue());
        if(baseStation!=null){
            gpsBean.setCell(baseStation.getCell());
            gpsBean.setLac(baseStation.getLac());
        }

        Publisher.sendGps(JSONUtil.genJsonStr(gpsBean));
    }

    @Override
    public void excute() throws Exception {
        // 发送开锁命令

        //090005
//        byte[] key = {(byte) 0x48,(byte) 0x32,(byte) 0x32,(byte) 0x32,(byte) 0x32,(byte) 0x32};
//        channel.sendData(new CmdCenterSendELock((byte) 0x38,74000349,key));
//        byte[]  key = "090005".getBytes();
//        ServerConMap store  = 	LConectServer.store;
//        List<INetChanel> iNetChanels =  (List)store.getAllChanels();
//        INetChanel  chanel = iNetChanels.get(0);
//        chanel.sendData(new CmdCenterSendELock((byte) 0x38,74000349,key));
    }

}
