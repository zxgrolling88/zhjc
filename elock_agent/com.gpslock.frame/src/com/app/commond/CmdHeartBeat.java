package com.app.commond;

import com.app.commond.recv.Heart;
import com.app.commond.recv.RespHeart;
import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;

/**
 * 心跳数据
 * 
 * @author fanpei
 * @date 2018-09-16 22:25
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX05, funcCode = CmdFuncCode.OX05_HearBeat)
public class CmdHeartBeat extends CmdBase {

	private Heart heart;

	@Override
	public boolean isHearBeat() {
		return true;
	}

	@Override
	public void parse() throws Exception {
		heart = new Heart(protocol.getLockDevID(), protocol.getFuncCode(), protocol.getDatas());
		heart.parse();
	}

	@Override
	public void excute() throws Exception {
		channel.sendData(new RespHeart());
	}

}
