package com.app.commond;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.exception.NotEnoughException;
import com.app.protocol.entity.ProtocolGPS;
import soft.common.BytesHexStrTranslate;
import soft.net.exception.DataIsNullException;

/**
 * GPRS 盲区补偿单点上传
 * 
 * @author fanpei
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX03, funcCode = CmdFuncCode.OX03_GPSCompensateSingleUpload)
public class CmdGPSCompensateSingleUpload extends CmdBase {
	private ProtocolGPS gpsData;// gps信息
	public CmdGPSCompensateSingleUpload() {
		gpsData = new ProtocolGPS();
	}

	@Override
	public void parse() throws Exception {
//		if (protocol.getDatas() == null) {
//			throw new DataIsNullException("the GPS info is null");
//		}
//		if (protocol.getDatas().length != ProtocolGPS.SIZE
//				&& protocol.getDatas().length != (ProtocolGPS.SIZE + 8)) {
//			throw new NotEnoughException("the GPS info bytes's length is not correct!");
//		}
//		byte[] gpsbytes = new byte[ProtocolGPS.SIZE];
//		System.arraycopy(protocol.getDatas(), 0, gpsbytes, 0, ProtocolGPS.SIZE);
//		if (gpsbytes == null) {
//			throw new DataIsNullException("GPS parse,gpsbytes is null!");
//		}else{
//			System.out.println("OX03_GPSCompensateSingleUpload:"+BytesHexStrTranslate.bytesToHexFun1(gpsbytes) );
//		}
//		gpsData = new ProtocolGPS(gpsbytes);
//		gpsData.parse();
//		gpsData.validate();

	}

	@Override
	public void excute() throws Exception {
		// TODO Auto-generated method stub

	}

}
