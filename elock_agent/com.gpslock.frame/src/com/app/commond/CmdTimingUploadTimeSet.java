package com.app.commond;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdBase;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;

/**
 * 定时上传时间设置
 * 
 * @author fanpei
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX21, funcCode = CmdFuncCode.OX21_TimingUploadTimeSet)
public class CmdTimingUploadTimeSet extends CmdBase {

	@Override
	public void parse() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void excute() throws Exception {

	}

}
