package com.app.commond.sys;

import soft.common.StaticClass;

/**
 * 命令设备地址
 * 
 * @author fanpei
 *
 */
public class CmdDevAddr extends StaticClass {
	/**
	 * 地址0x03 指令
	 */
	public static final byte OX03 = 0x03;
	/**
	 * 地址0x04 指令
	 */
	public static final byte OX04 = 0x04;

	/**
	 * 地址0x05 指令 心跳
	 */
	public static final byte OX05 = 0x05;
	/**
	 * 地址0x07 指令
	 */
	public static final byte OX07 = 0x07;
	/**
	 * 地址0x21 指令
	 */
	public static final byte OX21 = 0x21;
	/**
	 * 地址0x31 指令
	 */
	public static final byte OX31 = 0x31;
	/**
	 * 地址0x81 指令
	 */
	public static final byte OX81 = (byte) 0x81;
}
