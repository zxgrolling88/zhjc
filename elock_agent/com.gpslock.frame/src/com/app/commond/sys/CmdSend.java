package com.app.commond.sys;

import com.app.protocol.Direction;
import com.app.protocol.ProtocolBasic;

import soft.ifs.INetTrans;

/**
 * 命令返回接口
 * 
 * @author fanpei
 *
 */
public abstract class CmdSend implements INetTrans {
	protected Direction direc = Direction.DOWN;// 数据方式
	protected int lockDevID = -1;// 设备id

	/**
	 * 组装数据
	 * 
	 * @param datas
	 *            数据部分
	 * @param anno
	 *            地址和功能信息
	 * @param childFunNum
	 *            子功能数量
	 * @return
	 */
	public byte[] buildBytes(byte[] datas, AppCmd anno) {
		AppCmd tmpanno = anno;
		if (tmpanno == null) {
			tmpanno = this.getClass().getAnnotation(AppCmd.class);
		}
		ProtocolBasic mp = new ProtocolBasic(lockDevID, tmpanno.devAddr(), tmpanno.funcCode(),
				tmpanno.childFunNum(), datas, (byte) 0);
		mp.setDirec(direc);
		return mp.buildSendBytes();
	}

	/**
	 * 组装数据
	 * 
	 * @param datas
	 *            数据部分
	 * @return
	 */
	public byte[] buildBytes(byte[] datas) {
		return buildBytes(datas, null);
	}
}
