package com.app.commond.sys;

import java.io.IOException;

import com.app.ifs.ICmdRecvHandle;
import com.app.protocol.ProtocolBasic;

import soft.ifs.INetChanel;
import soft.ifs.IWriteLog;
import soft.log.LogWriter;

/**
 * 命令基类
 * 
 * @author fanpei
 * @date 2018-09-16 22:27
 *
 */
public abstract class CmdBase implements ICmdRecvHandle, Runnable {
	private static final IWriteLog log = new LogWriter(CmdBase.class);

	protected INetChanel channel;// 网络链路
	protected ProtocolBasic protocol;// 协议

	public CmdBase() {
		// reflection create need
	}

	/**
	 * 设置链路和协议数据
	 * 
	 * @param channel
	 * @param protocol
	 */
	public void setNetAndProtocol(INetChanel channel, ProtocolBasic protocol) {
		this.channel = channel;
		this.protocol = protocol;
	}

	/**
	 * 数据校验
	 * 
	 * @throws Exception
	 */
	public void validate() throws Exception {

	}

	/**
	 * 是否是心跳数据
	 * 
	 * @return
	 */
	public boolean isHearBeat() {
		return false;
	}

	@Override
	public void run() {
		try {
			validate();
			parse();
			excute();
		} catch (IOException e) {
			log.warn("执行命令时错误,CmdIno:{}", protocol == null ? "protocol null" : protocol.getCmdKey(), e);
		} catch (Exception e) {
			log.error("执行命令时错误,CmdIno:{}", protocol == null ? "protocol null" : protocol.getCmdKey(), e);
		}

	}

}
