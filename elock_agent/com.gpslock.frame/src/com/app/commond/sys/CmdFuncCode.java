package com.app.commond.sys;

import soft.common.StaticClass;

/**
 * 功能代码以及子功能数量
 * 
 * @author fanpei
 *
 */
public class CmdFuncCode extends StaticClass {

	/**
	 * 默认子功能数量
	 */
	public static final byte DEFAULCHILDNUM = 0x00;
	/**
	 * 表示需要
	 终端通信确认
	 */
	public static final byte CONFIRMCHILDNUM = 0x01;
	/**
	 * 表示中心下发电子锁指令子功能数量
	 */
	public static final byte DOWNLOCKCHILDNUM = 0x03;

	// ---------------------------------------0x03------------------------------------//
	/**
	 * 0x03 GPRS 盲区补偿单点上传
	 */
	public static final byte OX03_GPSCompensateSingleUpload = 0x01;

	/**
	 * 0x03 GPRS 带基站信息的 GPS 数据上传/点名回复
	 */
	public static final byte OX03_GPSStationStatusUpload = 0x09;

	/**
	 * 0x03 终端发送到中心的提示文字信息/文字命令
	 */
	public static final byte COX03_OnsleSendTextTipInfo = 0x10;

	/**
	 * 0x03 指令回码
	 */
	public static final byte OX03_CmdRespCode = 0x16;

	/**
	 * 0x03 2D 电子锁信息上传
	 */
	public static final byte OX03_ELockInfoUpload = 0x2D;

	public static final byte OX03_ELockInfoUpload_FuncNum = 0x04;

	// ---------------------------------------0x05------------------------------------//
	/**
	 * 0x05 心跳
	 */
	public static final byte OX05_HearBeat = 0;

	// ---------------------------------------0x07------------------------------------//

	/**
	 * 0x07 登录信息
	 */
	public static final byte OX07_Login = 0x01;

	/**
	 * 0x07 登录信息2
	 */
	public static final byte OX07_login2 = 0x02;

	// ---------------------------------------0x21------------------------------------//

	/**
	 * 0x21 定时上传时间间隔设置指令
	 */
	public static final byte OX21_TimingUploadTimeSet = 0x03;

	/**
	 * 0x21 中心授时指令
	 */
	public static final byte OX21_CenterTimeSend = 0x10;

	// ---------------------------------------0x31------------------------------------//

	/**
	 * 0x31 更改IP指令
	 */
	public static final byte OX31_IPChange = 0x01;

	// ---------------------------------------0x51------------------------------------//

	/**
	 * 0x51 中心发送到GPRS终端的文字信息/文字命令（解施封）
	 */
	public static final byte OX51_CenterSendGPRSConsleText = 0x11;

	// ---------------------------------------0x81------------------------------------//

	/**
	 * 0x81 中心下发确认收到指令
	 */
	public static final byte OX51_CenterSendConfirm = 0x27;

	/**
	 * 0x81 中心下发电子锁指令
	 */
	public static final byte OX51_CenterSendELock = 0x2D;


}
