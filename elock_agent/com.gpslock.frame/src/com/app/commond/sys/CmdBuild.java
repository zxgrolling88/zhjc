package com.app.commond.sys;

import com.app.protocol.Direction;
import com.app.protocol.ProtocolBasic;

import soft.common.BytesHexStrTranslate;
import soft.ifs.IBytesBuild;
import soft.ifs.IParse;

/**
 * 命令组建对象
 * 
 * @author fanpei
 *
 */
public abstract class CmdBuild implements IParse, IBytesBuild {
	protected Direction direc = Direction.DOWN;// 数据方式
	protected int lockDevID = -1;// 设备id

	public void setLockDevID(int lockDevID) {
		this.lockDevID = lockDevID;
	}

	public void setDirec(Direction direc) {
		this.direc = direc;
	}

	/**
	 * 组装数据
	 * 
	 * @param datas
	 *            数据部分
	 * @param anno
	 *            地址和功能信息
	 * @return
	 */
	public byte[] buildBytes(byte[] datas, AppCmd anno) {
		AppCmd tmpanno = anno;
		if (tmpanno == null) {
			tmpanno = this.getClass().getAnnotation(AppCmd.class);
		}
		ProtocolBasic mp = new ProtocolBasic(lockDevID, tmpanno.devAddr(), tmpanno.funcCode(), tmpanno.childFunNum(),
				datas, (byte) 0);
		mp.setDirec(direc);
		byte[] sendData =mp.buildSendBytes();
		System.out.println("下行报文：" + BytesHexStrTranslate.bytesToHexFun1(sendData));

		return sendData;
	}

	/**
	 * 组装数据
	 * 
	 * @param datas
	 *            数据部分
	 * @return
	 */
	public byte[] buildBytes(byte[] datas) {
		return buildBytes(datas, null);
	}
}
