package com.app.commond.sys;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 命令 注解 若
 * 
 * @Warning 若用此注解，若存在有参构造函数，则必须显示声明无参构造函数
 * 
 * @author fanpei
 * @date 2018-09-18 21:00
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AppCmd {
	/**
	 * 设备地址
	 * 
	 * @return
	 */
	byte devAddr();

	/**
	 * 功能
	 * 
	 * @return
	 */
	byte funcCode() default -1;

	/**
	 * 子功能个数
	 * 
	 * @return
	 */
	byte childFunNum() default CmdFuncCode.DEFAULCHILDNUM;
}
