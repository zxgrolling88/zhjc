package com.app.commond.entity;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 07:38
 */
public class CmdChangeIpBean {

    private int  lockDevID;
    private String ip;
    private String port;//锁类型，1表示密码锁，2表示普通锁

    public int getLockDevID() {
        return lockDevID;
    }

    public void setLockDevID(int lockDevID) {
        this.lockDevID = lockDevID;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
