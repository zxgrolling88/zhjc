package com.app.commond.entity;

import com.app.exception.SIMUserIDErrorException;

/**
 * SIM 用户信息
 * 
 * @author fanpei
 * @date 2018-09-20 22:04
 *
 */
public class SIMUserID {
	/**
	 * ICCID CHINA用户前五位
	 */
	public static final byte[] CHINASIMFLAG = new byte[] { 8, 9, 8, 6, 0 };
	/**
	 * IMSI用户信息长度
	 */
	public static final int IMSILEN = 15;
	/**
	 * ICCID 国别信息长度
	 */
	public static final int ICCIDSIMFLAGLEN = 5;
	/**
	 * ICCID用户信息长度
	 */
	public static final int ICCIDLEN = 20;

	/**
	 * SIM ID类型
	 */
	private MobleIDType mobleIDType;
	/**
	 * IMSI 共有 15 位，一个典型的 IMSI 号码为 460030912121001
	 */
	private byte[] IMSI;
	/**
	 * ICCID 一共 20 位，一个典型的 ICCID 号码为 898602B0101730418921 ,国内 SIM 卡前 5 位都是
	 * 89860后续版本的锁 没有特别要求都是传输 ICCID 后 15 位
	 */
	private byte[] ICCID;// 后 15 位

	/**
	 * 获取用户SIM用户信息长度
	 * 
	 * @return
	 */
	public int getSIMIDLen() {
		if (MobleIDType.IMSI == mobleIDType)
			return IMSILEN;
		else
			return ICCIDLEN - ICCIDSIMFLAGLEN;
	}

	/**
	 * 获取用户SIM用户信息
	 * 
	 * @return
	 */
	public byte[] getSIMIDs() {
		if (MobleIDType.IMSI == mobleIDType)
			return IMSI;
		else
			return ICCID;
	}

	/**
	 * 用户信息构造函数
	 * 
	 * @param mobleIDType SIM ID类型
	 * @param ids         用户信息
	 * @throws SIMUserIDErrorException
	 */
	public SIMUserID(MobleIDType mobleIDType, byte[] ids) throws SIMUserIDErrorException {
		if (MobleIDType.IMSI == mobleIDType) {
			if (ids == null || IMSILEN != ids.length) {
				throw new SIMUserIDErrorException("SIM UerID IMSI is null or IMSI's len is not correct!");
			}
			IMSI = ids;
		} else {
			if (ids == null || (ICCIDLEN - ICCIDSIMFLAGLEN) != ids.length) {
				throw new SIMUserIDErrorException("SIM UerID ICCID is null or ICCID's len is not correct!");
			}
			ICCID = ids;
		}
	}

	/**
	 * SIM ID类型
	 * 
	 * @author fanpei
	 * @date 2018-09-20 22:19
	 *
	 */
	public enum MobleIDType {
		IMSI, ICCID;
	}

}
