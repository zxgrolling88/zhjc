package com.app.commond.entity;

import soft.common.DateUtils;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 07:38
 */
public class GpsBean {

    private int  lockDevID;
    private String date;
    private double longitude;
    private double latitude;
    private int  direction;
    private int  speed;
    private float  totalMile;
    private String lockStatus;
    private int  alarmStatus;//锁电量（百分比）
    private int  lockCharge;//锁电量（百分比）
    private int  gpsAntennaStatus;//0正常,1开路，2短路，3非适合天线
    private int  gpsLocationStaus;//0定位无效（非精确定位）,2D 定位（非精确定位）, 3D 定位（精确定位）
    private int  lac;
    private int  cell;

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getCell() {
        return cell;
    }

    public void setCell(int cell) {
        this.cell = cell;
    }

    public int getAlarmStatus() {
        return alarmStatus;
    }

    public void setAlarmStatus(int alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getLockDevID() {
        return lockDevID;
    }

    public void setLockDevID(int lockDevID) {
        this.lockDevID = lockDevID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public float getTotalMile() {
        return totalMile;
    }

    public void setTotalMile(float totalMile) {
        this.totalMile = totalMile;
    }

    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public int getLockCharge() {
        return lockCharge;
    }

    public void setLockCharge(int lockCharge) {
        this.lockCharge = lockCharge;
    }

    public int getGpsAntennaStatus() {
        return gpsAntennaStatus;
    }

    public void setGpsAntennaStatus(int gpsAntennaStatus) {
        this.gpsAntennaStatus = gpsAntennaStatus;
    }

    public int getGpsLocationStaus() {
        return gpsLocationStaus;
    }

    public void setGpsLocationStaus(int gpsLocationStaus) {
        this.gpsLocationStaus = gpsLocationStaus;
    }
}
