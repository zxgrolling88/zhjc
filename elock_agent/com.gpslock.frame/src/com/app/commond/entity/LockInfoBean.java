package com.app.commond.entity;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-30 07:38
 */
public class LockInfoBean {

    private int  lockDevID;
    private String sendtime;
    private String cmd;
    private double voltage;
    private String lockStatus;
    private String  cmdSRC;//操作源

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getLockDevID() {
        return lockDevID;
    }

    public void setLockDevID(int lockDevID) {
        this.lockDevID = lockDevID;
    }

    public String getSendtime() {
        return sendtime;
    }

    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public String getCmdSRC() {
        return cmdSRC;
    }

    public void setCmdSRC(String cmdSRC) {
        this.cmdSRC = cmdSRC;
    }
}
