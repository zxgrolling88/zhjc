package com.app.commond.resp;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdSend;
import com.app.protocol.Direction;

import soft.common.StringUtil;

/**
 * 心跳信息
 * 
 * @author fanpei
 * @date 2018-09-19 22:25
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX05, funcCode = 10)
public class Heart extends CmdSend {
	private String version = "";// 版本号

	public Heart(String version, int lockDevId) {
		this.direc = Direction.UP;
		this.version = version;
	}

	@Override
	public byte[] buildBytes() {
		byte[] sendDatas = StringUtil.getUtf8Bytes(version);
		return buildBytes(sendDatas);
	}

}
