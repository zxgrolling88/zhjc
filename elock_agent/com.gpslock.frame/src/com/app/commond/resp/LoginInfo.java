package com.app.commond.resp;

import com.app.commond.entity.SIMUserID;
import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.commond.sys.CmdSend;
import com.app.protocol.ProtocolGPS;

/**
 * 登录信息
 * 
 * @author fanpei
 * @date 2018-09-20 22:36
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX07, funcCode = CmdFuncCode.OX07_Login)
public class LoginInfo extends CmdSend {
	private ProtocolGPS gpsDatas;// gps信息
	private SIMUserID simUserID;// 用户信息

	public ProtocolGPS getGpsDatas() {
		return gpsDatas;
	}

	public SIMUserID getSimUserID() {
		return simUserID;
	}

	/**
	 * 无用户信息登录信息construtor
	 * 
	 * @param gpsDatas
	 */
	public LoginInfo(ProtocolGPS gpsDatas) {
		this.gpsDatas = gpsDatas;
	}

	/**
	 * GPS construtor
	 * 
	 * @param gpsDatas
	 * @param simUserID
	 */
	public LoginInfo(ProtocolGPS gpsDatas, SIMUserID simUserID) {
		this.gpsDatas = gpsDatas;
		this.simUserID = simUserID;
	}

	/**
	 * default construtor
	 */
	public LoginInfo() {
	}

	@Override
	public byte[] buildBytes() {
		// TODO Auto-generated method stub
		return null;
	}
}
