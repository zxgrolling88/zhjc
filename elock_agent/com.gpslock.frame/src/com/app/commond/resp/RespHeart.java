package com.app.commond.resp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.commond.sys.AppCmd;
import com.app.commond.sys.CmdDevAddr;
import com.app.commond.sys.CmdFuncCode;
import com.app.commond.sys.CmdSend;

import soft.common.StringUtil;

/**
 * 心跳回复
 * 
 * @author fanpei
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX21, funcCode = CmdFuncCode.OX21_CenterTimeSend)
public class RespHeart extends CmdSend {
	private static DateFormat dateFormat = new SimpleDateFormat("YY/MM/dd,HH:mm:ssZ");

	public RespHeart() {
	}

	/**
	 * 获取时间时间字符串
	 * 
	 * @return
	 */
	private String getTimeStr() {
		String value = dateFormat.format(new Date());
		return value.substring(0, value.length() - 2);
	}

	@Override
	public byte[] buildBytes() {
		byte[] sendDatas = StringUtil.getUtf8Bytes(getTimeStr());
		return buildBytes(sendDatas);
	}

}
