package com.app.commond;

import com.app.commond.recv.Heart;
import com.app.commond.recv.RespHeart;
import com.app.commond.sys.*;
import com.app.protocol.ProtocolChangeIp;
import soft.common.BConvrtUtil;
import soft.common.BytesHexStrTranslate;

/**
 * 心跳数据
 * 
 * @author fanpei
 * @date 2018-09-16 22:25
 *
 */
@AppCmd(devAddr = CmdDevAddr.OX31, funcCode = CmdFuncCode.OX31_IPChange)
public class CmdChangeIp extends CmdBuild{

	private ProtocolChangeIp protocolChangeIp;
	public CmdChangeIp(String ip, String port) {
		String newIP = "";
		String[] ips = ip.split("\\.");
		for(String item:ips){
			if(item.length()==1){
				newIP+=	("00"+item);
			}
			if(item.length()==2){
				newIP+=	("0"+item);

			}
			if(item.length()==3){
				newIP+=	item;
			}
			newIP+=	".";
		}
		newIP= newIP.substring(0,newIP.length()-1);
		protocolChangeIp = new ProtocolChangeIp();
		protocolChangeIp.setIp(newIP.getBytes());
		protocolChangeIp.setTcpPort( port.getBytes());
	}

	@Override
	public byte[] buildBytes() throws Exception {
		byte[] sendDatas = protocolChangeIp.buildBytes();
		System.out.println("修改IP下行报文：" + BytesHexStrTranslate.bytesToHexFun1(sendDatas));
		return buildBytes(sendDatas);
	}

	@Override
	public void parse() throws Exception {

	}
}
