package com.app.commond;

import com.app.commond.sys.*;
import com.app.protocol.Direction;
import com.app.protocol.ProtocolLockInfo;
import soft.common.BConvrtUtil;
import soft.common.BytesHexStrTranslate;
import soft.common.DateUtils;
import soft.common.StringUtil;
import soft.ifs.IWriteLog;
import soft.log.LogWriter;

import java.util.Date;

/**
 * 锁操作
 * FE 81 2D 03 29 2A 15 32 DD276904 00 0000000000000000 0000 000000000000 00 00000000 180928224159 23 82 FF
 * FE 81 2D 03 29 2A 15 38 568C1801 00 0000000000000000 0000 353637383930 00 00000000 181127125430 23 60 FF
   FE 81 2D  03 2C 2A 15 26 45A62201 00 0000000000000000 0000 303444323038 00 00000000 190222102039 23 03 BBBBB1FF
   FE 81 2D  03 2C 2A 15 38 DD276904 00 0000000000000000 0000 303930303035 00 00000000 190222102510 23 03 BBBB34FF
 * @author fanpei
 */
@AppCmd(devAddr = CmdDevAddr.OX81, funcCode = CmdFuncCode.OX51_CenterSendELock, childFunNum = CmdFuncCode.DOWNLOCKCHILDNUM)
public class CmdCenterSendELock extends CmdBuild {
    private ProtocolLockInfo protocolLockInfo;

    public CmdCenterSendELock(byte cmd, int lockDevID, byte[] key) {
        byte[] lockdevIDs = BConvrtUtil.intToByte(lockDevID);
        protocolLockInfo = new ProtocolLockInfo();
        protocolLockInfo.setCmd(cmd);
        protocolLockInfo.setKey(key);
        protocolLockInfo.setLockdevIDs(lockdevIDs);
        String time = DateUtils.getHexTime(new Date());
        protocolLockInfo.setSendtime(BytesHexStrTranslate.toBytes(time));
    }


    @Override
    public void parse() throws Exception {
    }


    @Override
    public byte[] buildBytes() throws Exception {
        byte[] sendDatas = protocolLockInfo.buildBytes();
        System.out.println("操作锁下行报文：" + BytesHexStrTranslate.bytesToHexFun1(sendDatas));
        return buildBytes(sendDatas);
    }
}
