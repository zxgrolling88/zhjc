package com.app.redis;

import redis.clients.jedis.Jedis;
import soft.ifs.IWriteLog;
import soft.log.LogWriter;

/**
 * @author: taoroot
 * @date: 2018/1/19
 * @description:
 */
public class Publisher {
    private static final IWriteLog log = new LogWriter(Publisher.class);
    public static void sendLockStatus(String msg) {
        Jedis jedis = null;
        try {
            jedis = JedisConnectionPool.getInstance().getResource();
            jedis.publish("lock", msg);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    public static void sendGps(String msg) {
        Jedis jedis = null;
        try {
            jedis = JedisConnectionPool.getInstance().getResource();
            jedis.publish("gps", msg);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }
}
