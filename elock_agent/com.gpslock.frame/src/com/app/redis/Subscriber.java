package com.app.redis;

import com.app.commond.CmdCenterSendELock;
import com.app.commond.CmdChangeIp;
import com.app.commond.entity.CmdChangeIpBean;
import com.app.commond.entity.CmdLockBean;
import com.app.net.MyNetEventListener;
import redis.clients.jedis.JedisPubSub;
import soft.common.BConvrtUtil;
import soft.common.BytesHexStrTranslate;
import soft.common.JSONUtil;
import soft.ifs.INetChanel;
import soft.ifs.IWriteLog;
import soft.log.LogWriter;
import soft.net.lconectserver.LConectServer;
import soft.net.lconectserver.ServerConMap;


import java.util.List;


/**
 * @author: taoroot
 * @date: 2017/11/4
 * @description: 订阅者
 */
public class Subscriber extends JedisPubSub {
    private static final IWriteLog log = new LogWriter(Subscriber.class);

    @Override
    public void onMessage(String channel, String message) {
        log.info(channel + " : " + message);

        if ("changeIpOpr".equals(channel)) {
            CmdChangeIpBean changeIpBean = JSONUtil.genObject(message, CmdChangeIpBean.class);
            changeIp(changeIpBean);
        }
        //订阅锁操作指令
        if ("oprLock".equals(channel)) {
            CmdLockBean cmdLockBean = JSONUtil.genObject(message, CmdLockBean.class);
            byte oprType = (byte) 0x38;
            if (cmdLockBean.getCmd() == 38) {
                oprType = (byte) 0x38;
            } else if (cmdLockBean.getCmd() == 32) {
                oprType = (byte) 0x32;
            } else if (cmdLockBean.getCmd() == 12) {
                oprType = (byte) 0x12;
            }
            sendData(cmdLockBean, oprType);
        }


    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println(String.format("subscribe redis channel success, channel %s, subscribedChannels %d",
                channel, subscribedChannels));
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        System.out.println(String.format("unsubscribe redis channel, channel %s, subscribedChannels %d",
                channel, subscribedChannels));

    }

    private void changeIp(CmdChangeIpBean changeIpBean) {
        try {
            if (MyNetEventListener.channelHashMap.containsKey(changeIpBean.getLockDevID())) {
                INetChanel chanel = MyNetEventListener.channelHashMap.get(changeIpBean.getLockDevID());
                chanel.sendData(new CmdChangeIp(changeIpBean.getIp(), changeIpBean.getPort()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendData(CmdLockBean cmdLockBean, byte oprType) {
        try {

            if (MyNetEventListener.channelHashMap.containsKey(cmdLockBean.getLockDevID())) {
                INetChanel chanel = MyNetEventListener.channelHashMap.get(cmdLockBean.getLockDevID());
                if (cmdLockBean.getLockType() == 1) {
                    byte[] key = cmdLockBean.getPassword().getBytes();
                    chanel.sendData(new CmdCenterSendELock(oprType, cmdLockBean.getLockDevID(), key));
                } else {
                    byte[] key = BytesHexStrTranslate.toBytes("04D20008AA52");
                    chanel.sendData(new CmdCenterSendELock(oprType, cmdLockBean.getLockDevID(), key));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
