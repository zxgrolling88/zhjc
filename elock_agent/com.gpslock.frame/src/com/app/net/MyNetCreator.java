package com.app.net;

import io.netty.channel.Channel;
import soft.common.tdPool.TdCachePoolExctor;
import soft.ifs.IListenerCreator;
import soft.net.model.NetEventListener;

/**
 * 监听器创建器
 * 
 * @author fanpei
 *
 */
public class MyNetCreator implements IListenerCreator {
	private TdCachePoolExctor tdExcutor;

	public MyNetCreator() {
		tdExcutor = new TdCachePoolExctor("AppCmdHandle");
	}

	@Override
	public NetEventListener getListener(Channel ch) {
		return new MyNetEventListener(ch, tdExcutor);
	}
}
