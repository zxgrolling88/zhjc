package com.app.exception;

import soft.common.StringUtil;

/**
 * CRC8校验值错误
 * 
 * @author fanpei
 *
 */
public class CRC8CheckNotSame extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CRC8CheckNotSame(String msgStr) {
		super(StringUtil.getMsgStr("check crc8 not same:{}", msgStr));
	}
}
