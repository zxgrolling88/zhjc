package com.app.exception;

/**
 * 加载命令异常
 * 
 * @author fanpei
 * @date 2018-09-18 22:30
 *
 */
public class LoadCmdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoadCmdException(String dsrc) {
		super(dsrc);
	}

}
