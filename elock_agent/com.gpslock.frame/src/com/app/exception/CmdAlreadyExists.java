package com.app.exception;

/**
 * 此命令已存在异常
 * 
 * @author fanpei
 *
 */
public class CmdAlreadyExists extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CmdAlreadyExists(String dsrc) {
		super(dsrc);
	}

}
