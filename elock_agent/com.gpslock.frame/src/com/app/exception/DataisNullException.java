package com.app.exception;

/**
 * 数据为空 替换nullexcption
 * 
 * @author fanpei
 *
 */
public class DataisNullException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataisNullException(String dscr) {
		super(dscr);
	}
}
