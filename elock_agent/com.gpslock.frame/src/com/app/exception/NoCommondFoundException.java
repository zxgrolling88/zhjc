package com.app.exception;

/**
 * 未发现此命令
 * 
 * @author fanpei
 *
 */
public class NoCommondFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoCommondFoundException(String dsrc) {
		super(dsrc);
	}

}
