package com.app.exception;

/**
 * GPS数据校验错误
 * 
 * @author fanpei
 *
 */
public class GPSDataVertifyFailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GPSDataVertifyFailException(String dsrc) {
		super(dsrc);
	}

}
