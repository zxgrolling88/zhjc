package com.app.exception;

/**
 * SIM用户ID信息错误
 * 
 * @author fanpei
 * @date 2018-09-20 22:15
 *
 */
public class SIMUserIDErrorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SIMUserIDErrorException(String dString) {
		super(dString);
	}

}
