package soft.common;

/**
 * @project: elock_agent
 * @description: ${description}
 * @author: caochaofeng
 * @create: 2018-09-28 15:49
 */
public class DuFenMiaoToLonLat {


    public static double DuFenMiaoToLat(byte[] latitudes) {
//04685629   29 56 68 04  29 56.6804 分（北纬）
        String lat = BytesHexStrTranslate.bytesToHexFun1(latitudes);
        double du = Double.parseDouble(lat.substring(6, 8));
        double fen = Double.parseDouble(lat.substring(4, 6));
        String str = "0."+lat.substring(2, 4)+lat.substring(0, 2);
        if(str.contains("FF")){
            str =      str.replace("FF","");
        }
        double miao = Double.parseDouble(str)*60;

        return du + (fen + (miao / 60)) / 60;
    }

    public static double DuFenMiaoToLon( byte[] longitudes) {
        String lng = BytesHexStrTranslate.bytesToHexFun1(longitudes);
        // 31071412 》12 14 07 31-》121 度40.731 分（东经）
        double du = Double.parseDouble(lng.substring(6, 8)+lng.substring(4, 5));
        double fen = Double.parseDouble(lng.substring(5, 6)+lng.substring(2, 3));
        String str = "0."+lng.substring(3, 4)+lng.substring(0, 2);
        if(str.contains("FF")){
            str =     str.replace("FF","");
        }
        double miao = Double.parseDouble(str)*60;

        return du + (fen + (miao / 60)) / 60;
    }

    public static void main(String[] args) {

        String str = "0.0FF";
        if(str.contains("FF")){
            str =     str.replaceAll("F","");
        }
        double miao = Double.parseDouble(str)*60;
       System.out.println(miao);
//        System.out.println(DuFenMiaoToLat(lat));
    }
}


