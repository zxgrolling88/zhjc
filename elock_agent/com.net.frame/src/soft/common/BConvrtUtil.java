package soft.common;

import java.util.ArrayList;
import java.util.List;

/**
 * 整数 & byte[]转换助手--地位在前，高位在后
 *
 * @author fanpei
 */
public class BConvrtUtil extends StaticClass {

    /**
     * 将字节高低位调换
     *
     * @return
     */
    public static byte ChangeHighLowLoc(byte data) {
        byte high = (byte) (data & 0xf0);
        byte low = (byte) (data & 0x0f);
        return (byte) (low | high);
    }


    // long类型转成byte数组
    public static byte[] longToByte(long number) {
        long temp = number;
        byte[] b = new byte[8];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) (temp & 0xff);// 将最低位保存在最低位
            temp = temp >> 8; // 向右移8位
        }
        return b;
    }

    // byte数组转成long
    public static long byteToLong(byte[] b) {
        long s = 0;
        long s0 = b[0] & 0xff;// 最低位
        long s1 = b[1] & 0xff;
        long s2 = b[2] & 0xff;
        long s3 = b[3] & 0xff;
        long s4 = b[4] & 0xff;
        long s5 = b[5] & 0xff;
        long s6 = b[6] & 0xff;
        long s7 = b[7] & 0xff;

        // s0不变
        s1 <<= 8;
        s2 <<= 16;
        s3 <<= 24;
        s4 <<= 32;
        s5 <<= 40;
        s6 <<= 48;
        s7 <<= 56;
        s = s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7;
        return s;
    }

    /**
     *      * 数字字符串转ASCII码字符串
     *      *
     *      * @param String
     *      *            字符串
     *      * @return ASCII字符串
     *     
     */
    public static String stringToAsciiString(String content) {
        String result = "";
        int max = content.length();
        for (int i = 0; i < max; i++) {
            char c = content.charAt(i);
            String b = Integer.toHexString(c);
            result = result + b;
        }
        return result;
    }

    /**
     * byte数组中取int数值，本方法适用于(低位在前，高位在后)的顺序。
     *
     * @param ary byte数组
     * @return int数值
     */
    public static int bytesToInt(byte[] ary) {
        int value;
        int offset = 0;
        value = (int) ((ary[offset] & 0xFF)
                | ((ary[offset + 1] << 8) & 0xFF00)
                | ((ary[offset + 2] << 16) & 0xFF0000)
                | ((ary[offset + 3] << 24) & 0xFF000000));
        return value;
    }

    /**
     * byte数组转成long-高位在左
     *
     * @param b
     * @return
     */
    public static long byteHighToLong(byte[] b) {
        long s = 0;
        long[] sr = new long[8];
        for (int i = 0; i < b.length; i++) {
            sr[i] = b[i] & 0xff; // 最高位
        }

        // s7不变
        sr[6] <<= 8;// 左移自动补0
        sr[5] <<= 16;
        sr[4] <<= 24;
        sr[3] <<= 32;
        sr[2] <<= 40;
        sr[1] <<= 48;
        sr[0] <<= 56;
        s = sr[0] | sr[1] | sr[2] | sr[3] | sr[4] | sr[5] | sr[6] | sr[7];// 或运算相加
        return s;
    }

    public static byte[] intToByte(int number) {
        int temp = number;
        byte[] b = new byte[4];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) (temp & 0xff);// 将最低位保存在最低位
            temp = temp >> 8; // 向右移8位
        }
        return b;
    }



    public  static void main(String args[]){
        //5205D0C3
        System.out.println(byteToInt(new byte[]{ 0x05,0x52}));
    }

    public static int byteToInt(byte[] b) {
        int s = 0;
        int s0 = 0;
        int s1 = 0;
        int s2 = 0;
        int s3 = 0;

        s0 = b[0] & 0xff;// 最低位
        if (b.length > 1)
            s1 = b[1] & 0xff;
        if (b.length > 2)
            s2 = b[2] & 0xff;
        if (b.length > 3)
            s3 = b[3] & 0xff;
        s3 <<= 24;
        s2 <<= 16;
        s1 <<= 8;
        s = s0 | s1 | s2 | s3;
        return s;
    }

    public static byte[] shortToByte(short number) {
        int temp = number;
        byte[] b = new byte[2];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) (temp & 0xff);// 将最低位保存在最低位
            temp = temp >> 8; // 向右移8位
        }
        return b;
    }

    public static short byteToShort(byte[] b) {
        short s = 0;
        short s0 = (short) (b[0] & 0xff);// 最低位
        short s1 = (short) (b[1] & 0xff);
        s1 <<= 8;
        s = (short) (s0 | s1);
        return s;
    }

    /**
     * brief  获取电压值
     * params
     * time 2017/10/16 19:21
     * return
     */
    public static double getV(byte v) {
        double elect = 0.0;
        switch (v) {
            case (byte) 0x30:
                elect = 3.3;//严重低电压
                break;
            case (byte) 0x31:
                elect = 3.6;//电压低
                break;
            case (byte) 0x32:
                elect = 3.7;//电压正常
                break;
            case (byte) 0x33:
                elect = 3.8;//电压正常
                break;
            case (byte) 0x34:
                elect = 3.9;//电压正常
                break;
            case (byte) 0x35:
                elect = 4.0;//电压正常
                break;
            case (byte) 0x36:
                elect = 4.1;//电压正常
                break;
            case (byte) 0x37:
                elect = 4.2;//满电
                break;
        }
        return elect;

    }

    /**
     * 操作来源 操作源：短信 0x00,自动 0x01,键盘 0x02,手持机 0X03；
     * 中心 0X04；卡口 0X05；
     *
     * @param v
     * @return
     */
    public static String getLockOprSource(byte v) {

        String elect = "";
        switch (v) {
            case (byte) 0x00:
                elect = "短信";
                break;
            case (byte) 0x01:
                elect = "自动";
                break;
            case (byte) 0x02:
                elect = "键盘";
                break;
            case (byte) 0X03:
                elect = "手持机";
                break;
            case (byte) 0X04:
                elect = "中心";
                break;
            case (byte) 0X05:
                elect = "卡口";
                break;
        }
        return elect;
    }

    /**
     * 锁设备回复状态
     * params
     * time 2017/10/16 19:21
     * return
     */
    public static String getLockRespStatus(byte v) {
        String elect = "";
        switch (v) {
            case (byte) 0x62:
                elect = "检查成功";//检查成功
                break;
            case (byte) 0x69:
                elect = "检查超时";//检查超时
                break;
            case (byte) 0xA1:
                elect = "设置动态密码成功";//设置动态密码成功
                break;
            case (byte) 0x80:
                elect = "施封成功";//施封成功
                break;
            case (byte) 0x81:
                elect = "重复施封";//重复施封
                break;
            case (byte) 0x82:
                elect = "锁未插好,施封失败";//锁未插好,施封失败
                break;
            case (byte) 0x83:
                elect = "电压过低，不能施封";//电压过低，不能施封
                break;
            case (byte) 0x84:
                elect = "锁异常：非法拆壳,不予施封";//锁异常：非法拆壳,不予施封
                break;

            case (byte) 0x85:
                elect = "锁异常：应急开锁，不予施封";//锁异常：应急开锁，不予施封
                break;
            case (byte) 0X86:
                elect = "锁杆剪断报警，不予施封";//锁杆剪断报警，不予施封
                break;
            case (byte) 0X87:
                elect = "锁打开报警，不予施封";//锁打开报警，不予施封
                break;
            case (byte) 0X89:
                elect = "施封超时";//施封超时
                break;
            case (byte) 0x90:
                elect = "解封成功";//解封成功
                break;
            case (byte) 0x91:
                elect = "重复解封";//重复解封
                break;


            case (byte) 0x92:
                elect = "锁处于打开状态时解封";//锁处于打开状态时解封
                break;
            case (byte) 0X93:
                elect = "密钥不符，解封失败";//密钥不符，解封失败
                break;
            case (byte) 0X94:
                elect = "锁异常：非法拆壳,不予解封";//锁异常：非法拆壳,不予解封
                break;
            case (byte) 0X95:
                elect = "锁异常：应急开锁，不予解封";//锁异常：应急开锁，不予解封
                break;
            case (byte) 0x96:
                elect = "打开报警，不予解封";//打开报警，不予解封
                break;
            case (byte) 0x97:
                elect = "未施封就解";//未施封就解
                break;
            case (byte) 0x98:
                elect = "剪断报警，不予解封";//剪断报警，不予解封
                break;
            case (byte) 0x99:
                elect = "解封超时";//解封超时
                break;


            case (byte) 0x76:
                elect = "读数据成功";//读数据成功
                break;
            case (byte) 0x77:
                elect = "锁内部无记录";//锁内部无记录
                break;
            case (byte) 0x78:
                elect = "密钥不符，不能读取";//密钥不符，不能读取
                break;
            case (byte) 0x79:
                elect = "读取超时";//读取超时
                break;
            case (byte) 0X7A:
                elect = "记录上传";//记录上传
                break;


            case (byte) 0x70:
                elect = "解除报警成功";//解除报警成功
                break;


            case (byte) 0x71:
                elect = "解除报警超时";//解除报警超时
                break;
            case (byte) 0x72:
                elect = "锁不在报警状态";//锁不在报警状态
                break;
            case (byte) 0x73:
                elect = "密钥不符， 不予解除";//密钥不符， 不予解除
                break;
            case (byte) 0x74:
                elect = "锁异常：非法拆壳,不予解除";//锁异常：非法拆壳,不予解除
                break;
            case (byte) 0x75:
                elect = "锁异常：应急开锁，不予解除";//锁异常：应急开锁，不予解除
                break;


        }
        return elect;

    }

    /**
     * 合并组装新的数组
     *
     * @param datas1
     * @param datas2
     * @return
     */
    public static byte[] combineArry(byte[] datas1, byte[] datas2) {
        if (datas1 == null) {
            return datas2;
        }
        if (datas2 == null) {
            return datas1;
        }

        byte[] newData = new byte[datas1.length + datas2.length];
        System.arraycopy(datas1, 0, newData, 0, datas1.length);
        System.arraycopy(datas2, 0, newData, datas1.length, datas2.length);
        return newData;
    }

}
