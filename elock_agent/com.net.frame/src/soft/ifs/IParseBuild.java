package soft.ifs;

import soft.net.exception.DecodeDataFailException;

/**
 * 解析和组装数据
 * 
 * @author fanpei
 *
 */
public interface IParseBuild {

	/**
	 * 解析数据
	 * 
	 * @param in
	 * @throws DecodeDataFailException
	 */
	void parse(IByteBuff in) throws DecodeDataFailException;

	/**
	 * 构造组装数据
	 * 
	 * @return
	 */
	byte[] build();
}
