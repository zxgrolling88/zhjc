package soft.ifs;

/**
 * 网络传输数据接口
 * 
 * @author fanpei
 *
 */
public interface INetTrans {

	/**
	 * 组装数据
	 */

	byte[] buildBytes() throws Exception;
}
