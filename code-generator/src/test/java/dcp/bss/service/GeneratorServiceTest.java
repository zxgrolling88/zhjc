package dcp.bss.service;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


/**
 * Created by Terry on 2017/8/12.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeneratorServiceTest {

    @Autowired
    private SysGeneratorService sysGeneratorService;
    //workspace路径
    String wsPath = "D://code//";
    //zip输出路径
    String zipPath = wsPath+"gencode.zip";
    //表名
    String[] tableNames = new String[]{"biz_alarm","biz_area_in_out","biz_device",
            "biz_entp","biz_entp_loc","biz_geo","biz_gps","biz_history_gps","biz_pers","biz_vec","biz_alarm_set","biz_device_opr_log","biz_lock_status"};

    @Test
    public void testGeneratorCode() throws IOException {
        byte[] data = sysGeneratorService.generatorCode(tableNames);
        File file = new File(zipPath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        FileUtils.writeByteArrayToFile(file, data);

        //解压zip
        ZipInputStream zip = new ZipInputStream(new FileInputStream(zipPath));
        ZipEntry entry;
        while ((entry = zip.getNextEntry()) != null) {
            file = new File(wsPath+"gencode//" + entry.getName());
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            FileOutputStream out = new FileOutputStream(file);
            IOUtils.copy(zip, out);
        }
        zip.close();
    }
}
