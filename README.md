# 车辆在途实时监控项目

#### 项目介绍
车辆在途实时监控项目

#### 软件架构
软件架构说明


#### 安装教程

 **在途监控后端 管理平台** 


### 开发



IDE建议使用IDEA
拉取代码建议使用tortoisegit拉取，更新和提交建议使用IDEA
请安装Lombok IDEA插件

请使用JDK1.8



### 代码库




前端 http://183.136.157.103:6080/zhjc/front-end

后端 http://183.136.157.103:6080/zhjc/back-end

文档 http://183.136.157.103:6080/zhangchaoxu/zhjc-doc



### 公网访问地址

http://47.99.77.10


### 阿里云配置参考链接

redis安装： https://www.jianshu.com/p/5af382da7169
nginx安装参考：https://blog.csdn.net/herenoname/article/details/78933479
jdk安装: https://blog.csdn.net/ksksjipeng/article/details/54958696
MySQL 安装： https://blog.csdn.net/xiangxianghehe/article/details/77427077



### 数据库



线上测试mysql数据库

IP：47.99.77.10
数据库：ztjk
账号信息：ztjk/ztjk4DC@2018


线上redis数据库

    database: 1
    host: 47.99.77.10
    password: ztjk@2018
    port: 6379
    timeout: 10000

### 技术栈


Spring Boot 2
MyBatis
Shiro





----------------------------------------------分隔符---------------------------------------------------------

 **在途监控前端 开发流程** 


zhjc-vue 在途监控前端-VUE前端工程


About

本项目基于vue + Element UI构建开发
参考vue-element-admin

element参考框架 http://element-cn.eleme.io/#/zh-CN/component/input

目录结构

项目通过vue-cli 脚手架生成基础开发框架，再根据业务需求做调整修改而成

├── dist // 构建打包生成部署文件
├── build // 构建相关
├── config // 构建配置相关
├── src // 源代码
│ ├── assets // 静态资源
│ ├── components // 全局公用组件
│ ├── element-ui // element-ui组件配置
│ ├── icons // 字体图标
│ ├── router // 路由
│ ├── store // 全局store状态管理
│ ├── utils // 全局公用方法
│ ├── views // view业务视图
│ ├── App.vue // 入口组件
│ ├── main.js // 入口
├── static // 第三方不打包静态资源
├── .babelrc // babel-loader配置
├── eslintrc.js // eslint配置项
├── .gitignore // git忽略项
├── .postcssrc // PostCSS配置项
├── index.html // html模板
└── package.json // package.json    


Inatsll


npm install



Dev


npm run dev



Build


npm run build


 **目录结构** 

back-end: 车辆在途监控-后端接口程序
front-end :车辆在途监控-前端VUE工程
ztjk-doc :车辆在途监控-相关文档